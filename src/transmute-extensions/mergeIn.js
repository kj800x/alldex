import curry from "transmute/curry";
import merge from "transmute/merge";
import updateIn from "transmute/updateIn";

const mergeIn = curry((keyPath, patchObject, subject) =>
  updateIn(keyPath, merge(patchObject), subject)
);

export default mergeIn;
