export default function getComponentName(component) {
  return component.getDisplayName
    ? component.getDisplayName()
    : component.displayName || component.name || "Component";
}

export function getRootComponent(component) {
  return component.rootComponent ? component.rootComponent : component;
}
