import React from "react";
import { Route, Switch } from "react-router-dom";

import TagGroupPage from "../tag-groups/pages/TagGroupPage";
import withLayout from "../layout/withLayout";
import { getRootComponent } from "./getComponentName";

import HomePage from "../home-page/HomePage";
import TagItemsListPage from "../tags/pages/TagItemsListPage";
import AlbumItemsListPage from "../albums/pages/AlbumItemsListPage";
import ImportPage from "../import/pages/ImportPage";
import TagGroupsListPage from "../tag-groups/pages/TagGroupListPage";
// import RandomImagesPage from "view/image/randomImages/RandomImagesPage";
import RandomItemsPage from "../items/pages/RandomItemsPage";
import NotFoundPage from "../page-not-found/NotFoundPage";
import ItemPage from "../items/pages/ItemPage";
// import ImageListByAlbumPage from "view/album/imageListByAlbum/ImageListByAlbumPage";
// import SingleImageByAlbumIndexPage from "view/album/singleImageByAlbumIndex/SingleImageByAlbumIndexPage";
// import SimilarImagesListPage from "view/image/similarImagesListPage/SimilarImagesListPage";
// import SimilarImagesPage from "view/image/similarImagePairsPage/SimilarImagePairsPage";
// import SlideshowConfigPage from "view/image/slideshow/SlideshowConfigPage";
// import SlideshowSlidePage from "view/image/slideshow/SlideshowSlidePage";
// import TagMergePage from "view/tag/tagMerge/TagMergePage";
// import ImageMergePage from "view/image/mergeImagesPage/ImageMergePage";
import TagDetailsPage from "../tags/pages/TagDetailsPage";
import AlbumDetailsPage from "../albums/pages/AlbumDetailsPage";
import SearchPage from "../items/pages/SearchPage";
import AlbumSlideshowPage from "../albums/pages/AlbumSlideshowPage";
import DuplicateItemsPage from "../duplicates/pages/DuplicateItemsPage";
import ItemDuplicatePage from "../duplicates/pages/ItemDuplicatePage";
import SearchSlideshowPage from "../items/pages/SearchSlideshowPage";
import RandomSlideshowPage from "../items/pages/RandomSlideshowPage";
import AlbumVideoPage from "../albums/pages/AlbumVideoPage";

class RoutingConfig {
  constructor() {
    this.routes = [
      { path: "/", class: withLayout(HomePage) },
      { path: "/taggroups/", class: withLayout(TagGroupsListPage) },
      { path: "/taggroup/:tagGroupId", class: withLayout(TagGroupPage) },
      { path: "/items/import", class: withLayout(ImportPage) },
      {
        path: "/items/randomSlideshow",
        class: withLayout(RandomSlideshowPage)
      },
      { path: "/items/random", class: withLayout(RandomItemsPage) },
      {
        path: "/items/duplicates/:page",
        class: withLayout(DuplicateItemsPage)
      },
      {
        path: "/items/searchSlideshow/:query/:position",
        class: withLayout(SearchSlideshowPage)
      },
      { path: "/items/search/:query/:page", class: withLayout(SearchPage) },
      { path: "/item/:itemId", class: withLayout(ItemPage) },
      {
        path: "/item/:itemId/duplicates/:page",
        class: withLayout(ItemDuplicatePage)
      },
      { path: "/tag/:tagId/items/:page", class: withLayout(TagItemsListPage) },
      { path: "/tag/:tagId/details", class: withLayout(TagDetailsPage) },
      {
        path: "/album/:albumId/items/:page",
        class: withLayout(AlbumItemsListPage)
      },
      {
        path: "/album/:albumId/item/:position",
        class: withLayout(AlbumSlideshowPage)
      },
      { path: "/album/:albumId/video", class: withLayout(AlbumVideoPage) },
      { path: "/album/:albumId/details", class: withLayout(AlbumDetailsPage) },
      // {path: "/tag/byId/:tagId", class: TagDetailsPage},
      // {path: "/tag/merge/:tagNameB/:tagNameA", class: TagMergePage},
      // {path: "/tag/merge/:tagNameB", class: TagMergePage},
      // {path: "/tag/merge", class: TagMergePage},
      // {path: "/album/byId/:albumId", class: AlbumDetailsPage},
      // {path: "/slideshow", class: SlideshowConfigPage},
      // {path: "/slideshow/:order/:page/:searchTerm", class: SlideshowSlidePage},
      // {path: "/slideshow/:order/:page", class: SlideshowSlidePage},
      // {path: "/photo/import", class: ImportPage},
      // {path: "/photo/random", class: RandomImagesPage},
      // {path: "/photo/merge/:photoAId/:photoBId", class: ImageMergePage},
      // {path: "/photo/byId/:photoId", class: SingleImagePage},
      // {path: "/photo/byTag/:tag/page/:page", class: ImageListByTagPage},
      // {path: "/photo/byAlbum/:album/page/:page", class: ImageListByAlbumPage},
      // {path: "/photo/byAlbum/:albumId/index/:index", class: SingleImageByAlbumIndexPage},
      // {path: "/photo/:sourcePhotoId/similar/:page", class: SimilarImagesListPage},
      // {path: "/photos/similar/:page", class: SimilarImagesPage},
      { path: "", inexact: true, class: withLayout(NotFoundPage) }
    ];
  }

  static format(toString, params) {
    const out = [];
    for (let i = 0; i < toString.length; i++) {
      if (toString[i] === ":") {
        const paramCollector = [];
        // Consume toString until the next "/" or the string ends
        i++; // Skip forward to exclude the leading ":" from the paramCollector
        while (toString[i] !== "/" && toString[i] !== undefined) {
          paramCollector.push(toString[i]);
          i++;
        }
        i--; // Skip backwards to include the "/" in the toString
        // Add the param
        const param = paramCollector.join("");
        if (Object.keys(params).indexOf(param) === -1) {
          throw new Error(
            `Param missing from params (param: ${param}, params: ${JSON.stringify(
              params
            )})`
          );
        }
        out.push(params[param]);
      } else {
        out.push(toString[i]);
      }
    }
    return out.join("");
  }

  urlFor(pageClass, params = {}) {
    if (pageClass === null) {
      return "#";
    }
    const pageClassName = getRootComponent(pageClass);
    const route = this.routes.find(
      r => getRootComponent(r.class) === pageClassName
    );
    if (route) {
      return RoutingConfig.format(route.path, params);
    }
    throw new Error(`Page class not recognized (${pageClassName})`);
  }

  allRoutes(props) {
    const out = this.routes.map(route => {
      const render = routeProps => {
        return React.createElement(
          route.class,
          Object.assign({}, routeProps, props)
        );
      };
      return (
        <Route
          key={route.path}
          exact={route.inexact === undefined}
          path={route.path}
          render={render}
        />
      );
    });
    return <Switch>{out}</Switch>;
  }
}

export default new RoutingConfig();
