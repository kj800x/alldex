import React from "react";
import "./HomePage.css";
import SearchBar from "../library/searchBar/SearchBar";
import RandomItemsPage from "../items/pages/RandomItemsPage";
import RoutingConfig from "../routing/RoutingConfig";
import ImportPage from "../import/pages/ImportPage";
import BigButton from "../library/bigButton/BigButton";

function HomePage(props) {
  return (
    <div className="home-page">
      <h1> Alldex </h1>
      <div className="shortcuts">
        <BigButton target={RoutingConfig.urlFor(RandomItemsPage)}>
          Random Items
        </BigButton>
        <BigButton target={RoutingConfig.urlFor(ImportPage)}>Import</BigButton>
      </div>
      <SearchBar onSearch={props.onRedirect} />
    </div>
  );
}

export default HomePage;
