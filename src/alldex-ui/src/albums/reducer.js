import {
  FETCH_ALBUM,
  CREATE_ALBUM,
  DELETE_ALBUM,
  UPDATE_ALBUM,
  FETCH_ALL_ALBUMS
} from "./actions";
import { handleActions } from "redux-actions";
import { STARTED, SUCCEEDED, FAILED, UNINITIALIZED } from "../util/fetchStatus";

import setIn from "transmute/setIn";
import pipe from "transmute/pipe";
import updateIn from "transmute/updateIn";
import filter from "transmute/filter";

const defaultState = {
  fetchStatus: {},
  fetchAllStatus: UNINITIALIZED,
  data: {}
};

export default handleActions(
  {
    [FETCH_ALBUM.STARTED]: (
      state,
      {
        payload: {
          args: { id }
        }
      }
    ) => {
      return setIn(["fetchStatus", id], STARTED)(state);
    },
    [FETCH_ALBUM.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id },
          response
        }
      }
    ) => {
      return pipe(
        setIn(["fetchStatus", id], SUCCEEDED),
        setIn(["data", id], response)
      )(state);
    },
    [FETCH_ALBUM.FAILED]: (
      state,
      {
        payload: {
          args: { id }
        }
      }
    ) => {
      return setIn(["fetchStatus", id], FAILED)(state);
    },
    [FETCH_ALL_ALBUMS.STARTED]: state => {
      return setIn(["fetchAllStatus"], STARTED)(state);
    },
    [FETCH_ALL_ALBUMS.FAILED]: state => {
      return setIn(["fetchAllStatus"], FAILED)(state);
    },
    [FETCH_ALL_ALBUMS.SUCCEEDED]: (state, { payload: { response } }) => {
      return pipe(
        setIn(["fetchAllStatus"], SUCCEEDED),
        setIn(
          ["data"],
          response.reduce(
            (acc, elem) => Object.assign(acc, { [elem.id]: elem }),
            {}
          )
        )
      )(state);
    },
    [CREATE_ALBUM.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { title },
          response: [id]
        }
      }
    ) => {
      return pipe(
        setIn(["fetchStatus", id], SUCCEEDED),
        setIn(["data", id], { title, description: "", id })
      )(state);
    },
    [UPDATE_ALBUM.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { title, description, id }
        }
      }
    ) => {
      return pipe(
        setIn(["fetchStatus", id], SUCCEEDED),
        setIn(["data", id], { title, description, id })
      )(state);
    },
    [DELETE_ALBUM.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id }
        }
      }
    ) => {
      return pipe(
        updateIn(["fetchStatus"], filter((_value, key) => key !== `${id}`)),
        updateIn(["data"], filter((_value, key) => key !== `${id}`))
      )(state);
    }
  },
  defaultState
);
