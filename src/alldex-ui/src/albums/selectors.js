export const getAlbumId = name => state =>
  parseInt(
    Object.keys(state.albums.data).find(
      id => state.albums.data[id].title === name
    )
  ) || -1;
export const getPosition = (album_id, item_id) => state =>
  state.items.data[item_id].albums.find(elem => elem.album_id === album_id)
    .position || -1;
