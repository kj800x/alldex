import React from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import AlbumDetails from "../components/AlbumDetails";

const FETCH_ALBUM = gql`
  query album($id: Int) {
    getAlbum(id: $id) {
      title
      id
      description
      __typename
    }
  }
`;

const UPDATE_ALBUM = gql`
  mutation upsertAlbum($album: AlbumInput) {
    upsertAlbum(album: $album) {
      title
      id
      description
      __typename
    }
  }
`;

export default function AlbumDetailsContainer({ id }) {
  const { loading, error, data } = useQuery(FETCH_ALBUM, { variables: { id } });
  const [updateAlbum] = useMutation(UPDATE_ALBUM);
  const [deleteAlbum] = useMutation(UPDATE_ALBUM, {
    variables: { album: { id, isDeleted: true } },
    onCompleted: () => {
      alert("album deleted");
    }
  });

  if (loading) return <span>Album Details Loading...</span>;
  if (error) return <span>Album Details Query Error :(</span>;

  return (
    <AlbumDetails
      albumData={data.getAlbum}
      updateAlbum={patch =>
        updateAlbum({ variables: { album: { id, ...patch } } })
      }
      deleteAlbum={deleteAlbum}
    />
  );
}
