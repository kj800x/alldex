import ThumbPageAlbumHeader from "../components/ThumbPageAlbumHeader";
import withEntity from "../../util/withEntity/withEntity";

import { fetchAlbum } from "../actions";

export default withEntity("albums", "albumData", fetchAlbum, ({ id }) => id)(
  ThumbPageAlbumHeader
);
