import { connect } from "react-redux";

import SidebarAlbumElement from "../components/SidebarAlbumElement";
import withEntity from "../../util/withEntity/withEntity";

import { fetchAlbum } from "../actions";
import { enhancedDissociate } from "../actions";

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch, props) => ({
  onAlbumRemove: albumId =>
    dispatch(enhancedDissociate({ item_id: props.itemId, album_id: albumId }))
});

export default withEntity("albums", "albumData", fetchAlbum, ({ id }) => id)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SidebarAlbumElement)
);
