import { rawSearchContainer } from "../../items/containers/SearchContainer";

import MergedVideoScreen from "../components/MergedVideoScreen";
import withSettings from "../../settings/withSettings";

export default rawSearchContainer(withSettings(MergedVideoScreen));
