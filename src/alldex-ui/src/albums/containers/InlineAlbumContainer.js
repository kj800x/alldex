import InlineAlbum from "../components/InlineAlbum";
import withEntity from "../../util/withEntity/withEntity";

import { fetchAlbum } from "../actions";

export default withEntity("albums", "albumData", fetchAlbum, ({ id }) => id)(
  InlineAlbum
);
