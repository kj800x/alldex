import { connect } from "react-redux";
import { push } from "connected-react-router";
import AlbumSlideshowIndicatorWidget from "../components/AlbumSlideshowIndicatorWidget";

const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch => ({
  push: url => dispatch(push(url))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AlbumSlideshowIndicatorWidget);
