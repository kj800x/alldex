import React from "react";

function fetchArrayBuffer(url) {
  return new Promise(resolve => {
    var xhr = new XMLHttpRequest();
    xhr.open("get", url);
    xhr.responseType = "arraybuffer";
    xhr.onload = function() {
      resolve(xhr.response);
    };
    xhr.send();
  });
}

function nextEvent(emitter, eventType) {
  return new Promise(resolve => {
    const listener = event => {
      emitter.removeEventListener(eventType, listener);
      resolve(event);
    };

    emitter.addEventListener(eventType, listener);
  });
}

function firstNatural(predicate) {
  let n = 0;
  while (true) {
    if (predicate(n)) {
      return n;
    }
    n++;
  }
}

export default class MergedVideoViewer extends React.Component {
  constructor(props) {
    super(props);
    this.videoRef = React.createRef();
  }

  async setupFallbackVideoSequencing() {
    const video = this.videoRef.current;

    const files = this.props.searchResults.map(
      result => process.env.PUBLIC_URL + "/staticAssets/" + result.file
    );

    for (const file of files) {
      video.src = file;
      video.currentTime = 0;
      await nextEvent(video, "canplay");
      this.props.setActiveResult(files.indexOf(file));
      video.play();
      await nextEvent(video, "ended");
    }
  }

  async componentDidMount() {
    const video = this.videoRef.current;

    const files = this.props.searchResults.map(
      result => process.env.PUBLIC_URL + "/staticAssets/" + result.file
    );
    const mimeCodec = 'video/mp4; codecs="avc1.42E01E, mp4a.40.2"';

    const mediaSource = new MediaSource();

    video.src = URL.createObjectURL(mediaSource);
    await nextEvent(mediaSource, "sourceopen");

    const sourceBuffer = mediaSource.addSourceBuffer(mimeCodec);
    sourceBuffer.mode = "sequence";

    const sourceStarts = [];

    for (const file of files) {
      const buffer = await fetchArrayBuffer(file);

      sourceBuffer.appendBuffer(buffer);
      await nextEvent(sourceBuffer, "updateend");

      if (video.error) {
        console.log(video.error);
        if (video.error.message.includes("Detected unfragmented")) {
          console.log("Please fragment your video!!!");
        }
        console.log("Using fallback video sequencing");
        this.setupFallbackVideoSequencing();
        return;
      }

      sourceStarts.push(sourceBuffer.buffered.end(0));
    }
    mediaSource.endOfStream();

    video.addEventListener("timeupdate", () => {
      this.props.setActiveResult(
        firstNatural(
          n =>
            n === sourceStarts.length - 1 || sourceStarts[n] > video.currentTime
        )
      );
    });
  }

  render() {
    return <video ref={this.videoRef} controls autoPlay />;
  }
}
