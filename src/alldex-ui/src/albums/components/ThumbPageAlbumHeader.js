import React, { Component } from "react";

import LinkLike from "../../library/linkLike/LinkLike";

import "./ThumbPageAlbumHeader.css";
import AlbumDetailsPage from "../pages/AlbumDetailsPage";
import RoutingConfig from "../../routing/RoutingConfig";
import AlbumSlideshowPage from "../pages/AlbumSlideshowPage";
import AlbumVideoPage from "../pages/AlbumVideoPage";

export default class ThumbPageAlbumHeader extends Component {
  getAlbumDetailsPageUrl() {
    return RoutingConfig.urlFor(AlbumDetailsPage, {
      albumId: this.props.albumData.id
    });
  }

  render() {
    return (
      <div>
        <h2 className="thumb-page-header">
          Album:&nbsp;
          <LinkLike target={this.getAlbumDetailsPageUrl()}>
            {this.props.albumData.title}
          </LinkLike>{" "}
          (
          <LinkLike
            target={RoutingConfig.urlFor(AlbumSlideshowPage, {
              albumId: this.props.albumData.id,
              position: 1
            })}
          >
            Slides
          </LinkLike>
          ) (
          <LinkLike
            target={RoutingConfig.urlFor(AlbumVideoPage, {
              albumId: this.props.albumData.id
            })}
          >
            MV
          </LinkLike>
          )
        </h2>
        {this.props.albumData.description && (
          <div className="thumb-page-album-header-description">
            {this.props.albumData.description}
          </div>
        )}
      </div>
    );
  }
}
