import React, { Component } from "react";
import "./InlineAlbum.css";

import AlbumLink from "./AlbumLink";

export default class InlineAlbum extends Component {
  render() {
    return (
      <span className="inline-album">
        <AlbumLink
          albumData={this.props.albumData}
          position={this.props.position}
        />
      </span>
    );
  }
}
