import React, { Component } from "react";
import "./SidebarAlbumElement.css";
import RoutingConfig from "../../routing/RoutingConfig";
import LinkLike from "../../library/linkLike/LinkLike";
import AlbumDetailsPage from "../pages/AlbumDetailsPage";
import AlbumLink from "./AlbumLink";

export default class SidebarAlbumElement extends Component {
  getAlbumDetailsPage() {
    return RoutingConfig.urlFor(AlbumDetailsPage, {
      albumId: this.props.albumData.id
    });
  }

  renderRemoveButton() {
    if (this.props.editingEnabled) {
      return [
        <span key="spacing">&nbsp;</span>,
        <button
          key="button"
          onClick={this.props.onAlbumRemove.bind(this, this.props.albumData.id)}
        >
          -
        </button>
      ];
    }
    return null;
  }

  render() {
    return (
      <div className="album">
        <LinkLike
          className="album-details-link"
          target={this.getAlbumDetailsPage()}
        >
          ?
        </LinkLike>
        &nbsp;
        <AlbumLink
          albumData={this.props.albumData}
          position={this.props.position}
        />
        {this.renderRemoveButton()}
      </div>
    );
  }
}

SidebarAlbumElement.defaultProps = {
  editingEnabled: false
};
