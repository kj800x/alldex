import React, { Component } from "react";

import classNames from "classnames";

import "./AlbumSlideshowIndicatorWidget.css";
import LinkLike from "../../library/linkLike/LinkLike";
import RoutingConfig from "../../routing/RoutingConfig";
import AlbumSlideshowPage from "../pages/AlbumSlideshowPage";
import KeyboardInjector from "../../library/slideshowKeyboardInjector/SlideshowKeyboardInjector";

const DirectionalButton = ({
  totalResultsCount,
  position,
  truePosition,
  albumId,
  children
}) => {
  const visible =
    position !== truePosition && position >= 1 && position <= totalResultsCount;

  return (
    <LinkLike
      className={classNames("directional-button", "minibutton", { visible })}
      target={RoutingConfig.urlFor(AlbumSlideshowPage, { albumId, position })}
    >
      {children}
    </LinkLike>
  );
};

export default class AlbumSlideshowIndicatorWidget extends Component {
  constructor(props) {
    super(props);
    this.onLeftArrow = this.onLeftArrow.bind(this);
    this.onRightArrow = this.onRightArrow.bind(this);
  }

  onLeftArrow() {
    if (this.props.position > 1) {
      this.props.push(
        RoutingConfig.urlFor(AlbumSlideshowPage, {
          albumId: this.props.albumId,
          position: this.props.position - 1
        })
      );
    }
  }

  onRightArrow() {
    if (this.props.position < this.props.totalResultsCount) {
      this.props.push(
        RoutingConfig.urlFor(AlbumSlideshowPage, {
          albumId: this.props.albumId,
          position: this.props.position + 1
        })
      );
    }
  }

  render() {
    return (
      <div className="item-page-banner">
        <KeyboardInjector
          onLeftArrow={this.onLeftArrow}
          onRightArrow={this.onRightArrow}
        />
        <DirectionalButton
          albumId={this.props.albumId}
          totalResultsCount={this.props.totalResultsCount}
          truePosition={this.props.position}
          position={1}
        >
          &lt;&lt;
        </DirectionalButton>
        <DirectionalButton
          keyboardEvent="ArrowLeft"
          albumId={this.props.albumId}
          totalResultsCount={this.props.totalResultsCount}
          truePosition={this.props.position}
          position={this.props.position - 1}
        >
          &lt;
        </DirectionalButton>
        Album {this.props.albumId} ({this.props.position}/
        {this.props.totalResultsCount})
        <DirectionalButton
          keyboardEvent="ArrowRight"
          albumId={this.props.albumId}
          totalResultsCount={this.props.totalResultsCount}
          truePosition={this.props.position}
          position={this.props.position + 1}
        >
          &gt;
        </DirectionalButton>
        <DirectionalButton
          albumId={this.props.albumId}
          totalResultsCount={this.props.totalResultsCount}
          truePosition={this.props.position}
          position={this.props.totalResultsCount}
        >
          &gt;&gt;
        </DirectionalButton>
      </div>
    );
  }
}
