import React, { useState } from "react";

import MergedVideoViewer from "./MergedVideoViewer";
import Sidebar from "../../items/components/Sidebar";
import RoutingConfig from "../../routing/RoutingConfig";
import ItemPage from "../../items/pages/ItemPage";
import LinkLike from "../../library/linkLike/LinkLike";

export default ({ searchResults, showHidden }) => {
  const [activeResult, setActiveResult] = useState(0);
  return (
    <div className="item-viewer">
      <div className="item-viewer-sidebar">
        <Sidebar
          data={searchResults[activeResult]}
          showHidden={showHidden}
          banner={
            <div className="item-page-banner" style={{ padding: 10 }}>
              <LinkLike
                className="minibutton"
                target={RoutingConfig.urlFor(ItemPage, {
                  itemId: searchResults[activeResult].id
                })}
              >
                Item
              </LinkLike>
            </div>
          }
          editingEnabled={false}
        />
      </div>
      <div className="item-viewer-main">
        <MergedVideoViewer
          searchResults={searchResults}
          setActiveResult={setActiveResult}
        />
      </div>
    </div>
  );
};
