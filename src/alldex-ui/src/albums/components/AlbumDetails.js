import React, { Component } from "react";

import RoutingConfig from "../../routing/RoutingConfig";
import LinkLike from "../../library/linkLike/LinkLike";
import classNames from "classnames";

import "./AlbumDetails.css";
import AlbumItemsListPage from "../pages/AlbumItemsListPage";

const nullAsEmptyString = e => e || "";

export default class AlbumDetails extends Component {
  constructor(props) {
    super(props);
    this.onAlbumTitleChange = this.onAlbumTitleChange.bind(this);
    this.onAlbumDescriptionChange = this.onAlbumDescriptionChange.bind(this);
    this.submitChanges = this.submitChanges.bind(this);

    this.state = {
      title: props.albumData.title,
      description: props.albumData.description
    };
  }

  onAlbumTitleChange(e) {
    this.setState({
      title: e.target.value
    });
  }

  onAlbumDescriptionChange(e) {
    this.setState({
      description: e.target.value
    });
  }

  renderActions() {
    return [
      <LinkLike
        key="Images"
        className="minibutton rounded"
        target={RoutingConfig.urlFor(AlbumItemsListPage, {
          page: 1,
          albumId: this.props.albumData.id
        })}
      >
        Images
      </LinkLike>,
      <LinkLike
        key="Delete"
        className="minibutton rounded danger"
        target={this.props.deleteAlbum}
      >
        Delete
      </LinkLike>
    ];
  }

  renderTitle() {
    return [
      <dt key="name">Title</dt>,
      <dd key="value">
        <input
          value={this.state.title}
          onChange={this.onAlbumTitleChange}
          className={
            this.state.title === this.props.albumData.title
              ? undefined
              : "changed"
          }
        />
      </dd>
    ];
  }

  renderDescription() {
    return [
      <dt key="name">Description</dt>,
      <dd key="value">
        <textarea
          value={nullAsEmptyString(this.state.description)}
          onChange={this.onAlbumDescriptionChange}
          style={{ width: 167 }}
          className={classNames({
            changed:
              nullAsEmptyString(this.state.description) !==
              nullAsEmptyString(this.props.albumData.description)
          })}
        />
      </dd>
    ];
  }

  renderId() {
    return [
      <dt key="name">ID</dt>,
      <dd key="value">{this.props.albumData.id}</dd>
    ];
  }

  submitChanges() {
    this.props.updateAlbum({
      id: this.props.albumData.id,
      title: this.state.title,
      description: this.state.description
    });
  }

  render() {
    return (
      <div className="tag-details album-details">
        <h1>Album Details: {this.props.albumData.title}</h1>
        {this.renderActions()}
        <dl className="tag-details-editor album-details-editor grid-editor">
          {this.renderTitle()}
          {this.renderDescription()}
          {this.renderId()}
          <button onClick={this.submitChanges}>Update</button>
        </dl>
      </div>
    );
  }
}
