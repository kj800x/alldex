import React, { Component } from "react";

import RoutingConfig from "../../routing/RoutingConfig";
import LinkLike from "../../library/linkLike/LinkLike";
import AlbumItemsListPage from "../pages/AlbumItemsListPage";
import AlbumSlideshowPage from "../pages/AlbumSlideshowPage";
import AlbumVideoPage from "../pages/AlbumVideoPage";

export default class AlbumLink extends Component {
  getListByAlbumPageUrl() {
    return;
  }

  render() {
    return (
      <>
        <LinkLike
          target={RoutingConfig.urlFor(AlbumItemsListPage, {
            albumId: this.props.albumData.id,
            page: 1
          })}
        >
          {this.props.albumData.title}
        </LinkLike>
        &nbsp;
        <LinkLike
          target={RoutingConfig.urlFor(AlbumSlideshowPage, {
            albumId: this.props.albumData.id,
            position: this.props.position
          })}
        >
          ({this.props.position})
        </LinkLike>
        &nbsp;
        <LinkLike
          target={RoutingConfig.urlFor(AlbumVideoPage, {
            albumId: this.props.albumData.id
          })}
        >
          (MV)
        </LinkLike>
      </>
    );
  }
}
