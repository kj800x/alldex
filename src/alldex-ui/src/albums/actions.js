import * as api from "./api";
import apiAction, { buildApiActions } from "../util/apiAction";
import { getAlbumId, getPosition } from "./selectors";

export const FETCH_ALBUM = buildApiActions("FETCH_ALBUM");
export const CREATE_ALBUM = buildApiActions("CREATE_ALBUM");
export const ASSOCIATE_ALBUM = buildApiActions("ASSOCIATE_ALBUM");
export const DISSOCIATE_ALBUM = buildApiActions("DISSOCIATE_ALBUM");
export const UPDATE_ALBUM = buildApiActions("UPDATE_ALBUM");
export const DELETE_ALBUM = buildApiActions("DELETE_ALBUM");
export const FETCH_ALL_ALBUMS = buildApiActions("FETCH_ALL_ALBUMS");
export const fetchAlbum = apiAction(FETCH_ALBUM, api.getAlbum);
export const fetchAllAlbums = apiAction(FETCH_ALL_ALBUMS, api.getAllAlbums);
export const createAlbum = apiAction(CREATE_ALBUM, api.createAlbum);
export const associateAlbum = apiAction(ASSOCIATE_ALBUM, api.associateAlbum);
export const dissociateAlbum = apiAction(DISSOCIATE_ALBUM, api.dissociateAlbum);
export const updateAlbum = apiAction(UPDATE_ALBUM, api.patchAlbum);
export const deleteAlbum = apiAction(DELETE_ALBUM, api.deleteAlbum);

export const ensureAlbumExists = album => (dispatch, getState) => {
  const state = getState();
  const albumId = getAlbumId(album)(state);
  if (albumId === -1) {
    return dispatch(createAlbum({ title: album }));
  }
};

export const enhancedDissociate = ({ album_id, item_id }) => (
  dispatch,
  getState
) => {
  const state = getState();
  const position = getPosition(album_id, item_id)(state);
  return dispatch(dissociateAlbum({ album_id, item_id, position }));
};

export const ensureAndAssociate = ({ albumValue, itemId }) => async (
  dispatch,
  getState
) => {
  await dispatch(ensureAlbumExists(albumValue));

  const albumId = getAlbumId(albumValue)(getState());
  await dispatch(associateAlbum({ album_id: albumId, item_id: itemId }));
};
