import React from "react";

import SearchContainer from "../../items/containers/SearchContainer";
import MergedVideoContainer from "../containers/MergedVideoContainer";

const AlbumVideoPage = ({
  match: {
    params: { albumId }
  }
}) => {
  return (
    <SearchContainer
      query={{
        filter: {
          AND: [{ albumId: parseInt(albumId, 10) }, { type: "VIDEO" }]
        },
        ordering: { albumId: parseInt(albumId, 10) },
        offset: 0,
        limit: 40
      }}
      renderResults={({ itemIds }) => (
        <MergedVideoContainer itemIds={itemIds} />
      )}
    />
  );
};

export default AlbumVideoPage;
