import React from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import SearchContainer from "../../items/containers/SearchContainer";
import ItemViewerContainer from "../../items/containers/ItemViewerContainer";
import AlbumSlideshowIndicatorWidgetContainer from "../containers/AlbumSlideshowIndicatorWidgetContainer";
import withSettings from "../../settings/withSettings";
import RoutingConfig from "../../routing/RoutingConfig";

const renderResults = (position, albumId) =>
  withSettings(
    connect(
      () => ({}),
      { push }
    )(({ push, autoplay, totalResultsCount, itemIds: [id] }) => (
      <ItemViewerContainer
        id={id}
        onNext={() => {
          autoplay &&
            position < totalResultsCount &&
            push(
              RoutingConfig.urlFor(AlbumSlideshowPage, {
                albumId,
                position: position + 1
              })
            );
        }}
        bannerHeader={() => (
          <AlbumSlideshowIndicatorWidgetContainer
            position={position}
            totalResultsCount={totalResultsCount}
            albumId={albumId}
          />
        )}
      />
    ))
  );

const AlbumSlideshowPage = ({
  match: {
    params: { albumId, position }
  }
}) => {
  return (
    <SearchContainer
      query={{
        filter: { albumId: parseInt(albumId, 10) },
        ordering: { albumId: parseInt(albumId, 10) },
        offset: parseInt(position, 10) - 1,
        limit: 1
      }}
      renderResults={renderResults(
        parseInt(position, 10),
        parseInt(albumId, 10)
      )}
    />
  );
};

export default AlbumSlideshowPage;
