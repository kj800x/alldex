import React from "react";

import AlbumDetailsContainer from "../containers/AlbumDetailsContainer";

export default ({
  match: {
    params: { albumId }
  }
}) => {
  return <AlbumDetailsContainer id={parseInt(albumId, 10)} />;
};
