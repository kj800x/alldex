import React from "react";

import SearchContainer from "../../items/containers/SearchContainer";

import PaginationWidget from "../../library/paginationWidget/PaginationWidget";

import { RESULTS_PER_PAGE } from "../../constants";
import RoutingConfig from "../../routing/RoutingConfig";
import ThumbPageAlbumHeaderContainer from "../containers/ThumbPageAlbumHeaderContainer";
import ItemSearch from "../../items/components/ItemSearch";

const AlbumItemsListPage = ({
  match: {
    params: { albumId, page }
  }
}) => {
  return (
    <SearchContainer
      query={{
        filter: { albumId: parseInt(albumId, 10) },
        ordering: { albumId: parseInt(albumId, 10) },
        offset: (parseInt(page, 10) - 1) * RESULTS_PER_PAGE,
        limit: RESULTS_PER_PAGE
      }}
      renderResults={({ itemIds, totalResultsCount }) => (
        <ItemSearch
          itemIds={itemIds}
          header={() => (
            <ThumbPageAlbumHeaderContainer id={parseInt(albumId, 10)} />
          )}
          controls={() => (
            <PaginationWidget
              mode={PaginationWidget.MODE.STANDARD}
              backButtonTarget={RoutingConfig.urlFor(AlbumItemsListPage, {
                albumId,
                page: parseInt(page) - 1
              })}
              nextButtonTarget={RoutingConfig.urlFor(AlbumItemsListPage, {
                albumId,
                page: parseInt(page) + 1
              })}
              currentPage={page}
              pageCount={Math.ceil(totalResultsCount / RESULTS_PER_PAGE)}
            />
          )}
        />
      )}
    />
  );
};

export default AlbumItemsListPage;
