import API from "../util/api";

const api = new API("/api/albums");

export const getAlbum = api.get(({ id }) => `/${id}`);
export const getAllAlbums = api.get();
export const createAlbum = api.post("", ({ title }) => ({
  title,
  description: ""
}));
export const associateAlbum = api.put(
  "/associate",
  ({ item_id, album_id }) => ({ item_id, album_id })
);
export const dissociateAlbum = api.delete(
  "/associate",
  ({ item_id, album_id }) => ({ item_id, album_id })
);
export const patchAlbum = api.patch(
  ({ id }) => `/${id}`,
  ({ id, title, description }) => ({ id, title, description })
);
export const deleteAlbum = api.delete(({ id }) => `/${id}`);
