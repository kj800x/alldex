import React from "react";
import LinkLike from "../library/linkLike/LinkLike";
import HomePage from "../home-page/HomePage";
import RandomItemsPage from "../items/pages/RandomItemsPage";
import RoutingConfig from "../routing/RoutingConfig";
import ImportPage from "../import/pages/ImportPage";
import SearchBar from "../library/searchBar/SearchBar";
import DuplicateItemsPage from "duplicates/pages/DuplicateItemsPage";

const Header = () => (
  <div className="header">
    <LinkLike
      className="minibutton rounded"
      target={RoutingConfig.urlFor(HomePage)}
    >
      <b>Alldex</b>
    </LinkLike>
    <SearchBar />
    <span style={{ float: "right" }}>
      <LinkLike
        className="minibutton rounded"
        target={RoutingConfig.urlFor(DuplicateItemsPage, { page: 1 })}
      >
        Dupls
      </LinkLike>
      <LinkLike
        className="minibutton rounded"
        target={RoutingConfig.urlFor(ImportPage)}
      >
        Import
      </LinkLike>
      <LinkLike
        className="minibutton rounded"
        target={RoutingConfig.urlFor(RandomItemsPage)}
      >
        Random
      </LinkLike>
    </span>
  </div>
);

export default Header;
