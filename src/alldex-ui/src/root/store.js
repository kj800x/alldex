import { createStore, applyMiddleware, compose } from "redux";
import { createBrowserHistory } from "history";
import { routerMiddleware } from "connected-react-router";
import thunk from "redux-thunk";

import createRootReducer from "./reducer";
import syncMiddleware from "../settings/syncMiddleware";

export const history = createBrowserHistory();

export default function configureStore() {
  if (window.__REDUX_DEVTOOLS_EXTENSION__) {
    return createStore(
      createRootReducer(history),
      compose(
        applyMiddleware(routerMiddleware(history)),
        applyMiddleware(thunk),
        applyMiddleware(syncMiddleware),
        window.__REDUX_DEVTOOLS_EXTENSION__ &&
          window.__REDUX_DEVTOOLS_EXTENSION__()
      )
    );
  } else {
    return createStore(
      createRootReducer(history),
      compose(
        applyMiddleware(routerMiddleware(history)),
        applyMiddleware(thunk),
        applyMiddleware(syncMiddleware)
      )
    );
  }
}
