import { combineReducers } from "redux";
import tagGroups from "tag-groups/reducer";
import items from "items/reducer";
import tags from "tags/reducer";
import albums from "albums/reducer";
import search from "items/searchReducer";
import importItems from "import/reducer";
import settings from "settings/reducer";
import duplicates from "duplicates/duplicateReducer";
import itemDuplicates from "duplicates/itemDuplicateReducer";
import { connectRouter } from "connected-react-router";

export default history =>
  combineReducers({
    tagGroups,
    items,
    tags,
    albums,
    search,
    import: importItems,
    router: connectRouter(history),
    duplicates,
    itemDuplicates,
    settings
  });
