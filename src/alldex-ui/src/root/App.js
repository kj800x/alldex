import React from "react";
import classNames from "classnames";
import { connect } from "react-redux";
import pipe from "transmute/pipe";
import { ApolloProvider } from "@apollo/react-hooks";

import apolloClient from "./apollo-client";
import RoutingConfig from "../routing/RoutingConfig";

import "./App.css";

const App = ({ inDarkMode, inFullscreen }) => (
  <div
    className={classNames({
      "dark-theme": inDarkMode,
      fullscreen: inFullscreen
    })}
  >
    <ApolloProvider client={apolloClient}>
      {RoutingConfig.allRoutes()}
    </ApolloProvider>
  </div>
);

function mapStateToProps(state) {
  return {
    inDarkMode: state.settings.inDarkMode,
    inFullscreen: state.settings.inFullscreen
  };
}

export default pipe(
  // withAllEntity("tagGroups", "_taggroupdata", fetchAllTagGroups),
  // withAllEntity("tags", "_tagdata", fetchAllTags),
  // withAllEntity("albums", "_albumdata", fetchAllAlbums),
  connect(mapStateToProps)
  // withRouter
)(App);
