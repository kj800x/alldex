import * as api from "./api";
import apiAction, { buildApiActions } from "../util/apiAction";

export const IMPORT_ITEMS = buildApiActions("IMPORT_ITEMS");
export const importItems = apiAction(IMPORT_ITEMS, api.importItems);

export const UPDATE_IMPORT_URLS = "UPDATE_IMPORT_URLS";
export const UPDATE_IMPORT_TAGALBUMS = "UPDATE_IMPORT_TAGALBUMS";

export const RESET_IMPORT_FORM = "RESET_IMPORT_FORM";
