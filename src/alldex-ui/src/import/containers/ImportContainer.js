import React, { Component } from "react";
import { createAction } from "redux-actions";
import {
  SUCCEEDED,
  FAILED,
  UNINITIALIZED,
  STARTED
} from "../../util/fetchStatus";
import { connect } from "react-redux";
import LoadingOverlay from "../../library/loadingOverlay/LoadingOverlay";

import { importItems } from "../actions";
import {
  UPDATE_IMPORT_URLS,
  UPDATE_IMPORT_TAGALBUMS,
  RESET_IMPORT_FORM
} from "../actions";

import ImportForm from "../components/ImportForm";
import RoutingConfig from "../../routing/RoutingConfig";
import TagItemsListPage from "../../tags/pages/TagItemsListPage";

class ImportContainer extends Component {
  constructor(props) {
    super(props);
    this.importItems = this.importItems.bind(this);
    this.props.reset();
  }

  getTags() {
    return this.props.tagAlbums
      .split("\n")
      .filter(e => e && !e.startsWith("album:"));
  }

  getAlbums() {
    return this.props.tagAlbums
      .split("\n")
      .filter(e => e.startsWith("album:"))
      .map(e => e.replace("album:", ""));
  }

  getSources() {
    return this.props.importUrls.split("\n").filter(e => e);
  }

  importItems() {
    this.props
      .importItems({
        data: {
          sources: this.getSources(),
          tags: this.getTags(),
          albums: this.getAlbums()
        }
      })
      .then(response => {
        this.props.history.replace(
          RoutingConfig.urlFor(TagItemsListPage, {
            tagId: response.import_tag,
            page: 1
          })
        );
      });
  }

  render() {
    switch (this.props.fetchStatus) {
      case UNINITIALIZED:
        return <ImportForm {...this.props} importItems={this.importItems} />;
      case SUCCEEDED:
        return null; // We should automatically be redirected.
      case FAILED:
        return (
          <div>
            {" "}
            Something failed... Check the redux store for failed requests{" "}
          </div>
        );
      case STARTED:
      default:
        return <LoadingOverlay message="Importing..." />;
    }
  }
}

function mapStateToProps(state) {
  return {
    tagAlbums: state.import.importTagsAndAlbums,
    importUrls: state.import.importUrls,
    fetchStatus: state.import.fetchStatus
  };
}

function mapDispatchToProps(dispatch) {
  return {
    importItems: data => dispatch(importItems(data)),
    updateTagAlbums: ({ target: { value } }) =>
      dispatch(createAction(UPDATE_IMPORT_TAGALBUMS)({ text: value })),
    updateUrls: ({ target: { value } }) =>
      dispatch(createAction(UPDATE_IMPORT_URLS)({ text: value })),
    reset: () => dispatch(createAction(RESET_IMPORT_FORM)())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ImportContainer);
