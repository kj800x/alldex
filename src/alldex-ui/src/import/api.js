import API from "../util/api";

const api = new API("/api/items");

export const importItems = api.put("/import", ({ data }) => data);
