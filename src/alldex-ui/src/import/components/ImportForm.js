import React from "react";

import "./ImportForm.css";

const ImportForm = ({
  importItems,
  importUrls,
  tagAlbums,
  updateTagAlbums,
  updateUrls
}) => {
  return (
    <div className="import-form">
      <h2>Import Items</h2>
      <h5>Sources</h5>
      <textarea value={importUrls} onChange={updateUrls} />
      <h5>Tag and Albums</h5>
      <textarea value={tagAlbums} onChange={updateTagAlbums} />
      <br />
      <button onClick={importItems}>Import</button>
    </div>
  );
};

export default ImportForm;
