import {
  IMPORT_ITEMS,
  UPDATE_IMPORT_URLS,
  UPDATE_IMPORT_TAGALBUMS,
  RESET_IMPORT_FORM
} from "./actions";
import { handleActions } from "redux-actions";
import { STARTED, SUCCEEDED, FAILED, UNINITIALIZED } from "../util/fetchStatus";

import setIn from "transmute/setIn";

const defaultState = {
  importUrls: "",
  importTagsAndAlbums: "",
  fetchStatus: UNINITIALIZED
};

export default handleActions(
  {
    [IMPORT_ITEMS.STARTED]: state => {
      return setIn(["fetchStatus"], STARTED)(state);
    },
    [IMPORT_ITEMS.SUCCEEDED]: state => {
      return {
        fetchStatus: SUCCEEDED,
        importUrls: "",
        importTagsAndAlbums: ""
      };
    },
    [IMPORT_ITEMS.FAILED]: state => {
      return setIn(["fetchStatus"], FAILED)(state);
    },
    [UPDATE_IMPORT_URLS]: (state, { payload: { text } }) => {
      return setIn(["importUrls"], text)(state);
    },
    [UPDATE_IMPORT_TAGALBUMS]: (state, { payload: { text } }) => {
      return setIn(["importTagsAndAlbums"], text)(state);
    },
    [RESET_IMPORT_FORM]: () => defaultState
  },
  defaultState
);
