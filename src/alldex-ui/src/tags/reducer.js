import {
  FETCH_TAG,
  CREATE_TAG,
  UPDATE_TAG,
  DELETE_TAG,
  FETCH_ALL_TAGS
} from "./actions";
import { DELETE_TAG_GROUP } from "../tag-groups/actions";
import { handleActions } from "redux-actions";
import { STARTED, SUCCEEDED, FAILED, UNINITIALIZED } from "../util/fetchStatus";

import updateIn from "transmute/updateIn";
import setIn from "transmute/setIn";
import pipe from "transmute/pipe";
import filter from "transmute/filter";
import map from "transmute/map";

const defaultState = {
  fetchStatus: {},
  fetchAllStatus: UNINITIALIZED,
  data: {}
};

export default handleActions(
  {
    [FETCH_TAG.STARTED]: (
      state,
      {
        payload: {
          args: { id }
        }
      }
    ) => {
      return setIn(["fetchStatus", id], STARTED)(state);
    },
    [FETCH_TAG.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id },
          response
        }
      }
    ) => {
      return pipe(
        setIn(["fetchStatus", id], SUCCEEDED),
        setIn(["data", id], response)
      )(state);
    },
    [FETCH_TAG.FAILED]: (
      state,
      {
        payload: {
          args: { id }
        }
      }
    ) => {
      return setIn(["fetchStatus", id], FAILED)(state);
    },
    [FETCH_ALL_TAGS.STARTED]: state => {
      return setIn(["fetchAllStatus"], STARTED)(state);
    },
    [FETCH_ALL_TAGS.FAILED]: state => {
      return setIn(["fetchAllStatus"], FAILED)(state);
    },
    [FETCH_ALL_TAGS.SUCCEEDED]: (state, { payload: { response } }) => {
      return pipe(
        setIn(["fetchAllStatus"], SUCCEEDED),
        setIn(
          ["data"],
          response.reduce(
            (acc, elem) => Object.assign(acc, { [elem.id]: elem }),
            {}
          )
        )
      )(state);
    },
    [CREATE_TAG.SUCCEEDED]: (state, { payload: { response } }) => {
      const { id } = response;
      return pipe(
        setIn(["fetchStatus", id], SUCCEEDED),
        setIn(["data", id], response)
      )(state);
    },
    [UPDATE_TAG.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id, value, description, taggroup_id }
        }
      }
    ) => {
      return setIn(["data", id], { id, value, description, taggroup_id })(
        state
      );
    },
    [DELETE_TAG.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id }
        }
      }
    ) => {
      return pipe(
        updateIn(["fetchStatus"], filter((_value, key) => key !== `${id}`)),
        updateIn(["data"], filter((_value, key) => key !== `${id}`))
      )(state);
    },
    [DELETE_TAG_GROUP.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id, default_group }
        }
      }
    ) => {
      return updateIn(
        ["data"],
        map(
          updateIn(["taggroup_id"], oldtaggroup_id =>
            oldtaggroup_id === id ? default_group : oldtaggroup_id
          )
        )
      )(state);
    }
  },
  defaultState
);
