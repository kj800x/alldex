import React from "react";

import TagDetailsContainer from "../containers/TagDetailsContainer";

export default ({
  match: {
    params: { tagId }
  }
}) => {
  return <TagDetailsContainer id={parseInt(tagId, 10)} />;
};
