import React from "react";

import SearchContainer from "../../items/containers/SearchContainer";
import PaginationWidget from "../../library/paginationWidget/PaginationWidget";
import ThumbPageTagHeaderContainer from "../containers/ThumbPageTagHeaderContainer";
import ItemSearch from "../../items/components/ItemSearch";

import { RESULTS_PER_PAGE } from "../../constants";
import RoutingConfig from "../../routing/RoutingConfig";

const TagItemsListPage = ({
  match: {
    params: { tagId, page }
  }
}) => {
  return (
    <SearchContainer
      query={{
        filter: { tagId: parseInt(tagId, 10) },
        ordering: "desc",
        offset: (parseInt(page, 10) - 1) * RESULTS_PER_PAGE,
        limit: RESULTS_PER_PAGE
      }}
      renderResults={({ itemIds, totalResultsCount }) => (
        <ItemSearch
          itemIds={itemIds}
          header={() => (
            <ThumbPageTagHeaderContainer id={parseInt(tagId, 10)} />
          )}
          controls={() => (
            <PaginationWidget
              mode={PaginationWidget.MODE.STANDARD}
              backButtonTarget={RoutingConfig.urlFor(TagItemsListPage, {
                tagId,
                page: parseInt(page) - 1
              })}
              nextButtonTarget={RoutingConfig.urlFor(TagItemsListPage, {
                tagId,
                page: parseInt(page) + 1
              })}
              currentPage={page}
              pageCount={Math.ceil(totalResultsCount / RESULTS_PER_PAGE)}
            />
          )}
        />
      )}
    />
  );
};

export default TagItemsListPage;
