import * as api from "./api";
import * as albumActions from "../albums/actions";
import apiAction, { buildApiActions } from "../util/apiAction";
import { getTagId } from "./selectors";
import { ALBUM_PREFIX } from "../albums/constants";

const stripPrefix = prefix => value => value.substring(prefix.length);

export const FETCH_TAG = buildApiActions("FETCH_TAG");
export const FETCH_ALL_TAGS = buildApiActions("FETCH_ALL_TAGS");
export const CREATE_TAG = buildApiActions("CREATE_TAG");
export const ASSOCIATE_TAG = buildApiActions("ASSOCIATE_TAG");
export const DISSOCIATE_TAG = buildApiActions("DISSOCIATE_TAG");
export const UPDATE_TAG = buildApiActions("UPDATE_TAG");
export const DELETE_TAG = buildApiActions("DELETE_TAG");
export const fetchTag = apiAction(FETCH_TAG, api.getTag);
export const fetchAllTags = apiAction(FETCH_ALL_TAGS, api.getAllTags);
export const createTag = apiAction(CREATE_TAG, api.createTag);
export const associateTag = apiAction(ASSOCIATE_TAG, api.associateTag);
export const dissociateTag = apiAction(DISSOCIATE_TAG, api.dissociateTag);
export const updateTag = apiAction(UPDATE_TAG, api.patchTag);
export const deleteTag = apiAction(DELETE_TAG, api.deleteTag);

export const ensureTagExists = tag => (dispatch, getState) => {
  const state = getState();
  const tagId = getTagId(tag)(state);
  if (tagId === -1) {
    return dispatch(createTag({ value: tag }));
  }
};

export const ensureAndAssociate = ({ tagValue, itemId }) => async (
  dispatch,
  getState
) => {
  if (tagValue.startsWith(ALBUM_PREFIX)) {
    await dispatch(
      albumActions.ensureAndAssociate({
        albumValue: stripPrefix(ALBUM_PREFIX)(tagValue),
        itemId
      })
    );
    return;
  }

  await dispatch(ensureTagExists(tagValue));

  const tagId = getTagId(tagValue)(getState());
  await dispatch(associateTag({ tag_id: tagId, item_id: itemId }));
};
