import React, { Component } from "react";

import RoutingConfig from "../../routing/RoutingConfig";
import LinkLike from "../../library/linkLike/LinkLike";
import TagItemsListPage from "../pages/TagItemsListPage";

export default class TagLink extends Component {
  getListByTagPageUrl() {
    return RoutingConfig.urlFor(TagItemsListPage, {
      tagId: this.props.tagData.id,
      page: 1
    });
  }

  render() {
    return (
      <LinkLike
        target={this.getListByTagPageUrl()}
        style={{ color: "#" + this.props.tagGroupData.color }}
      >
        {this.props.tagData.value}
      </LinkLike>
    );
  }
}
