import React, { Component } from "react";
import "./InlineTag.css";

import TagLink from "./TagLink";

export default class InlineTag extends Component {
  render() {
    if (!this.props.showHidden && this.props.tagGroupData.isHidden) {
      return null;
    }

    return (
      <span className="inline-tag">
        <TagLink
          tagData={this.props.tagData}
          tagGroupData={this.props.tagGroupData}
        />
      </span>
    );
  }
}

InlineTag.defaultProps = {
  editingEnabled: false,
  showHidden: false
};
