import React, { Component } from "react";

import LinkLike from "../../library/linkLike/LinkLike";
import RoutingConfig from "../../routing/RoutingConfig";
import TagDetailsPage from "../pages/TagDetailsPage";
import SearchSlideshowPage from "../../items/pages/SearchSlideshowPage";

import "./ThumbPageTagHeader.css";

export default class ThumbPageTagHeader extends Component {
  getListByTagPageUrl() {
    return RoutingConfig.urlFor(TagDetailsPage, {
      tagId: this.props.tagData.id
    });
  }

  render() {
    return (
      <div>
        <h2 className="thumb-page-header">
          Tag:&nbsp;
          <LinkLike
            target={this.getListByTagPageUrl()}
            style={{ color: this.props.tagGroupData.color }}
          >
            {this.props.tagData.value}
          </LinkLike>{" "}
          (
          <LinkLike
            target={RoutingConfig.urlFor(SearchSlideshowPage, {
              query: '"' + this.props.tagData.value + '"',
              position: 1
            })}
          >
            Slides
          </LinkLike>
          )
        </h2>
        {this.props.tagData.description && (
          <div className="thumb-page-tag-header-description">
            {this.props.tagData.description}
          </div>
        )}
      </div>
    );
  }
}
