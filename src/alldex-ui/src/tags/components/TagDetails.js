import React, { Component } from "react";

import RoutingConfig from "../../routing/RoutingConfig";
import LinkLike from "../../library/linkLike/LinkLike";
import TagItemsListPage from "../pages/TagItemsListPage";
import TagGroupsListPage from "../../tag-groups/pages/TagGroupListPage";
import classNames from "classnames";

import "./TagDetails.css";

const nullAsEmptyString = e => e || "";
const TagMergePage = null; // TODO

export default class TagDetails extends Component {
  constructor(props) {
    super(props);
    this.onTagValueChange = this.onTagValueChange.bind(this);
    this.onTagGroupChange = this.onTagGroupChange.bind(this);
    this.onTagDescriptionChange = this.onTagDescriptionChange.bind(this);
    this.submitChanges = this.submitChanges.bind(this);

    this.state = {
      value: props.tagData.value,
      tagGroupId: props.tagData.tagGroup.id,
      hidden: props.tagData.hidden,
      description: props.tagData.description,
      groupColor: Object.values(props.tagGroups).filter(function(group) {
        return props.tagData.tagGroup.id === group.id;
      })[0].color
    };
  }

  onTagValueChange(e) {
    this.setState({
      value: e.target.value
    });
  }

  onTagDescriptionChange(e) {
    this.setState({
      description: e.target.value
    });
  }

  onTagGroupChange(e) {
    const newId = parseInt(e.target.value, 10);
    this.setState({
      tagGroupId: newId,
      groupColor: Object.values(this.props.tagGroups).find(g => newId === g.id)
        .color
    });
  }

  renderTagGroupOption(group) {
    return (
      <option
        key={group.id}
        value={group.id}
        style={{ color: "#" + group.color }}
      >
        {group.value}
      </option>
    );
  }

  renderActions() {
    return [
      <LinkLike
        key="Images"
        className="minibutton rounded"
        target={RoutingConfig.urlFor(TagItemsListPage, {
          page: 1,
          tagId: this.props.tagData.id
        })}
      >
        Images
      </LinkLike>,
      <LinkLike
        key="Merge"
        className="minibutton rounded"
        target={RoutingConfig.urlFor(TagMergePage, {
          tagNameB: this.props.tagData.value,
          tagNameA: ""
        })}
      >
        Merge
      </LinkLike>,
      <LinkLike
        key="Delete"
        className="minibutton rounded danger"
        target={this.props.deleteTag}
      >
        Delete
      </LinkLike>
    ];
  }

  renderValue() {
    return [
      <dt key="name">Value</dt>,
      <dd key="value">
        <input
          value={this.state.value}
          onChange={this.onTagValueChange}
          className={
            this.state.value === this.props.tagData.value
              ? undefined
              : "changed"
          }
        />
      </dd>
    ];
  }

  renderGroup() {
    return [
      <dt key="name">
        Group [
        <LinkLike target={RoutingConfig.urlFor(TagGroupsListPage, {})}>
          see all
        </LinkLike>
        ]
      </dt>,
      <dd key="value">
        <select
          style={{ color: "#" + this.state.groupColor }}
          value={this.state.tagGroupId}
          className={classNames({
            changed: this.state.tagGroupId !== this.props.tagData.tagGroup.id
          })}
          onChange={this.onTagGroupChange}
        >
          {this.getTagGroupOptions()}
        </select>
      </dd>
    ];
  }

  renderDescription() {
    return [
      <dt key="name">Description</dt>,
      <dd key="value">
        <textarea
          value={nullAsEmptyString(this.state.description)}
          onChange={this.onTagDescriptionChange}
          style={{ width: 167 }}
          className={classNames({
            changed:
              nullAsEmptyString(this.state.description) !==
              nullAsEmptyString(this.props.tagData.description)
          })}
        />
      </dd>
    ];
  }

  renderId() {
    return [
      <dt key="name">ID</dt>,
      <dd key="value">{this.props.tagData.id}</dd>
    ];
  }

  getTagGroupOptions() {
    return Object.values(this.props.tagGroups).map(this.renderTagGroupOption);
  }

  submitChanges() {
    this.props.updateTag({
      id: this.props.tagData.id,
      value: this.state.value,
      description: this.state.description,
      tagGroupId: this.state.tagGroupId
    });
  }

  render() {
    return (
      <div className="tag-details">
        <h1>Tag Details: {this.props.tagData.value}</h1>
        {this.renderActions()}
        <dl className="tag-details-editor grid-editor">
          {this.renderValue()}
          {this.renderGroup()}
          {this.renderDescription()}
          {this.renderId()}
          <button onClick={this.submitChanges}>Update</button>
        </dl>
      </div>
    );
  }
}
