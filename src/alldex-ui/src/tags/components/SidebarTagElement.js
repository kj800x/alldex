import React, { Component } from "react";
import RoutingConfig from "../../routing/RoutingConfig";
import TagDetailsPage from "../pages/TagDetailsPage";
import LinkLike from "../../library/linkLike/LinkLike";
import TagLink from "./TagLink";

import "./SidebarTagElement.css";

export default class SidebarTagElement extends Component {
  getTagDetailsPage() {
    return RoutingConfig.urlFor(TagDetailsPage, {
      tagId: this.props.tagData.id
    });
  }

  renderRemoveButton() {
    if (this.props.editingEnabled) {
      return [
        <span key="spacing">&nbsp;</span>,
        <button
          key="button"
          onClick={this.props.onTagRemove.bind(this, this.props.tagData.id)}
        >
          -
        </button>
      ];
    }
    return null;
  }

  render() {
    if (!this.props.showHidden && this.props.tagGroupData.isHidden) {
      return null;
    }

    return (
      <div className="tag">
        <LinkLike
          className="tag-details-link"
          style={{ color: "#" + this.props.tagGroupData.color }}
          target={this.getTagDetailsPage()}
        >
          ?
        </LinkLike>
        &nbsp;
        <TagLink
          tagData={this.props.tagData}
          tagGroupData={this.props.tagGroupData}
        />
        {this.renderRemoveButton()}
      </div>
    );
  }
}

SidebarTagElement.defaultProps = {
  editingEnabled: false,
  showHidden: false
};
