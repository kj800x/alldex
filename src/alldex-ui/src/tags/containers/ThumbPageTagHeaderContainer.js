import ThumbPageTagHeader from "../components/ThumbPageTagHeader";
import withEntity from "../../util/withEntity/withEntity";

import { fetchTag } from "../actions";
import { fetchTagGroup } from "../../tag-groups/actions";

export default withEntity("tags", "tagData", fetchTag, ({ id }) => id)(
  withEntity(
    "tagGroups",
    "tagGroupData",
    fetchTagGroup,
    ({ tagData: { taggroup_id } }) => taggroup_id
  )(ThumbPageTagHeader)
);
