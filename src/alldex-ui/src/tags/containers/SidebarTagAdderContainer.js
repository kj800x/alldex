import { connect } from "react-redux";

import SubmitInput from "../../library/submitInput/SubmitInput";
import { ensureAndAssociate } from "../actions";

const mapStateToProps = state => ({});

const mapDispatchToProps = (dispatch, props) => ({
  onSubmit: value =>
    dispatch(ensureAndAssociate({ tagValue: value, itemId: props.itemId }))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SubmitInput);
