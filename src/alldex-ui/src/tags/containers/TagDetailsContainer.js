import React from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import TagDetails from "../components/TagDetails";

const FETCH_TAG = gql`
  query tag($id: Int) {
    getTag(id: $id) {
      id
      __typename
      isDeleted
      value
      description
      tagGroup {
        id
        __typename
      }
    }
    getTagGroups {
      __typename
      id
      value
      color
      isHidden
      special
    }
  }
`;

const UPDATE_TAG = gql`
  mutation upsertTag($tag: TagInput) {
    upsertTag(tag: $tag) {
      __typename
      id
      isDeleted
      value
      description
      tagGroup {
        id
        __typename
      }
    }
  }
`;

export default function TagDetailsContainer({ id }) {
  const { loading, error, data } = useQuery(FETCH_TAG, { variables: { id } });
  const [updateTag] = useMutation(UPDATE_TAG);
  const [deleteTag] = useMutation(UPDATE_TAG, {
    variables: { tag: { id, isDeleted: true } },
    onCompleted: () => {
      alert("tag deleted");
    }
  });

  if (loading) return <span>Tag Details Loading...</span>;
  if (error) return <span>Tag Details Query Error :(</span>;

  return (
    <TagDetails
      tagData={data.getTag}
      tagGroups={data.getTagGroups}
      updateTag={patch => updateTag({ variables: { tag: { id, ...patch } } })}
      deleteTag={deleteTag}
    />
  );
}
