import { connect } from "react-redux";

import SidebarTagElement from "../components/SidebarTagElement";
import withEntity from "../../util/withEntity/withEntity";

import { dissociateTag } from "../actions";

import { fetchTag } from "../actions";
import { fetchTagGroup } from "../../tag-groups/actions";

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch, props) => ({
  onTagRemove: tagId =>
    dispatch(dissociateTag({ item_id: props.itemId, tag_id: tagId }))
});

export default withEntity("tags", "tagData", fetchTag, ({ id }) => id)(
  withEntity(
    "tagGroups",
    "tagGroupData",
    fetchTagGroup,
    ({ tagData: { taggroup_id } }) => taggroup_id
  )(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(SidebarTagElement)
  )
);
