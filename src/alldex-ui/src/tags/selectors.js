export const getTagId = name => state =>
  parseInt(
    Object.keys(state.tags.data).find(id => state.tags.data[id].value === name)
  ) || -1;
