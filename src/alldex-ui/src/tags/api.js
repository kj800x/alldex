import API from "../util/api";

const api = new API("/api/tags");

export const getTag = api.get(({ id }) => `/${id}`);
export const getAllTags = api.get();
export const createTag = api.post("", ({ value }) => ({ value }));
export const associateTag = api.put("/associate", ({ tag_id, item_id }) => ({
  tag_id,
  item_id
}));
export const dissociateTag = api.delete(
  "/associate",
  ({ tag_id, item_id }) => ({ tag_id, item_id })
);
export const patchTag = api.patch(
  ({ id }) => `/${id}`,
  ({ id, value, description, taggroup_id }) => ({
    id,
    value,
    description,
    taggroup_id
  })
);
export const deleteTag = api.delete(({ id }) => `/${id}`);
