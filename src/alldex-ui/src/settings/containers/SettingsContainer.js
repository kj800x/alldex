import { connect } from "react-redux";

import {
  setInDarkMode,
  setInEditMode,
  setInFullscreen,
  setShowHidden,
  setInDetailsMode,
  setTypeFilter,
  setAndFilter,
  setAutoplay
} from "../actions";

import SettingsPanel from "../components/SettingsPanel";

function mapStateToProps(state) {
  return {
    inDarkMode: state.settings.inDarkMode,
    inFullscreen: state.settings.inFullscreen,
    inEditMode: state.settings.inEditMode,
    showHidden: state.settings.showHidden,
    inDetailsMode: state.settings.inDetailsMode,
    andFilter: state.settings.andFilter,
    typeFilters: state.settings.typeFilters,
    autoplay: state.settings.autoplay
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setInDarkMode: ({ target: { checked } }) =>
      dispatch(setInDarkMode(checked)),
    setInEditMode: ({ target: { checked } }) =>
      dispatch(setInEditMode(checked)),
    setInFullscreen: ({ target: { checked } }) =>
      dispatch(setInFullscreen(checked)),
    setShowHidden: ({ target: { checked } }) =>
      dispatch(setShowHidden(checked)),
    setInDetailsMode: ({ target: { checked } }) =>
      dispatch(setInDetailsMode(checked)),
    setTypeFilter: (type, value) => dispatch(setTypeFilter(type, value)),
    setAndFilter: ({ target: { value } }) => dispatch(setAndFilter(value)),
    setAutoplay: ({ target: { checked } }) => dispatch(setAutoplay(checked))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsPanel);
