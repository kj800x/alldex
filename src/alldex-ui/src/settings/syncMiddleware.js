const CommonBroadcastChannel = new BroadcastChannel("common");

export default store => {
  CommonBroadcastChannel.onmessage = msg =>
    store.dispatch(JSON.parse(msg.data));

  return next => action => {
    if (action.meta && action.meta.instanceSync) {
      CommonBroadcastChannel.postMessage(
        JSON.stringify({ type: action.type, payload: action.payload })
      );
    }
    return next(action);
  };
};
