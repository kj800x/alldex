import { handleActions } from "redux-actions";
import Cookies from "cookies-js";

import setIn from "transmute/setIn";

import {
  SET_DARK_MODE,
  SET_FULLSCREEN,
  SET_EDIT_MODE,
  SET_SHOW_HIDDEN,
  SET_DETAILS_MODE,
  SET_TYPE_FILTER,
  SET_AND_FILTER,
  SET_AUTOPLAY
} from "./actionTypes";

const getBooleanCookie = (key, deefault) =>
  Cookies.get(key) === undefined ? deefault : Cookies.get(key) === "true";
const getStringCookie = (key, deefault) =>
  Cookies.get(key) === undefined ? deefault : Cookies.get(key);

const defaultState = {
  inDarkMode: getBooleanCookie("inDarkMode", false),
  inFullscreen: getBooleanCookie("inFullscreen", false),
  inEditMode: getBooleanCookie("inEditMode", false),
  showHidden: getBooleanCookie("showHidden", false),
  inDetailsMode: getBooleanCookie("showHidden", false),
  autoplay: getBooleanCookie("autoplay", true),
  andFilter: getStringCookie("andFilter", ""),
  typeFilters: {
    image: getBooleanCookie("typeFilters.image", true),
    video: getBooleanCookie("typeFilters.video", true),
    text: getBooleanCookie("typeFilters.text", true),
    url: getBooleanCookie("typeFilters.url", true),
    imageset: getBooleanCookie("typeFilters.imageset", true)
  }
};

// TODO should not have side effects in reducers. Should instead refactor this into a middleware triggered by a meta
export default handleActions(
  {
    [SET_DARK_MODE]: (state, { payload: { inDarkMode } }) => {
      Cookies.set("inDarkMode", inDarkMode);

      return setIn(["inDarkMode"], inDarkMode)(state);
    },
    [SET_DETAILS_MODE]: (state, { payload: { inDetailsMode } }) => {
      Cookies.set("inDetailsMode", inDetailsMode);

      return setIn(["inDetailsMode"], inDetailsMode)(state);
    },
    [SET_FULLSCREEN]: (state, { payload: { inFullscreen } }) => {
      Cookies.set("inFullscreen", inFullscreen);

      return setIn(["inFullscreen"], inFullscreen)(state);
    },
    [SET_EDIT_MODE]: (state, { payload: { inEditMode } }) => {
      Cookies.set("inEditMode", inEditMode);

      return setIn(["inEditMode"], inEditMode)(state);
    },
    [SET_SHOW_HIDDEN]: (state, { payload: { showHidden } }) => {
      Cookies.set("showHidden", showHidden);

      return setIn(["showHidden"], showHidden)(state);
    },
    [SET_TYPE_FILTER]: (state, { payload: { type, value } }) => {
      Cookies.set(`typeFilters.${type}`, value);

      return setIn(["typeFilters", type], value)(state);
    },
    [SET_AND_FILTER]: (state, { payload: { value } }) => {
      Cookies.set("andFilter", value);

      return setIn(["andFilter"], value)(state);
    },
    [SET_AUTOPLAY]: (state, { payload: { value } }) => {
      Cookies.set("autoplay", value);

      return setIn(["autoplay"], value)(state);
    }
  },
  defaultState
);
