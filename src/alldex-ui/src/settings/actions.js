import { createAction } from "redux-actions";

import {
  SET_DARK_MODE,
  SET_EDIT_MODE,
  SET_FULLSCREEN,
  SET_SHOW_HIDDEN,
  SET_DETAILS_MODE,
  SET_TYPE_FILTER,
  SET_AND_FILTER,
  SET_AUTOPLAY
} from "./actionTypes";

export const setInDarkMode = createAction(
  SET_DARK_MODE,
  inDarkMode => ({ inDarkMode }),
  () => ({ instanceSync: true })
);
export const setInFullscreen = createAction(
  SET_FULLSCREEN,
  inFullscreen => ({ inFullscreen }),
  () => ({ instanceSync: true })
);
export const setInEditMode = createAction(
  SET_EDIT_MODE,
  inEditMode => ({ inEditMode }),
  () => ({ instanceSync: true })
);
export const setShowHidden = createAction(
  SET_SHOW_HIDDEN,
  showHidden => ({ showHidden }),
  () => ({ instanceSync: true })
);
export const setInDetailsMode = createAction(
  SET_DETAILS_MODE,
  inDetailsMode => ({ inDetailsMode }),
  () => ({ instanceSync: true })
);
export const setTypeFilter = createAction(
  SET_TYPE_FILTER,
  (type, value) => ({ type, value }),
  () => ({ instanceSync: true })
);
export const setAndFilter = createAction(
  SET_AND_FILTER,
  value => ({ value }),
  () => ({ instanceSync: true })
);
export const setAutoplay = createAction(
  SET_AUTOPLAY,
  value => ({ value }),
  () => ({ instanceSync: true })
);
