import { connect } from "react-redux";

function mapStateToProps(state) {
  return {
    inDarkMode: state.settings.inDarkMode,
    inFullscreen: state.settings.inFullscreen,
    inEditMode: state.settings.inEditMode,
    inDetailsMode: state.settings.inDetailsMode,
    showHidden: state.settings.showHidden,
    andFilter: state.settings.andFilter,
    typeFilters: state.settings.typeFilters,
    autoplay: state.settings.autoplay
  };
}

export default connect(mapStateToProps);
