import React from "react";

// TODO fix this, it shouldn't be done with tables
const SettingsPanel = props => (
  <div>
    <table>
      <tbody>
        <tr>
          <td>Dark Mode:</td>
          <td>
            <input
              type="checkbox"
              onChange={props.setInDarkMode}
              checked={props.inDarkMode}
            />
          </td>
        </tr>
        <tr>
          <td>Fullscreen:</td>
          <td>
            <input
              type="checkbox"
              onChange={props.setInFullscreen}
              checked={props.inFullscreen}
            />
          </td>
        </tr>
        <tr>
          <td>Edit Mode:</td>
          <td>
            <input
              type="checkbox"
              onChange={props.setInEditMode}
              checked={props.inEditMode}
            />
          </td>
        </tr>
        <tr>
          <td>Show Hidden:</td>
          <td>
            <input
              type="checkbox"
              onChange={props.setShowHidden}
              checked={props.showHidden}
            />
          </td>
        </tr>
        <tr>
          <td>Autoplay:</td>
          <td>
            <input
              type="checkbox"
              onChange={props.setAutoplay}
              checked={props.autoplay}
            />
          </td>
        </tr>
        <tr>
          <td>Details Mode:</td>
          <td>
            <input
              type="checkbox"
              onChange={props.setInDetailsMode}
              checked={props.inDetailsMode}
            />
          </td>
        </tr>
        <tr>
          <td>Types:</td>
          <td>
            <table>
              <tbody>
                <tr>
                  <td>Image</td>
                  <td>
                    <input
                      type="checkbox"
                      checked={props.typeFilters["image"]}
                      onChange={() =>
                        props.setTypeFilter(
                          "image",
                          !props.typeFilters["image"]
                        )
                      }
                    />
                  </td>
                </tr>
                <tr>
                  <td>Video</td>
                  <td>
                    <input
                      type="checkbox"
                      checked={props.typeFilters["video"]}
                      onChange={() =>
                        props.setTypeFilter(
                          "video",
                          !props.typeFilters["video"]
                        )
                      }
                    />
                  </td>
                </tr>
                <tr>
                  <td>Text</td>
                  <td>
                    <input
                      type="checkbox"
                      checked={props.typeFilters["text"]}
                      onChange={() =>
                        props.setTypeFilter("text", !props.typeFilters["text"])
                      }
                    />
                  </td>
                </tr>
                <tr>
                  <td>Url</td>
                  <td>
                    <input
                      type="checkbox"
                      checked={props.typeFilters["url"]}
                      onChange={() =>
                        props.setTypeFilter("url", !props.typeFilters["url"])
                      }
                    />
                  </td>
                </tr>
                <tr>
                  <td>ImageSet</td>
                  <td>
                    <input
                      type="checkbox"
                      checked={props.typeFilters["imageset"]}
                      onChange={() =>
                        props.setTypeFilter(
                          "imageset",
                          !props.typeFilters["imageset"]
                        )
                      }
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td>AND Filter:</td>
          <td>
            <input onChange={props.setAndFilter} value={props.andFilter} />
          </td>
        </tr>
      </tbody>
    </table>
  </div>
);

export default SettingsPanel;
