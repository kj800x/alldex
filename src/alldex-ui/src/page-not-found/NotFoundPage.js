import React, { Component } from "react";
import "./NotFoundPage.css";

export default class NotFoundPage extends Component {
  render() {
    return (
      <div className="page-not-found-page">
        <h1>Page Not Found</h1>
      </div>
    );
  }
}
