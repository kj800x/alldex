import React from "react";

import Header from "../header";
import Footer from "../footer";

const Layout = ({ children }) => (
  <div className="layout">
    <Header />
    <div className="content-wrapper">{children}</div>
    <Footer />
  </div>
);

export default Layout;
