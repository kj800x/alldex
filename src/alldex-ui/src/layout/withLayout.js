import React from "react";
import Layout from "./Layout";

import getName from "../util/getName";

export default PageClass => {
  const fn = props => (
    <Layout>
      <PageClass {...props} />
    </Layout>
  );
  fn.rootComponent = PageClass;
  fn.displayName = `withLayout(${getName(PageClass)})`;
  return fn;
};
