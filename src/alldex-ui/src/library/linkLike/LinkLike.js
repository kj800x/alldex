import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { Link } from "react-router-dom";

import "./LinkLike.css";

export const LinkLikeTarget = PropTypes.oneOfType([
  PropTypes.string.isRequired,
  PropTypes.func.isRequired
]);

export default class LinkLike extends Component {
  getClassName() {
    return classNames(
      { "link-like": !this.props.suppressDefaultFormatting },
      this.props.className
    );
  }

  renderSpan() {
    return (
      <span
        onClick={this.props.target}
        className={this.getClassName()}
        style={this.props.style}
      >
        {this.props.children}
      </span>
    );
  }

  renderExternalLink() {
    return (
      <a
        href={this.props.target}
        className={this.getClassName()}
        style={this.props.style}
      >
        {this.props.children}
      </a>
    );
  }

  renderInternalLink() {
    return (
      <Link
        to={this.props.target}
        className={this.getClassName()}
        style={this.props.style}
      >
        {this.props.children}
      </Link>
    );
  }

  render() {
    if (typeof this.props.target === "string") {
      if (this.props.target.indexOf("http") !== -1) {
        return this.renderExternalLink();
      } else {
        return this.renderInternalLink();
      }
    } else {
      return this.renderSpan();
    }
  }
}

LinkLike.defaultProps = {
  suppressDefaultFormatting: false,
  style: {}
};

LinkLike.propTypes = {
  target: LinkLikeTarget.isRequired,
  children: PropTypes.node,
  className: PropTypes.string,
  suppressDefaultFormatting: PropTypes.bool,
  style: PropTypes.object.isRequired
};
