import React, { Component } from "react";
import classNames from "classnames";
import ReactDOM from "react-dom";
import styled, { keyframes } from "styled-components";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import withSettings from "../../settings/withSettings";

const PANEL_WIDTH = 500;
const SLIDE_TIME = 300;
const TRANSITION_NAME = "panel";

const slideIn = keyframes`
  from {
    right: -${PANEL_WIDTH}px;
  }

  to {
    right: 0px;
  }
`;

const slideOut = keyframes`
  from {
    right: 0px;
  }

  to {
    right: -${PANEL_WIDTH}px;
  }
`;

const Panel = styled.div`
  position: fixed;
  right: 0px;
  top: 0px;
  bottom: 0px;
  width: ${PANEL_WIDTH}px;
  box-shadow: 0 0 20px 5px grey;

  .dark-theme & {
    box-shadow: 0 0 20px 5px #7d1515;
  }

  & > div {
    background-color: white;
    height: 100%;
  }

  .dark-theme & > div {
    background-color: black;
  }

  &.${TRANSITION_NAME}-enter {
    animation: ${slideIn} ${SLIDE_TIME}ms ease-out;
  }

  &.${TRANSITION_NAME}-exit {
    animation: ${slideOut} ${SLIDE_TIME}ms ease-in;
    right: -${PANEL_WIDTH}px;
  }
`;

class PanelPortal extends Component {
  render() {
    return ReactDOM.createPortal(
      <div className={classNames({ "dark-theme": this.props.inDarkMode })}>
        {this.props.children}
      </div>,
      document.getElementById("modalRoot")
    );
  }
}

const SettingsPanelPortal = withSettings(PanelPortal);

export default ({ visible, children }) => (
  <SettingsPanelPortal>
    <TransitionGroup>
      {visible && (
        <CSSTransition
          key="panel"
          timeout={SLIDE_TIME}
          classNames={TRANSITION_NAME}
        >
          <Panel>{children}</Panel>
        </CSSTransition>
      )}
    </TransitionGroup>
  </SettingsPanelPortal>
);
