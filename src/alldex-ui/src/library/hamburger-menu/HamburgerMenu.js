import React, { Component } from "react";
import { Manager, Reference, Popper } from "react-popper";

import "./HamburgerMenu.css";

import VimiumClickable from "../vimiumClickable/VimiumClickable";

export default class HamburgerMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
    this.toggleOpen = this.toggleOpen.bind(this);
  }

  toggleOpen() {
    this.setState({
      open: !this.state.open
    });
  }

  renderTether() {
    return <div className="hamburger-tether">{this.props.children}</div>;
  }

  render() {
    return (
      <Manager>
        <Reference>
          {({ ref }) => (
            <div
              className="hamburger-container"
              ref={ref}
              onClick={this.toggleOpen}
            >
              <VimiumClickable>
                <div className="hamburger-bar1"></div>
                <div className="hamburger-bar2"></div>
                <div className="hamburger-bar3"></div>
              </VimiumClickable>
            </div>
          )}
        </Reference>
        {this.state.open && (
          <Popper placement="right-end">
            {({ ref, style, placement, arrowProps }) => (
              <div
                ref={ref}
                style={style}
                className="hamburger-popper"
                data-placement={placement}
              >
                {this.props.children}
                <div ref={arrowProps.ref} style={arrowProps.style} />
              </div>
            )}
          </Popper>
        )}
      </Manager>
    );
  }
}
