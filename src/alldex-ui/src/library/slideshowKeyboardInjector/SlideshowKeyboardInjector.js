import { Component } from "react";
import PropTypes from "prop-types";

export default class SlideshowKeyboardInjector extends Component {
  constructor(props) {
    super(props);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  handleKeyDown(e) {
    switch (e.key) {
      case "ArrowLeft":
        this.props.onLeftArrow && this.props.onLeftArrow();
        return;
      case "ArrowRight":
        this.props.onRightArrow && this.props.onRightArrow();
        return;
      default:
        return;
    }
  }

  componentWillMount() {
    document.addEventListener("keydown", this.handleKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeyDown);
  }

  render() {
    return null;
  }
}

SlideshowKeyboardInjector.propTypes = {
  onLeftArrow: PropTypes.func.isRequired,
  onRightArrow: PropTypes.func.isRequired
};
