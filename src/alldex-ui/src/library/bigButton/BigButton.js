import React, { Component } from "react";
import LinkLike from "../linkLike/LinkLike";

import "./BigButton.css";

export default class BigButton extends Component {
  render() {
    return (
      <LinkLike className="big-button" target={this.props.target}>
        {this.props.children}
      </LinkLike>
    );
  }
}
