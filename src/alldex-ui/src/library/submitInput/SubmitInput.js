import React, { Component } from "react";

import "./SubmitInput.css";

export default class SubmitInput extends Component {
  constructor(props) {
    super(props);
    this.onInputChange = this.onInputChange.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.onSubmitClick = this.onSubmitClick.bind(this);
    this.state = {
      value: ""
    };
  }

  onInputChange(event) {
    this.setState({ value: event.target.value });
  }

  onKeyDown(event) {
    if (event.keyCode === 13) {
      this.onSubmitClick();
    }
  }

  onSubmitClick() {
    this.props.onSubmit(this.state.value);
    this.setState({ value: "" });
  }

  render() {
    const InputComponent = this.props.InputComponent || "input";

    return (
      <span className="submit-input">
        <InputComponent
          type="text"
          value={this.state.value}
          onChange={this.onInputChange}
          onKeyDown={this.onKeyDown}
        />
        <button onClick={this.onSubmitClick}>+</button>
      </span>
    );
  }
}
