import React from "react";

import "./VimiumClickable.css";

const VimiumClickable = ({ children }) => (
  <button className="vimium-clickable">{children}</button>
);

export default VimiumClickable;
