import React from "react";

import BigButton from "../bigButton/BigButton";

const ExternalButton = ({ href, target }) => {
  let url;
  try {
    url = new URL(href);
  } catch (e) {
    url = { hostname: href, pathname: "" };
  }
  return (
    <BigButton target={target || href}>
      {url.hostname}
      <small>{url.pathname}</small>
    </BigButton>
  );
};

export default ExternalButton;
