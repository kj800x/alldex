import React, { PureComponent } from "react";
import classNames from "classnames";

import "./PaginationWidget.css";
import LinkLike from "../linkLike/LinkLike";

/** A pagination widget supporting three modes:
 1) Standard pagination through a finite set (pages are purely computable and there is a known end)
 [-] Page 1/10 [+]
 2) Infinite pagination through an (in)finite set (pages are purely computable and there may be an end but it's so large as to not care)
 [-] Page 1 [+]
 3) Random pagination (pages are not purely computable, the plus button is more akin to a 'fetch more random results')
 [+]

 A target is one of:
 - A String (representing a path to redirect to when the button is pressed)
 - A Function (which is called when the button is pressed)

 Props:
 - mode:             oneOf PaginationWidget.MODE
 - currentPage:      number (required if mode is STANDARD or INFINITE)
 - pageCount:        number (required if mode is STANDARD)
 - backButtonTarget: target (required if mode is STANDARD or INFINITE)
 - nextButtonTarget: target (required if mode is STANDARD or INFINITE or RANDOM)
 */

const MODE = {
  STANDARD: "STANDARD",
  INFINITE: "INFINITE",
  RANDOM: "RANDOM"
};

export default class PaginationWidget extends PureComponent {
  renderBackButton() {
    if (this.props.mode !== MODE.RANDOM) {
      const hidden = this.props.currentPage <= 1;
      return (
        <LinkLike
          target={this.props.backButtonTarget}
          className={classNames("button-link", { hidden })}
        >
          -
        </LinkLike>
      );
    }
    return null;
  }

  renderNextButton() {
    const hidden =
      this.props.mode === MODE.STANDARD &&
      this.props.currentPage >= this.props.pageCount;
    return (
      <LinkLike
        target={this.props.nextButtonTarget}
        className={classNames("button-link", { hidden })}
      >
        +
      </LinkLike>
    );
  }

  renderPosition() {
    switch (this.props.mode) {
      case MODE.STANDARD:
        return (
          <span>
            Page {this.props.currentPage}/{this.props.pageCount}
          </span>
        );
      case MODE.INFINITE:
        return <span>Page {this.props.currentPage}</span>;
      case MODE.RANDOM:
        return null;
      default:
        throw new Error("PaginationWidget was not given a valid mode");
    }
  }

  render() {
    return (
      <div className="paginationWidget">
        {this.renderBackButton()}
        {this.renderPosition()}
        {this.renderNextButton()}
      </div>
    );
  }
}

PaginationWidget.MODE = MODE;

// PaginationWidget.propTypes = {
//   mode: PropTypes.oneOf(Object.values(MODE)),
//   // (required if mode is STANDARD or INFINITE)
//   currentPage: PropTypes.number,
//   // (required if mode is STANDARD)
//   pageCount: PropTypes.number,
//   // (required if mode is STANDARD or INFINITE)
//   backButtonTarget: LinkLikeTarget,
//   // (required in all cases),
//   nextButtonTarget: LinkLikeTarget.isRequired,
// };
