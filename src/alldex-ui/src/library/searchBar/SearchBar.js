import React, { Component } from "react";

import "./SearchBar.css";
import SearchPage from "../../items/pages/SearchPage";
import RoutingConfig from "../../routing/RoutingConfig";
import { withRouter } from "react-router-dom";
import Autocomplete from "../autocomplete/Autocomplete";

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.doSearch = this.doSearch.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.onSearchSelect = this.onSearchSelect.bind(this);
    this.onSearchKeypress = this.onSearchKeypress.bind(this);
    this.state = {
      value: ""
    };
  }

  doSearch() {
    this.props.history.push(
      RoutingConfig.urlFor(SearchPage, {
        query: this.state.value,
        page: 1
      })
    );
  }

  onSearchChange({ target: { value } }) {
    this.setState({ value });
  }

  onSearchSelect({ target: { value } }) {
    this.setState({ value }, this.doSearch);
  }

  onSearchKeypress({ key }) {
    if (key === "Enter") {
      this.doSearch();
    }
  }

  render() {
    return (
      <div className="search-bar">
        <Autocomplete
          useQuotes={true}
          value={this.state.value}
          onChange={this.onSearchChange}
          // onSelect={this.onSearchSelect}
          onKeyPress={this.onSearchKeypress}
        />
        <button onClick={this.doSearch}>Search</button>
      </div>
    );
  }
}

export default withRouter(SearchBar);
