import React from "react";

import "./ScrollingText.css";

export default ({ children }) => (
  <div className="scrolling-text-container">
    <div className="scrolling-text">{children}</div>
  </div>
);
