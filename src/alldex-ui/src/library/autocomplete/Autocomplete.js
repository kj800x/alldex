import React from "react";
import ReactAutocomplete from "react-autocomplete";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import { ALBUM_PREFIX } from "../../albums/constants";

import "./Autocomplete.css";

import pipe from "transmute/pipe";
import map from "transmute/map";

const concat = (a, b) => a.concat(b);

const getTagValue = ({ value }) => `${value}`;
const getAlbumTitleWithPrefix = ({ title }) => `${ALBUM_PREFIX}${title}`;

// Basic autocomplete for tags and albums
const FETCH_SEARCHABLES = gql`
  {
    getTags {
      value
      id
      __typename
    }
    getAlbums {
      title
      id
      __typename
    }
  }
`;

function getValues(data, only, useQuotes) {
  if (only && only === "tags") {
    return Object.values(map(getTagValue, data.getTags));
  }

  const withQuotesIfNeeded = s => (s.indexOf(" ") !== -1 ? `"${s}"` : s);
  const id = s => s;

  const wrapper = useQuotes ? withQuotesIfNeeded : id;

  return concat(
    Object.values(
      map(
        pipe(
          getTagValue,
          wrapper
        ),
        data.getTags
      )
    ),
    Object.values(
      map(
        pipe(
          getAlbumTitleWithPrefix,
          wrapper
        ),
        data.getAlbums
      )
    )
  );
}

function shouldItemRender(item, value) {
  return (
    value.length > 1 && item.toLowerCase().indexOf(value.toLowerCase()) !== -1
  );
}

function sortItems(a, b, value) {
  const aLower = a.toLowerCase();
  const bLower = b.toLowerCase();
  const valueLower = value.toLowerCase();
  const queryPosA = aLower.indexOf(valueLower);
  const queryPosB = bLower.indexOf(valueLower);
  if (queryPosA !== queryPosB) {
    return queryPosB - queryPosA;
  }
  return bLower < aLower ? 1 : -1;
}

function Autocomplete({ onKeypress, onChange, onSelect, value, only }) {
  const { loading, error, data } = useQuery(FETCH_SEARCHABLES);

  if (loading) return <span>Search Loading...</span>;
  if (error) return <span>Search Error :(</span>;

  const values = getValues(data, only);

  return (
    <ReactAutocomplete
      value={value}
      items={values}
      shouldItemRender={shouldItemRender}
      getItemValue={item => item}
      sortItems={sortItems}
      onChange={(_event, value) => onChange({ target: { value } })}
      onSelect={
        onSelect || ((_event, value) => onChange({ target: { value } }))
      }
      inputProps={{ onKeyPress: onKeypress }}
      wrapperStyle={{ position: "relative", display: "inline-block" }}
      renderMenu={children => <div className="menu">{children}</div>}
      renderItem={(item, isHighlighted) => (
        <div
          className={`item ${isHighlighted ? "item-highlighted" : ""}`}
          key={item}
        >
          {item}
        </div>
      )}
    />
  );
}

// Autocomplete.propTypes = {
//   onKeypress: PropTypes.func,
//   onChange: PropTypes.func,
//   onSelect: PropTypes.func.isRequired,
//   value: PropTypes.string.isRequired,
//   only: PropTypes.oneOf(["tags"]),
// };

export default Autocomplete;
