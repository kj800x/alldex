import API from "../util/api";

const api = new API("/api/duplicates");

export const getDuplicates = api.get(
  ({ limit, offset }) => `?limit=${limit}&offset=${offset}`
);
export const getDuplicatesForItem = api.get(
  ({ id, limit, offset }) => `/${id}?limit=${limit}&offset=${offset}`
);
// export const patchItem = api.patch(({ id }) => `/${id}`, ({ id, title, description, sourceUrl }) => ({ id, title, description, sourceUrl }));
