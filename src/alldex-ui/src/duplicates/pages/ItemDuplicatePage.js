import React from "react";

import ItemDuplicateContainer from "../containers/ItemDuplicateContainer";
import DuplicateItemsSearch from "../components/DuplicateItemsSearch";

import RoutingConfig from "routing/RoutingConfig";
import PaginationWidget from "library/paginationWidget/PaginationWidget";

import { RESULTS_PER_PAGE } from "../../constants";

const ItemDuplicatePage = ({
  match: {
    params: { itemId, page }
  }
}) => {
  return (
    <ItemDuplicateContainer
      itemId={itemId}
      limit={RESULTS_PER_PAGE}
      offset={(parseInt(page, 10) - 1) * RESULTS_PER_PAGE}
      renderResults={({ results, totalResultsCount }) => (
        <DuplicateItemsSearch
          pairs={results.map(res => ({
            ids: [res.id, itemId],
            similarity: res.similarity
          }))}
          header={() => (
            <h2 className="thumb-page-header">Item Duplicates for {itemId}</h2>
          )}
          controls={() => (
            <PaginationWidget
              mode={PaginationWidget.MODE.INFINITE}
              backButtonTarget={RoutingConfig.urlFor(ItemDuplicatePage, {
                itemId,
                page: parseInt(page) - 1
              })}
              nextButtonTarget={RoutingConfig.urlFor(ItemDuplicatePage, {
                itemId,
                page: parseInt(page) + 1
              })}
              currentPage={page}
              pageCount={Math.ceil(totalResultsCount / RESULTS_PER_PAGE)}
            />
          )}
        />
      )}
    />
  );
};

export default ItemDuplicatePage;
