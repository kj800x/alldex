import React from "react";

import DuplicateItemsContainer from "../containers/DuplicateItemsContainer";
import DuplicateItemsSearch from "../components/DuplicateItemsSearch";

import RoutingConfig from "routing/RoutingConfig";
import PaginationWidget from "library/paginationWidget/PaginationWidget";

import { RESULTS_PER_PAGE } from "../../constants";

const DuplicateItemsPage = ({
  match: {
    params: { page }
  }
}) => {
  return (
    <DuplicateItemsContainer
      limit={RESULTS_PER_PAGE}
      offset={(parseInt(page, 10) - 1) * RESULTS_PER_PAGE}
      renderResults={({ results, totalResultsCount }) => (
        <DuplicateItemsSearch
          pairs={results}
          header={() => <h2 className="thumb-page-header">Duplicate Items</h2>}
          controls={() => (
            <PaginationWidget
              mode={PaginationWidget.MODE.INFINITE}
              backButtonTarget={RoutingConfig.urlFor(DuplicateItemsPage, {
                page: parseInt(page) - 1
              })}
              nextButtonTarget={RoutingConfig.urlFor(DuplicateItemsPage, {
                page: parseInt(page) + 1
              })}
              currentPage={page}
              pageCount={Math.ceil(totalResultsCount / RESULTS_PER_PAGE)}
            />
          )}
        />
      )}
    />
  );
};

export default DuplicateItemsPage;
