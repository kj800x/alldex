import { FIND_DUPLICATES } from "./actions";
import { handleActions } from "redux-actions";
import { STARTED, SUCCEEDED, FAILED, UNINITIALIZED } from "../util/fetchStatus";

const defaultState = {
  fetchStatus: UNINITIALIZED,
  limit: null,
  offset: null,
  results: null
};

export default handleActions(
  {
    [FIND_DUPLICATES.STARTED]: (
      state,
      {
        payload: {
          args: { limit, offset }
        }
      }
    ) => {
      return {
        fetchStatus: STARTED,
        limit: limit,
        offset: offset,
        results: null
      };
    },
    [FIND_DUPLICATES.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { limit, offset },
          response
        }
      }
    ) => {
      return {
        fetchStatus: SUCCEEDED,
        limit: limit,
        offset: offset,
        results: response
      };
    },
    [FIND_DUPLICATES.FAILED]: (
      state,
      {
        payload: {
          args: { limit, offset }
        }
      }
    ) => {
      return {
        fetchStatus: FAILED,
        limit: limit,
        offset: offset,
        results: null
      };
    }
  },
  defaultState
);
