import React, { Component } from "react";
import {
  SUCCEEDED,
  FAILED,
  UNINITIALIZED,
  STARTED
} from "../../util/fetchStatus";
import { connect } from "react-redux";
import LoadingOverlay from "../../library/loadingOverlay/LoadingOverlay";

import { findItemDuplicates } from "../actions";

class ItemDuplicateContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static getDerivedStateFromProps(nextProps) {
    if (
      JSON.stringify(nextProps.currentOffset) !==
        JSON.stringify(nextProps.offset) ||
      JSON.stringify(nextProps.currentItemId) !==
        JSON.stringify(nextProps.itemId)
    ) {
      nextProps.requestDuplicates();
    }
    return null;
  }

  render() {
    if (
      JSON.stringify(this.props.currentOffset) !==
        JSON.stringify(this.props.offset) ||
      JSON.stringify(this.props.currentItemId) !==
        JSON.stringify(this.props.itemId)
    ) {
      return null;
    }

    switch (this.props.queryFetchStatus) {
      case SUCCEEDED:
        return this.props.renderResults({
          results: this.props.results,
          totalResultsCount: this.props.totalResultsCount
        });
      case FAILED:
        return (
          <div>
            {" "}
            Something failed... Check the redux store for failed requests{" "}
          </div>
        );
      case UNINITIALIZED:
      case STARTED:
      default:
        return <LoadingOverlay />;
    }
  }
}

function mapStateToProps(state) {
  return {
    currentItemId: state.itemDuplicates.itemId,
    currentOffset: state.itemDuplicates.offset,
    queryFetchStatus: state.itemDuplicates.fetchStatus,
    results: state.itemDuplicates.results,
    totalResultsCount: 10000 //TODO: For now /*state.duplicates.results && state.duplicates.results.count,*/
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    requestDuplicates: () =>
      dispatch(
        findItemDuplicates({
          id: ownProps.itemId,
          limit: ownProps.limit,
          offset: ownProps.offset
        })
      )
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemDuplicateContainer);
