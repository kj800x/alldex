import React, { Component } from "react";
import {
  SUCCEEDED,
  FAILED,
  UNINITIALIZED,
  STARTED
} from "../../util/fetchStatus";
import { connect } from "react-redux";
import LoadingOverlay from "../../library/loadingOverlay/LoadingOverlay";

import { findDuplicates } from "../actions";

class DuplicateItemsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static getDerivedStateFromProps(nextProps) {
    if (
      JSON.stringify(nextProps.currentOffset) !==
      JSON.stringify(nextProps.offset)
    ) {
      nextProps.requestDuplicates();
    }
    return null;
  }

  render() {
    if (
      JSON.stringify(this.props.currentOffset) !==
      JSON.stringify(this.props.offset)
    ) {
      return null;
    }

    switch (this.props.queryFetchStatus) {
      case SUCCEEDED:
        return this.props.renderResults({
          results: this.props.results,
          totalResultsCount: this.props.totalResultsCount
        });
      case FAILED:
        return (
          <div>
            {" "}
            Something failed... Check the redux store for failed requests{" "}
          </div>
        );
      case UNINITIALIZED:
      case STARTED:
      default:
        return <LoadingOverlay />;
    }
  }
}

function mapStateToProps(state) {
  return {
    currentOffset: state.duplicates.offset,
    queryFetchStatus: state.duplicates.fetchStatus,
    results: state.duplicates.results,
    totalResultsCount: 10000 //TODO: For now /*state.duplicates.results && state.duplicates.results.count,*/
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    requestDuplicates: () =>
      dispatch(
        findDuplicates({ limit: ownProps.limit, offset: ownProps.offset })
      )
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DuplicateItemsContainer);
