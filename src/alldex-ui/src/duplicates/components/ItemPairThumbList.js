import React, { Component } from "react";

import "./ItemPairThumbList.css";

import ItemThumbContainer from "../../items/containers/ItemThumbContainer";

export default class ItemPairThumbList extends Component {
  render() {
    return (
      <div className="item-thumb-list">
        {this.props.pairs.map(pair => (
          <div className="item-pair" key={`${pair.ids[0]}_${pair.ids[1]}`}>
            <p>Similarity: {pair.similarity.toFixed(3) * 100}%</p>
            <ItemThumbContainer id={pair.ids[0]} key={0} />
            <ItemThumbContainer id={pair.ids[1]} key={1} />
          </div>
        ))}
      </div>
    );
  }
}
