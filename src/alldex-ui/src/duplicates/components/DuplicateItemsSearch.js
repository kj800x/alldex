import React from "react";

import ItemPairThumbList from "./ItemPairThumbList";

const DuplicateItemSearch = ({
  pairs,
  controls = () => null,
  header = () => null
}) => {
  if (!pairs.length) {
    return (
      <div>
        <h2 className="thumb-page-header">No Results</h2>
      </div>
    );
  }

  return (
    <div>
      {header()}
      {controls()}
      <ItemPairThumbList pairs={pairs} />
      {controls()}
    </div>
  );
};

export default DuplicateItemSearch;
