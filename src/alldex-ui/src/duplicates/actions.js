import * as api from "./api";
import apiAction, { buildApiActions } from "../util/apiAction";

export const FIND_DUPLICATES = buildApiActions("FIND_DUPLICATES");
export const findDuplicates = apiAction(FIND_DUPLICATES, api.getDuplicates);

export const FIND_ITEM_DUPLICATES = buildApiActions("FIND_ITEM_DUPLICATES");
export const findItemDuplicates = apiAction(
  FIND_ITEM_DUPLICATES,
  api.getDuplicatesForItem
);
