import { FIND_ITEM_DUPLICATES } from "./actions";
import { handleActions } from "redux-actions";
import { STARTED, SUCCEEDED, FAILED, UNINITIALIZED } from "../util/fetchStatus";

const defaultState = {
  fetchStatus: UNINITIALIZED,
  itemId: null,
  limit: null,
  offset: null,
  results: null
};

export default handleActions(
  {
    [FIND_ITEM_DUPLICATES.STARTED]: (
      state,
      {
        payload: {
          args: { id, limit, offset }
        }
      }
    ) => {
      return {
        fetchStatus: STARTED,
        itemId: id,
        limit: limit,
        offset: offset,
        results: null
      };
    },
    [FIND_ITEM_DUPLICATES.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id, limit, offset },
          response
        }
      }
    ) => {
      return {
        fetchStatus: SUCCEEDED,
        itemId: id,
        limit: limit,
        offset: offset,
        results: response
      };
    },
    [FIND_ITEM_DUPLICATES.FAILED]: (
      state,
      {
        payload: {
          args: { id, limit, offset }
        }
      }
    ) => {
      return {
        fetchStatus: FAILED,
        itemId: id,
        limit: limit,
        offset: offset,
        results: null
      };
    }
  },
  defaultState
);
