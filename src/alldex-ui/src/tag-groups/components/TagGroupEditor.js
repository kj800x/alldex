import React, { Component } from "react";
import "./TagGroupEditor.css";

export default class TagDetails extends Component {
  constructor(props) {
    super(props);
    this.onValueChange = this.onValueChange.bind(this);
    this.onColorChange = this.onColorChange.bind(this);
    this.onIsHiddenChange = this.onIsHiddenChange.bind(this);
    this.submitChanges = this.submitChanges.bind(this);

    this.state = {
      value: props.tagGroupData.value,
      isHidden: props.tagGroupData.isHidden,
      color: props.tagGroupData.color
    };
  }

  onValueChange(e) {
    this.setState({
      value: e.target.value
    });
  }

  onColorChange(e) {
    this.setState({
      color: e.target.value
    });
  }

  onIsHiddenChange(e) {
    this.setState({
      isHidden: e.target.checked
    });
  }

  renderValue() {
    return [
      <dt key="name">Value</dt>,
      <dd key="value">
        <input
          value={this.state.value}
          onChange={this.onValueChange}
          className={
            this.state.value === this.props.tagGroupData.value
              ? undefined
              : "changed"
          }
        />
      </dd>
    ];
  }

  renderColor() {
    return [
      <dt key="name">Color</dt>,
      <dd key="value">
        <input
          value={this.state.color}
          onChange={this.onColorChange}
          className={
            this.state.color === this.props.tagGroupData.color
              ? undefined
              : "changed"
          }
        />
      </dd>
    ];
  }

  renderIsHidden() {
    return [
      <dt key="name">Hidden</dt>,
      <dd key="value">
        <input
          type="checkbox"
          checked={this.state.isHidden}
          onChange={this.onIsHiddenChange}
          className={
            !!this.state.isHidden === !!this.props.tagGroupData.isHidden
              ? undefined
              : "changed"
          }
        />
      </dd>
    ];
  }

  renderId() {
    return [
      <dt key="name">ID</dt>,
      <dd key="value">{this.props.tagGroupData.id}</dd>
    ];
  }

  renderSpecial() {
    return [
      <dt key="name">Special</dt>,
      <dd key="value">{this.props.tagGroupData.special}</dd>
    ];
  }

  submitChanges() {
    this.props.updateTagGroup({
      value: this.state.value,
      color: this.state.color,
      isHidden: this.state.isHidden
    });
  }

  render() {
    return (
      <div className="tag-details tag-group-details">
        <h1>Tag Group Details: {this.props.tagGroupData.value}</h1>
        <dl className="tag-details-editor tag-group-details-editor grid-editor">
          {this.renderValue()}
          {this.renderColor()}
          {this.renderIsHidden()}
          {this.renderId()}
          {this.renderSpecial()}
          <button onClick={this.submitChanges}>Update</button>
        </dl>
      </div>
    );
  }
}
