import React from "react";
import RoutingConfig from "../../routing/RoutingConfig";
import TagGroupPage from "../pages/TagGroupPage";
import LinkLike from "../../library/linkLike/LinkLike";
import SubmitInput from "../../library/submitInput/SubmitInput";

function renderTagGroup(inEditMode, deleteTagGroup) {
  return function(tagGroupData) {
    return (
      <li key={tagGroupData.id}>
        <LinkLike
          style={{ color: "#" + tagGroupData.color }}
          target={RoutingConfig.urlFor(TagGroupPage, {
            tagGroupId: tagGroupData.id
          })}
        >
          {tagGroupData.value}
        </LinkLike>
        {tagGroupData.special && <sup>[{tagGroupData.special}]</sup>}
        &nbsp;
        {inEditMode && !tagGroupData.special && (
          <button
            onClick={() => {
              deleteTagGroup(tagGroupData.id);
            }}
          >
            {" "}
            -{" "}
          </button>
        )}
      </li>
    );
  };
}

export default ({
  tagGroupsData,
  inEditMode,
  deleteTagGroup,
  createTagGroup
}) => {
  return (
    <div className="tag-details">
      <h1> Tag Groups </h1>
      {inEditMode && <SubmitInput onSubmit={createTagGroup} />}
      <ul>
        {Object.values(tagGroupsData).map(
          renderTagGroup(inEditMode, deleteTagGroup)
        )}
      </ul>
    </div>
  );
};
