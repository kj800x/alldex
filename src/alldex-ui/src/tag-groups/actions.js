import * as api from "./api";
import apiAction, { buildApiActions } from "../util/apiAction";
import {
  DEFAULT_NEW_TAGGROUP_COLOR,
  DEFAULT_NEW_TAGGROUP_ISHIDDEN
} from "./constants";

export const FETCH_TAG_GROUP = buildApiActions("FETCH_TAG_GROUP");
export const FETCH_ALL_TAG_GROUPS = buildApiActions("FETCH_ALL_TAG_GROUPS");
export const DELETE_TAG_GROUP = buildApiActions("DELETE_TAG_GROUP");
export const CREATE_TAG_GROUP = buildApiActions("CREATE_TAG_GROUP");
export const UPDATE_TAG_GROUP = buildApiActions("UPDATE_TAG_GROUP");
export const fetchTagGroup = apiAction(FETCH_TAG_GROUP, api.getTagGroup);
export const fetchAllTagGroups = apiAction(
  FETCH_ALL_TAG_GROUPS,
  api.getAllTagGroups
);
export const deleteTagGroup = apiAction(DELETE_TAG_GROUP, api.deleteTagGroup);
export const createTagGroup = apiAction(CREATE_TAG_GROUP, api.createTagGroup);
export const updateTagGroup = apiAction(UPDATE_TAG_GROUP, api.patchTagGroup);

const getDefaultGroupId = state =>
  Object.values(state.tagGroups.data).find(elem => elem.special === "default")
    .id;

export const enhancedDeleteTagGroup = id => (dispatch, getState) => {
  const default_group = getDefaultGroupId(getState());
  dispatch(deleteTagGroup({ id, default_group }));
};

export const enhancedCreateTagGroup = value => (dispatch, getState) => {
  dispatch(
    createTagGroup({
      value,
      color: DEFAULT_NEW_TAGGROUP_COLOR,
      isHidden: DEFAULT_NEW_TAGGROUP_ISHIDDEN
    })
  );
};
