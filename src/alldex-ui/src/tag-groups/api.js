import API from "../util/api";

const api = new API("/api/taggroups");

export const getTagGroup = api.get(({ id }) => `/${id}`);
export const getAllTagGroups = api.get();
export const deleteTagGroup = api.delete(({ id }) => `/${id}`);
export const createTagGroup = api.post("", ({ value, color, isHidden }) => ({
  value,
  color,
  isHidden
}));
export const patchTagGroup = api.patch(
  ({ id }) => `/${id}`,
  ({ value, color, isHidden }) => ({ value, color, isHidden })
);
