import {
  FETCH_TAG_GROUP,
  FETCH_ALL_TAG_GROUPS,
  DELETE_TAG_GROUP,
  CREATE_TAG_GROUP,
  UPDATE_TAG_GROUP
} from "./actions";
import { handleActions } from "redux-actions";
import { STARTED, SUCCEEDED, FAILED, UNINITIALIZED } from "../util/fetchStatus";

import setIn from "transmute/setIn";
import pipe from "transmute/pipe";
import updateIn from "transmute/updateIn";
import filter from "transmute/filter";
import merge from "transmute/merge";

const defaultState = {
  fetchStatus: {},
  fetchAllStatus: UNINITIALIZED,
  data: {}
};

export default handleActions(
  {
    [FETCH_TAG_GROUP.STARTED]: (
      state,
      {
        payload: {
          args: { id }
        }
      }
    ) => {
      return setIn(["fetchStatus", id], STARTED)(state);
    },
    [FETCH_TAG_GROUP.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id },
          response
        }
      }
    ) => {
      return pipe(
        setIn(["fetchStatus", id], SUCCEEDED),
        setIn(["data", id], response)
      )(state);
    },
    [FETCH_TAG_GROUP.FAILED]: (
      state,
      {
        payload: {
          args: { id }
        }
      }
    ) => {
      return setIn(["fetchStatus", id], FAILED)(state);
    },
    [FETCH_ALL_TAG_GROUPS.STARTED]: state => {
      return setIn(["fetchAllStatus"], STARTED)(state);
    },
    [FETCH_ALL_TAG_GROUPS.SUCCEEDED]: (state, { payload: { response } }) => {
      return pipe(
        setIn(["fetchAllStatus"], SUCCEEDED),
        setIn(
          ["data"],
          response.reduce(
            (acc, elem) => Object.assign(acc, { [elem.id]: elem }),
            {}
          )
        )
      )(state);
    },
    [FETCH_ALL_TAG_GROUPS.FAILED]: state => {
      return setIn(["fetchAllStatus"], FAILED)(state);
    },
    [DELETE_TAG_GROUP.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id }
        }
      }
    ) => {
      return pipe(
        updateIn(["fetchStatus"], filter((_value, key) => key !== `${id}`)),
        updateIn(["data"], filter((_value, key) => key !== `${id}`))
      )(state);
    },
    [CREATE_TAG_GROUP.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { value, color, isHidden },
          response: [id]
        }
      }
    ) => {
      return pipe(
        setIn(["fetchStatus", id], SUCCEEDED),
        setIn(["data", id], { value, color, isHidden, id })
      )(state);
    },
    [UPDATE_TAG_GROUP.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id, value, color, isHidden }
        }
      }
    ) => {
      return pipe(
        setIn(["fetchStatus", id], SUCCEEDED),
        updateIn(["data", id], merge({ value, color, isHidden }))
      )(state);
    }
  },
  defaultState
);
