import React from "react";

import TagGroupListContainer from "../containers/TagGroupListContainer";

export default () => {
  return <TagGroupListContainer />;
};
