import React from "react";

import TagGroupViewerContainer from "../containers/TagGroupEditorContainer";

export default ({
  match: {
    params: { tagGroupId }
  }
}) => {
  return <TagGroupViewerContainer id={parseInt(tagGroupId, 10)} />;
};
