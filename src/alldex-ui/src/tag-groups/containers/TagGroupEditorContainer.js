import { connect } from "react-redux";
import TagGroupViewer from "../components/TagGroupEditor";
import withEntity from "../../util/withEntity/withEntity";

import { fetchTagGroup, updateTagGroup } from "../actions";

const mapStateToProps = (state, { id }) => ({});

const mapDispatchToProps = (dispatch, { id }) => ({
  updateTagGroup: ({ value, color, isHidden }) =>
    dispatch(updateTagGroup({ id, value, color, isHidden }))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  withEntity("tagGroups", "tagGroupData", fetchTagGroup, ({ id }) => id)(
    TagGroupViewer
  )
);
