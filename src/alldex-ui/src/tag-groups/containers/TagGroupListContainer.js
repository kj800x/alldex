import React from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import TagGroupList from "../components/TagGroupList";

const FETCH_TAG_GROUPS = gql`
  query tagGroups {
    getTagGroups {
      id
      __typename
      value
      color
      isHidden
      isDeleted
      special
    }
  }
`;

const UPDATE_TAG_GROUP = gql`
  mutation upsertTagGroup($tagGroup: TagGroupInput) {
    upsertTagGroup(tagGroup: $tagGroup) {
      id
      __typename
      value
      color
      isHidden
      isDeleted
      special
    }
  }
`;

export default function TagGroupListContainer() {
  const { loading, error, data } = useQuery(FETCH_TAG_GROUPS);
  const [updateTagGroup] = useMutation(UPDATE_TAG_GROUP);

  if (loading) return <span>Tag Groups Loading...</span>;
  if (error) return <span>Tag Groups Query Error :(</span>;

  return (
    <TagGroupList
      tagGroupsData={data.getTagGroups}
      inEditMode={true}
      deleteTagGroup={id =>
        updateTagGroup({ variables: { tagGroup: { id, isDeleted: true } } })
      }
      createTagGroup={value =>
        updateTagGroup({ variables: { tagGroup: { value } } })
      }
    />
  );
}
