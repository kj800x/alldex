import React, { Component } from "react";
import { connect } from "react-redux";
import { STARTED, SUCCEEDED, UNINITIALIZED, FAILED } from "../fetchStatus";
import getName from "../getName";

import LoadingOverlay from "../../library/loadingOverlay/LoadingOverlay";

export default (entityName, targetProp, fetcher) => Child => {
  class WithAllEntity extends Component {
    constructor(props) {
      super(props);
      this.state = {};
    }

    static getDerivedStateFromProps(nextProps) {
      if (
        nextProps.__WITH_ALL_ENTITY_FETCH_STATUS === UNINITIALIZED ||
        nextProps.__WITH_ALL_ENTITY_FETCH_STATUS === undefined
      ) {
        nextProps.__WITH_ALL_ENTITY_FETCHER();
      }
      return null;
    }

    static displayName = `withAllEntity(${getName(Child)})`;

    render() {
      switch (this.props.__WITH_ALL_ENTITY_FETCH_STATUS) {
        case SUCCEEDED:
          return (
            <Child
              {...{ [targetProp]: this.props.__WITH_ALL_ENTITY_FETCH_DATA }}
              {...this.props}
            />
          );
        case FAILED:
          return (
            <div>
              {" "}
              Something failed... Check the redux store for failed requests{" "}
            </div>
          );
        case UNINITIALIZED:
        case STARTED:
        default:
          return <LoadingOverlay />;
      }
    }
  }

  function mapStateToProps(state) {
    return {
      __WITH_ALL_ENTITY_FETCH_STATUS: state[entityName].fetchAllStatus,
      __WITH_ALL_ENTITY_FETCH_DATA: state[entityName].data
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      __WITH_ALL_ENTITY_FETCHER: () => dispatch(fetcher())
    };
  }

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(WithAllEntity);
};
