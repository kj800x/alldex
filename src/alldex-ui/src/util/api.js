export default class API {
  /**
   * This is a class for building API calls
   */

  constructor(rootPath, extraArgs = { parseJson: true }) {
    this.rootPath = rootPath;
    this.parseJson = extraArgs.parseJson;
  }

  resolvePath(path, invocationArgs) {
    if (path === undefined) {
      return this.rootPath;
    }
    if (typeof path === "function") {
      return `${this.rootPath}${path(...invocationArgs)}`;
    }
    return `${this.rootPath}${path}`;
  }

  resolvePayload(payload, invocationArgs) {
    if (typeof payload === "function") {
      return payload(...invocationArgs);
    }
    return payload;
  }

  get(path) {
    return (...invocationArgs) => {
      const request = fetch(this.resolvePath(path, invocationArgs));
      if (this.parseJson) {
        return request.then(response => response.json());
      }
      return request;
    };
  }

  requestWithPayload(path, payload, method) {
    return (...invocationArgs) => {
      let headers;
      if (this.parseJson) {
        headers = new Headers();
        headers.append("Content-Type", "application/json");
        const url = this.resolvePath(path, invocationArgs);
        const resolvedPayload = this.resolvePayload(payload, invocationArgs);
        const args = {
          method,
          headers: headers,
          body: JSON.stringify(resolvedPayload)
        };
        return fetch(url, args).then(response => response.json());
      } else {
        const url = this.resolvePath(path, invocationArgs);
        const resolvedPayload = this.resolvePayload(payload, invocationArgs);
        const args = { method, body: resolvedPayload };
        return fetch(url, args);
      }
    };
  }

  post(path, payload) {
    return this.requestWithPayload(path, payload, "POST");
  }

  patch(path, payload) {
    return this.requestWithPayload(path, payload, "PATCH");
  }

  put(path, payload) {
    return this.requestWithPayload(path, payload, "PUT");
  }

  delete(path, payload) {
    return this.requestWithPayload(path, payload, "DELETE");
  }
}
