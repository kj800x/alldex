export default C =>
  typeof C.displayName === "string"
    ? C.displayName
    : typeof C.displayName === "function"
    ? C.displayName()
    : typeof C.name === "string"
    ? C.name
    : "UNKNOWN_NAME";
