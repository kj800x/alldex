import { createAction } from "redux-actions";

import { STARTED, SUCCEEDED, FAILED } from "./fetchStatus";

export const buildApiActions = actionName =>
  typeof actionName === "string" || actionName instanceof String
    ? {
        STARTED: `${actionName}_${STARTED}`,
        SUCCEEDED: `${actionName}_${SUCCEEDED}`,
        FAILED: `${actionName}_${FAILED}`
      }
    : actionName;

export default function apiAction(actionName, apiFunc) {
  const { STARTED, SUCCEEDED, FAILED } = buildApiActions(actionName);

  const fetchStarted = createAction(STARTED);
  const fetchSucceeded = createAction(SUCCEEDED);
  const fetchFailed = createAction(FAILED);

  return args => async dispatch => {
    dispatch(fetchStarted({ args }));
    let response;
    try {
      response = await apiFunc(args);
    } catch (error) {
      dispatch(fetchFailed({ error, errorText: error.toString(), args }));
    }
    dispatch(fetchSucceeded({ response, args }));

    return response;
  };
}
