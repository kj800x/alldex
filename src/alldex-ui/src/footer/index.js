import React from "react";

import HamburgerMenu from "../library/hamburger-menu/HamburgerMenu";
import SettingsContainer from "../settings/containers/SettingsContainer";

const Footer = () => (
  <div className="footer">
    <HamburgerMenu>
      <SettingsContainer />
    </HamburgerMenu>
  </div>
);

export default Footer;
