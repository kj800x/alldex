import ItemThumb from "../components/ItemThumb";
import withEntity from "../../util/withEntity/withEntity";
import withSettings from "../../settings/withSettings";

import { fetchItem } from "../actions";

export default withSettings(
  withEntity("items", "itemData", fetchItem, ({ id }) => id)(ItemThumb)
);
