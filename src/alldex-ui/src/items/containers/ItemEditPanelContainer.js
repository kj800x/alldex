import ItemEditPanel from "../components/ItemEditPanel";
import withEntity from "../../util/withEntity/withEntity";
import { connect } from "react-redux";
import pipe from "transmute/pipe";

import { fetchItem, updateItem } from "../actions";

const mapStateToProps = () => ({});
const mapDispatchToProps = (dispatch, ownProps) => ({
  updateItem: changes => {
    dispatch(updateItem(changes));
    ownProps.onClose();
  }
});

export default pipe(
  withEntity("items", "itemData", fetchItem, ({ id }) => id),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(ItemEditPanel);
