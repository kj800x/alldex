import React, { Component } from "react";
import {
  SUCCEEDED,
  FAILED,
  UNINITIALIZED,
  STARTED
} from "../../util/fetchStatus";
import { connect } from "react-redux";
import LoadingOverlay from "../../library/loadingOverlay/LoadingOverlay";

import { searchItems } from "../actions";
import { queryParser } from "../pages/SearchPage";

const buildFilteredQuery = (baseQuery, typeFilters, andFilter) => {
  const types = Object.keys(typeFilters);
  const typeFilterQuery = types.reduce(
    (acc, type) => ({
      OR: typeFilters[type] ? [...acc.OR, { type: type.toUpperCase() }] : acc.OR
    }),
    { OR: [] }
  );

  const AND = [baseQuery.filter];
  if (typeFilterQuery.OR.length > 0) {
    AND.push(typeFilterQuery);
  }
  if (andFilter) {
    AND.push(queryParser(andFilter));
  }

  return {
    ...baseQuery,
    filter: { AND: AND }
  };
};

class SearchContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static getDerivedStateFromProps(nextProps) {
    if (
      JSON.stringify(
        buildFilteredQuery(
          nextProps.query,
          nextProps.typeFilters,
          nextProps.andFilter
        )
      ) !== JSON.stringify(nextProps.activeQuery)
    ) {
      nextProps.requestSearch(
        nextProps.query,
        nextProps.typeFilters,
        nextProps.andFilter
      );
    }
    return null;
  }

  render() {
    if (
      JSON.stringify(
        buildFilteredQuery(
          this.props.query,
          this.props.typeFilters,
          this.props.andFilter
        )
      ) !== JSON.stringify(this.props.activeQuery)
    ) {
      return null;
    }

    switch (this.props.queryFetchStatus) {
      case SUCCEEDED:
        const RenderedComponent = this.props.renderResults;
        return (
          <RenderedComponent
            forceReSearch={() =>
              this.props.requestSearch(
                this.props.query,
                this.props.typeFilters,
                this.props.andFilter
              )
            }
            itemIds={this.props.searchResults.map(item => item.id)}
            totalResultsCount={this.props.totalResultsCount}
          />
        );
      case FAILED:
        return (
          <div>
            Something failed... Check the redux store for failed requests
          </div>
        );
      case UNINITIALIZED:
      case STARTED:
      default:
        return <LoadingOverlay />;
    }
  }
}

function mapStateToProps(state) {
  return {
    activeQuery: state.search.query,
    queryFetchStatus: state.search.fetchStatus,
    searchResults: state.search.results && state.search.results.results,
    totalResultsCount: state.search.results && state.search.results.count,
    typeFilters: state.settings.typeFilters,
    andFilter: state.settings.andFilter
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    requestSearch: (query, typeFilters, andFilter) =>
      dispatch(
        searchItems({
          query: buildFilteredQuery(query, typeFilters, andFilter)
        })
      )
  };
}

export const rawSearchContainer = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default rawSearchContainer(SearchContainer);
