import { connect } from "react-redux";
import { push } from "connected-react-router";
import SearchSlideshowIndicatorWidget from "../components/SearchSlideshowIndicatorWidget";

const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch => ({
  push: url => dispatch(push(url))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchSlideshowIndicatorWidget);
