import React, { Component } from "react";
import { connect } from "react-redux";

import StarsDisplay from "../components/StarsDisplay";

import {
  ensureTagExists,
  dissociateTag,
  associateTag
} from "../../tags/actions";
import { getTagId } from "../../tags/selectors";

class StarsDisplayContainer extends Component {
  componentDidMount() {
    this.props.ensureTagExists("1-star");
    this.props.ensureTagExists("2-star");
    this.props.ensureTagExists("3-star");
    this.props.ensureTagExists("4-star");
    this.props.ensureTagExists("5-star");
  }

  render() {
    return <StarsDisplay {...this.props} />;
  }
}

const mapStateToProps = state => ({
  starTagIds: [
    getTagId("1-star")(state),
    getTagId("2-star")(state),
    getTagId("3-star")(state),
    getTagId("4-star")(state),
    getTagId("5-star")(state)
  ]
});

const mapDispatchToProps = (dispatch, props) => ({
  ensureTagExists: tagValue => dispatch(ensureTagExists(tagValue)),
  onTagAdd: tagId =>
    dispatch(associateTag({ item_id: props.data.id, tag_id: tagId })),
  onTagRemove: tagId =>
    dispatch(dissociateTag({ item_id: props.data.id, tag_id: tagId }))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StarsDisplayContainer);
