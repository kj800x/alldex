import ItemViewer from "../components/ItemViewer";
import withEntity from "../../util/withEntity/withEntity";
import withSettings from "../../settings/withSettings";

import { fetchItem } from "../actions";

export default withSettings(
  withEntity("items", "itemData", fetchItem, ({ id }) => id)(ItemViewer)
);
