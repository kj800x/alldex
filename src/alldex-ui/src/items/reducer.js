import { FETCH_ITEM, SEARCH_ITEMS, UPDATE_ITEM } from "./actions";
import { DISSOCIATE_TAG, ASSOCIATE_TAG, DELETE_TAG } from "../tags/actions";
import {
  ASSOCIATE_ALBUM,
  DISSOCIATE_ALBUM,
  DELETE_ALBUM
} from "../albums/actions";

import { handleActions } from "redux-actions";
import { STARTED, SUCCEEDED, FAILED } from "../util/fetchStatus";

import setIn from "transmute/setIn";
import updateIn from "transmute/updateIn";
import pipe from "transmute/pipe";
import map from "transmute/map";
import reduce from "transmute/reduce";
import mergeIn from "transmute-extensions/mergeIn";

const defaultState = {
  fetchStatus: {},
  data: {}
};

export const removeAssociationsWith = id => oldIds =>
  oldIds.filter(idElem => idElem !== id);
export const removeAlbumAssociationsWith = id => oldIds =>
  oldIds.filter(elem => elem.album_id !== id);
export const reduceGreaterAlbumPositions = (id, position) => albummap =>
  albummap.album_id === id && albummap.position > position
    ? updateIn(["position"], pos => pos - 1, albummap)
    : albummap;

const debufferFingerprint = item =>
  item.fingerprint
    ? { ...item, fingerprint: new Uint8Array(item.fingerprint.data) }
    : item;

export default handleActions(
  {
    [FETCH_ITEM.STARTED]: (
      state,
      {
        payload: {
          args: { id }
        }
      }
    ) => {
      return setIn(["fetchStatus", id], STARTED)(state);
    },
    [FETCH_ITEM.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id },
          response
        }
      }
    ) => {
      return pipe(
        setIn(["fetchStatus", id], SUCCEEDED),
        setIn(["data", id], debufferFingerprint(response))
      )(state);
    },
    [UPDATE_ITEM.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id, title, description, sourceUrl }
        }
      }
    ) => {
      return pipe(
        setIn(["fetchStatus", id], SUCCEEDED),
        mergeIn(["data", id], { title, description, sourceUrl })
      )(state);
    },
    [FETCH_ITEM.FAILED]: (
      state,
      {
        payload: {
          args: { id }
        }
      }
    ) => {
      return setIn(["fetchStatus", id], FAILED)(state);
    },
    [ASSOCIATE_ALBUM.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { item_id, album_id },
          response: position
        }
      }
    ) => {
      return updateIn(["data", item_id, "albums"], oldAlbums => [
        ...oldAlbums,
        { album_id, position }
      ])(state);
    },
    [DISSOCIATE_ALBUM.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { item_id, album_id, position }
        }
      }
    ) => {
      return pipe(
        updateIn(
          ["data", item_id, "albums"],
          removeAlbumAssociationsWith(album_id)
        ),
        updateIn(
          ["data"],
          map(
            updateIn(
              ["albums"],
              map(reduceGreaterAlbumPositions(album_id, position))
            )
          )
        )
      )(state);
    },
    [ASSOCIATE_TAG.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { item_id, tag_id }
        }
      }
    ) => {
      return updateIn(["data", item_id, "tags"], oldTags => [
        ...oldTags,
        tag_id
      ])(state);
    },
    [DISSOCIATE_TAG.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { item_id, tag_id }
        }
      }
    ) => {
      return updateIn(
        ["data", item_id, "tags"],
        removeAssociationsWith(tag_id)
      )(state);
    },
    [DELETE_TAG.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id }
        }
      }
    ) => {
      return updateIn(
        ["data"],
        map(updateIn(["tags"], removeAssociationsWith(id)))
      )(state);
    },
    [DELETE_ALBUM.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { id }
        }
      }
    ) => {
      return updateIn(
        ["data"],
        map(updateIn(["albums"], removeAlbumAssociationsWith(id)))
      )(state);
    },
    [SEARCH_ITEMS.SUCCEEDED]: (
      state,
      {
        payload: {
          response: { results }
        }
      }
    ) => {
      return reduce(
        state,
        (acc, item) =>
          pipe(
            setIn(["fetchStatus", item.id], SUCCEEDED),
            setIn(["data", item.id], debufferFingerprint(item))
          )(acc),
        results
      );
    }
  },
  defaultState
);
