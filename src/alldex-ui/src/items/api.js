import API from "../util/api";

const api = new API("/api/items");

export const getItem = api.get(({ id }) => `/${id}`);
export const searchItems = api.post("/search", ({ query }) => query);
export const patchItem = api.patch(
  ({ id }) => `/${id}`,
  ({ id, title, description, sourceUrl }) => ({
    id,
    title,
    description,
    sourceUrl
  })
);
export const reimportItem = api.post(({ id }) => `/${id}/reimport`);
