import React from "react";

import "./ItemDetailsHeader.css";

const ItemDetailsHeader = ({ data }) => {
  return (
    <div className="item-details-header">
      {data.title && <h1>{data.title}</h1>}
      {data.description && <p>{data.description}</p>}
      {data.sourceUrl && <p>From {data.sourceUrl}</p>}
      {data.file && <p>Saved at {data.file}</p>}
    </div>
  );
};

export default ItemDetailsHeader;
