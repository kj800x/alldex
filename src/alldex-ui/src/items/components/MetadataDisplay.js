import React, { Component } from "react";
import "./MetadataDisplay.css";
import ItemDuplicatePage from "duplicates/pages/ItemDuplicatePage";
import RoutingConfig from "routing/RoutingConfig";
// import SimilarImagesListPage from "view/image/similarImagesListPage/SimilarImagesListPage";
// import ImageModel from "models/ImageModel";
import LinkLike from "library/linkLike/LinkLike";

// function displaySource(sourceUrl) {
//   if (sourceUrl) {
//     if (sourceUrl.startsWith("http")) {
//       return sourceUrl.split("//")[1].split("/")[0];
//     } else {
//       return sourceUrl;
//     }
//   }
//   return "";
// }

export default class ImageInfo extends Component {
  renderType() {
    return [
      <dt key="1">Type</dt>,
      <dd key="2">{this.props.itemData.type.toLowerCase()}</dd>
    ];
  }

  renderSource() {
    if (this.props.itemData.sourceUrl) {
      if (this.props.itemData.sourceUrl.startsWith("http")) {
        const sourceDomain = this.props.itemData.sourceUrl
          .split("//")[1]
          .split("/")[0];
        return [
          <dt key="1">Source</dt>,
          <dd key="2">
            <a href={this.props.itemData.sourceUrl}>{sourceDomain}</a>
          </dd>
        ];
      } else {
        return [
          <dt key="1">Source</dt>,
          <dd key="2">{this.props.itemData.sourceUrl}</dd>
        ];
      }
    }
    return null;
  }

  renderSourceKey() {
    if (this.props.itemData.sourceKey) {
      return [
        <dt key="1">Source Key</dt>,
        <dd key="2">{this.props.itemData.sourceKey}</dd>
      ];
    }
    return null;
  }

  renderTitle() {
    if (this.props.itemData.title) {
      return [
        <dt key="1">Title</dt>,
        <dd key="2" title={this.props.itemData.title}>
          {this.props.itemData.title}
        </dd>
      ];
    }
    return null;
  }

  renderDescription() {
    if (this.props.itemData.description) {
      return [
        <dt key="1">Desc.</dt>,
        <dd
          key="2"
          title={this.props.itemData.description}
          style={{ whiteSpace: "nowrap" }}
        >
          {this.props.itemData.description}
        </dd>
      ];
    }
    return null;
  }

  renderFingerprint() {
    if (this.props.itemData.fingerprint) {
      const fingerprintHex = Array.prototype.slice
        .call(this.props.itemData.fingerprint)
        .map(function(num) {
          return num.toString(16);
        })
        .join("");
      return [
        <dt key="1">Fingerprint</dt>,
        <dd key="2" className="fingerprint">
          {fingerprintHex}
          <br />
          <LinkLike
            target={RoutingConfig.urlFor(ItemDuplicatePage, {
              itemId: this.props.itemData.id,
              page: 1
            })}
          >
            similar
          </LinkLike>
        </dd>
      ];
    }
    return null;
  }

  render() {
    return (
      <dl className="item-metadata">
        {this.renderSource()}
        {this.renderSourceKey()}
        {this.renderType()}
        {this.renderTitle()}
        {this.renderDescription()}
        {this.renderFingerprint()}
      </dl>
    );
  }
}

// ImageInfo.propTypes = {
//   image: ImageModel.isRequired,
// };
