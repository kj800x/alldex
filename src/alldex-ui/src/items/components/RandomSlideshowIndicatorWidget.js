import React, { Component } from "react";

import classNames from "classnames";

import "../../albums/components/AlbumSlideshowIndicatorWidget.css";
import LinkLike from "../../library/linkLike/LinkLike";
import KeyboardInjector from "../../library/slideshowKeyboardInjector/SlideshowKeyboardInjector";

const DirectionalButton = ({ forceRefresh, children }) => {
  return (
    <LinkLike
      className={classNames("directional-button", "minibutton", {
        visible: true
      })}
      target={forceRefresh}
    >
      {children}
    </LinkLike>
  );
};

export default class SearchSlideshowIndicatorWidget extends Component {
  constructor(props) {
    super(props);
    this.onRightArrow = this.onRightArrow.bind(this);
  }

  onRightArrow() {
    this.props.forceRefresh();
  }

  render() {
    return (
      <div className="item-page-banner">
        <KeyboardInjector onRightArrow={this.onRightArrow} />
        Random
        <DirectionalButton forceRefresh={this.props.forceRefresh}>
          &gt;
        </DirectionalButton>
      </div>
    );
  }
}
