import React, { useState } from "react";
import styled from "styled-components";

const TallTextArea = styled.textarea`
  height: 200px;
  font-family: sans-serif;
  resize: none;
`;

const StyledLabel = styled.label`
  padding: 10px 0 10px 10px;
  display: block;
  font-size: small;
`;

const StyledInput = styled.input`
  border-color: transparent !important;
  background-color: transparent !important;
  border-bottom: 1px solid transparent !important;
  width: 100%;
  padding: 0px;
  margin: 0px;
  font-size: large;
  text-overflow: ellipsis;
  overflow: hidden;

  &:hover {
    border-bottom: 1px solid skyblue !important;
  }
`;

const StyledButton = styled.button`
  margin-left: 10px;
  padding: 4px;
  color: green !important;
  border-color: green !important;
  border-style: solid;
  font-family: sans-serif;
  font-size: medium;
  border-radius: 5px;

  &:hover {
    cursor: pointer;
    color: darkgreen !important;
    border-color: darkgreen !important;
  }
`;

const FormField = ({ onChange, value, label, as = "input" }) => {
  return (
    <StyledLabel>
      {value && label}
      <br />
      <StyledInput
        as={as}
        onChange={onChange}
        placeholder={label}
        value={value}
      />
    </StyledLabel>
  );
};

function ItemEditPanel({ itemData, updateItem }) {
  const [title, setTitle] = useState(itemData.title);
  const [description, setDescription] = useState(itemData.description);
  const [sourceUrl, setSourceUrl] = useState(itemData.sourceUrl);

  const withValue = setter => ({ target: { value } }) => setter(value);

  return (
    <div>
      <FormField onChange={withValue(setTitle)} value={title} label={"Title"} />
      <FormField
        onChange={withValue(setDescription)}
        as={TallTextArea}
        value={description}
        label={"Description"}
      />
      <FormField
        onChange={withValue(setSourceUrl)}
        value={sourceUrl}
        label={"Source"}
      />
      <StyledButton
        onClick={() => {
          updateItem({
            id: itemData.id,
            title,
            description,
            sourceUrl
          });
        }}
      >
        Save and Close
      </StyledButton>
    </div>
  );
}

export default ItemEditPanel;
