import React from "react";

import RoutingConfig from "../../routing/RoutingConfig";
import ItemPage from "../pages/ItemPage";
import LinkLike from "../../library/linkLike/LinkLike";

import "./VideoItemThumb.css";

export default ({ item }) => (
  <div className="urlItemTagWrapper video-tag-wrapper">
    <LinkLike target={RoutingConfig.urlFor(ItemPage, { itemId: item.id })}>
      <img
        className="image-item-thumb"
        src={process.env.PUBLIC_URL + "/staticThumbs/" + item.file}
        alt="Video Thumb"
      />
      <div className="play-overlay" />
    </LinkLike>
  </div>
);
