import React from "react";

import "./ImageItemThumb.css";

import ItemPage from "../pages/ItemPage";
import RoutingConfig from "../../routing/RoutingConfig";
import LinkLike from "../../library/linkLike/LinkLike";

export default ({ item }) => {
  return (
    <LinkLike target={RoutingConfig.urlFor(ItemPage, { itemId: item.id })}>
      <img
        className="image-item-thumb"
        src={process.env.PUBLIC_URL + "/staticAssets/" + item.file}
        alt="Main"
      />
    </LinkLike>
  );
};
