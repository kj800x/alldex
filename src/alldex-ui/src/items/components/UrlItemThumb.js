import React from "react";

import ExternalButton from "../../library/externalButton/ExternalButton";

import RoutingConfig from "../../routing/RoutingConfig";
import ItemPage from "../pages/ItemPage";

export default ({ item }) => (
  <div className="urlItemTagWrapper">
    <ExternalButton
      href={item.url}
      target={RoutingConfig.urlFor(ItemPage, { itemId: item.id })}
    />
  </div>
);
