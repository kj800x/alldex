import React from "react";

import ImageItemThumb from "./ImageItemThumb";
import UrlItemThumb from "./UrlItemThumb";
import VideoItemThumb from "./VideoItemThumb";
import TextItemThumb from "./TextItemThumb";

import InlineTagContainer from "../../tags/containers/InlineTagContainer";
import InlineAlbumContainer from "../../albums/containers/InlineAlbumContainer";

import "./ItemThumb.css";

const MAIN_COMPONENT_BY_TYPE = {
  IMAGE: ImageItemThumb,
  // "IMAGESET": ImageSetItemThumb,
  VIDEO: VideoItemThumb,
  URL: UrlItemThumb,
  TEXT: TextItemThumb
};

function InlineAlbumList({ itemData }) {
  return (
    <>
      {itemData.albums.map(({ album_id, position }) => (
        <InlineAlbumContainer
          id={album_id}
          position={position}
          key={album_id}
        />
      ))}
    </>
  );
}

function InlineTagList({ itemData, showHidden }) {
  return (
    <>
      {itemData.tags.map(tagId => (
        <InlineTagContainer id={tagId} key={tagId} showHidden={showHidden} />
      ))}
    </>
  );
}

export default function ItemThumb({ itemData, showHidden }) {
  const MainComponent =
    MAIN_COMPONENT_BY_TYPE[itemData.type] ||
    (() => "Error: Invalid Item Type: " + itemData.type);

  return (
    <div>
      <MainComponent item={itemData} />
      <hr />
      <div className="thumb-album-and-tag-container">
        <InlineAlbumList itemData={itemData} />
        <InlineTagList itemData={itemData} showHidden={showHidden} />
      </div>
    </div>
  );
}
