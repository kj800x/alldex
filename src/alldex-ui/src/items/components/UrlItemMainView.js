import React from "react";

import ExternalButton from "../../library/externalButton/ExternalButton";

export default ({ data }) => (
  <div className="urlItemWrapper">
    <ExternalButton href={data.url} />
  </div>
);
