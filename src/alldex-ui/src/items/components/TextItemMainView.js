import React from "react";

import "./TextItemMainView.css";

export default ({ data }) => (
  <div className="text-item-main-view">
    {data.content && data.content.replace(/\n/g, "\n\n")}
  </div>
);
