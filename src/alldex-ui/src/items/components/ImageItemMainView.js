import React from "react";

export default ({ data }) => (
  <img src={process.env.PUBLIC_URL + "/staticAssets/" + data.file} alt={data.title} />
);
