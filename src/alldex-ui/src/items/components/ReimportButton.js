import React, { Component } from "react";
import { reimportItem } from "../api";
import RoutingConfig from "../../routing/RoutingConfig";
import TagItemsListPage from "../../tags/pages/TagItemsListPage";

import { withRouter } from "react-router";

class ReimportButton extends Component {
  constructor(props) {
    super(props);
    this.reimport = this.reimport.bind(this);
  }

  reimport() {
    reimportItem({ id: this.props.itemData.id }).then(res => {
      console.log(res);
      this.props.history.replace(
        RoutingConfig.urlFor(TagItemsListPage, {
          tagId: res.import_tag,
          page: 1
        })
      );
    });
  }

  render() {
    return (
      <>
        {this.props.editingEnabled && (
          <button onClick={this.reimport}>Reimport</button>
        )}
      </>
    );
  }
}

export default withRouter(ReimportButton);
