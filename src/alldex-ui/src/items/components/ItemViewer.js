import React from "react";

import Sidebar from "./Sidebar";
import ItemDetailsHeader from "./ItemDetailsHeader";

import ImageItemMainView from "./ImageItemMainView";
import UrlItemMainView from "./UrlItemMainView";
import VideoItemMainView from "./VideoItemMainView";
import TextItemMainView from "./TextItemMainView";

import "./ItemViewer.css";

const MAIN_COMPONENT_BY_TYPE = {
  IMAGE: ImageItemMainView,
  IMAGESET: () => <span>ImageSet not implemented</span>,
  VIDEO: VideoItemMainView,
  URL: UrlItemMainView,
  TEXT: TextItemMainView
};

const ItemDetailsFooter = ({ data }) => {
  return null;
};

export default ({
  itemData,
  showHidden,
  inDetailsMode,
  inEditMode,
  onNext,
  bannerHeader = () => null
}) => {
  const MainComponent =
    MAIN_COMPONENT_BY_TYPE[itemData.type] ||
    (() => "Error: Invalid Item Type: " + itemData.type);

  return (
    <div className="item-viewer">
      <div className="item-viewer-sidebar">
        <Sidebar
          banner={bannerHeader()}
          data={itemData}
          showHidden={showHidden}
          editingEnabled={inEditMode}
        />
      </div>
      <div className="item-viewer-main">
        {inDetailsMode && <ItemDetailsHeader data={itemData} />}
        <MainComponent data={itemData} onNext={onNext} />
        {inDetailsMode && <ItemDetailsFooter data={itemData} />}
      </div>
    </div>
  );
};
