import React, { useEffect, useRef } from "react";

export default ({ data, onNext }) => {
  const ref = useRef(null);
  useEffect(() => {
    ref.current.addEventListener("ended", onNext);
  }, [ref]);

  return (
    <video
      src={process.env.PUBLIC_URL + "/staticAssets/" + data.file}
      controls
      autoPlay="autoPlay"
      alt="Main"
      ref={ref}
    />
  );
};
