import React from "react";

import LinkLike from "../../library/linkLike/LinkLike";
import ScrollingText from "../../library/scrollingText/ScrollingText";

import RoutingConfig from "../../routing/RoutingConfig";
import ItemPage from "../pages/ItemPage";

export default ({ item }) => (
  <div className="urlItemTagWrapper">
    <LinkLike
      target={RoutingConfig.urlFor(ItemPage, { itemId: item.id })}
      style={{ width: "100%" }}
    >
      <ScrollingText>
        {item.content && item.content.replace(/\n/g, "\n\n")}
      </ScrollingText>
    </LinkLike>
  </div>
);
