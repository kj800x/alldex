import React, { Component } from "react";

import classNames from "classnames";

import "../../albums/components/AlbumSlideshowIndicatorWidget.css";
import LinkLike from "../../library/linkLike/LinkLike";
import RoutingConfig from "../../routing/RoutingConfig";
import KeyboardInjector from "../../library/slideshowKeyboardInjector/SlideshowKeyboardInjector";
import SearchSlideshowPage from "../pages/SearchSlideshowPage";

const DirectionalButton = ({
  totalResultsCount,
  targetPosition,
  currentPosition,
  query,
  children
}) => {
  const visible =
    targetPosition !== currentPosition &&
    targetPosition >= 1 &&
    targetPosition <= totalResultsCount;

  return (
    <LinkLike
      className={classNames("directional-button", "minibutton", { visible })}
      target={RoutingConfig.urlFor(SearchSlideshowPage, {
        query,
        position: targetPosition
      })}
    >
      {children}
    </LinkLike>
  );
};

export default class SearchSlideshowIndicatorWidget extends Component {
  constructor(props) {
    super(props);
    this.onLeftArrow = this.onLeftArrow.bind(this);
    this.onRightArrow = this.onRightArrow.bind(this);
  }

  onLeftArrow() {
    if (this.props.position > 1) {
      this.props.push(
        RoutingConfig.urlFor(SearchSlideshowPage, {
          query: this.props.query,
          position: this.props.position - 1
        })
      );
    }
  }

  onRightArrow() {
    if (this.props.position < this.props.totalResultsCount) {
      this.props.push(
        RoutingConfig.urlFor(SearchSlideshowPage, {
          query: this.props.query,
          position: this.props.position + 1
        })
      );
    }
  }

  render() {
    return (
      <div className="item-page-banner">
        <KeyboardInjector
          onLeftArrow={this.onLeftArrow}
          onRightArrow={this.onRightArrow}
        />
        <DirectionalButton
          query={this.props.query}
          totalResultsCount={this.props.totalResultsCount}
          currentPosition={this.props.position}
          targetPosition={1}
        >
          &lt;&lt;
        </DirectionalButton>
        <DirectionalButton
          query={this.props.query}
          totalResultsCount={this.props.totalResultsCount}
          currentPosition={this.props.position}
          targetPosition={this.props.position - 1}
        >
          &lt;
        </DirectionalButton>
        {this.props.query} ({this.props.position}/{this.props.totalResultsCount}
        )
        <DirectionalButton
          query={this.props.query}
          totalResultsCount={this.props.totalResultsCount}
          currentPosition={this.props.position}
          targetPosition={this.props.position + 1}
        >
          &gt;
        </DirectionalButton>
        <DirectionalButton
          query={this.props.query}
          totalResultsCount={this.props.totalResultsCount}
          currentPosition={this.props.position}
          targetPosition={this.props.totalResultsCount}
        >
          &gt;&gt;
        </DirectionalButton>
      </div>
    );
  }
}
