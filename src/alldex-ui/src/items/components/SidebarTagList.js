import React from "react";

import SidebarTagElementContainer from "../../tags/containers/SidebarTagElementContainer";
import SidebarAlbumElementContainer from "../../albums/containers/SidebarAlbumElementContainer";
import SidebarTagAdderContainer from "../../tags/containers/SidebarTagAdderContainer";
import Autocomplete from "../../library/autocomplete/Autocomplete.js";

export default ({ itemData, showHidden, editingEnabled }) => {
  return (
    <div>
      {editingEnabled && (
        <SidebarTagAdderContainer
          itemId={itemData.id}
          InputComponent={Autocomplete}
        />
      )}
      {itemData.albums.map(({ position, album_id }) => (
        <SidebarAlbumElementContainer
          itemId={itemData.id}
          id={album_id}
          key={album_id}
          position={position}
          editingEnabled={editingEnabled}
        />
      ))}
      {itemData.tags.map(e => (
        <SidebarTagElementContainer
          itemId={itemData.id}
          id={e}
          key={e}
          showHidden={showHidden}
          editingEnabled={editingEnabled}
        />
      ))}
    </div>
  );
};
