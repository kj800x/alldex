import React from "react";

import ItemThumbList from "./ItemThumbList";

const ItemSearch = ({
  itemIds,
  controls = () => null,
  header = () => null
}) => {
  return (
    <div>
      {header()}
      {controls()}
      <ItemThumbList itemIds={itemIds} />
      {controls()}
    </div>
  );
};

export default ItemSearch;
