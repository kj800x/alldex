import React from "react";

import MetadataDisplay from "./MetadataDisplay";
import SidebarTagList from "./SidebarTagList";
import ItemEditPanelButton from "./ItemEditPanelButton";
import ReimportButton from "./ReimportButton";

import StarsDisplayContainer from "../containers/StarsDisplayContainer";

export default ({ data, banner, showHidden, editingEnabled }) => {
  return (
    <div>
      {banner || null}
      <StarsDisplayContainer data={data} />
      <SidebarTagList
        itemData={data}
        showHidden={showHidden}
        editingEnabled={editingEnabled}
      />
      <hr />
      <MetadataDisplay
        itemData={data}
        showHidden={showHidden}
        editingEnabled={editingEnabled}
      />
      <ItemEditPanelButton editingEnabled={editingEnabled} itemData={data} />
      <ReimportButton editingEnabled={editingEnabled} itemData={data} />
    </div>
  );
};
