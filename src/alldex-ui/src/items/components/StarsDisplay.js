import React, { Component } from "react";

import "./StarsDisplay.css";
import VimiumClickable from "../../library/vimiumClickable/VimiumClickable";

const FULL_STAR = "\u2605";
const EMPTY_STAR = "\u2606";

class Star extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.onClick(this.props.i);
  }

  render() {
    const isFilled = this.props.i <= this.props.stars;
    return (
      <span onClick={this.handleClick}>
        <VimiumClickable>{isFilled ? FULL_STAR : EMPTY_STAR}</VimiumClickable>
      </span>
    );
  }
}

export default class StarsDisplay extends Component {
  constructor(props) {
    super(props);
    this.setToStar = this.setToStar.bind(this);
  }

  setToStar(number) {
    const currentStar = this.countStars();
    if (currentStar === 0) {
      this.props.onTagAdd(this.props.starTagIds[number - 1]);
    } else if (currentStar === number) {
      const currentStarId = this.props.starTagIds[currentStar - 1];
      this.props.onTagRemove(currentStarId);
    } else {
      const currentStarId = this.props.starTagIds[currentStar - 1];
      this.props.onTagRemove(currentStarId).then(() => {
        this.props.onTagAdd(this.props.starTagIds[number - 1]);
      });
    }
  }

  countStars() {
    const tagIds = this.props.data.tags;
    const starId = this.props.starTagIds.find(id => tagIds.indexOf(id) !== -1);
    const star = this.props.starTagIds.indexOf(starId) + 1;
    return star || 0;
  }

  render() {
    const stars = this.countStars();
    return (
      <div className="stars">
        <Star i={1} stars={stars} onClick={this.setToStar} />
        <Star i={2} stars={stars} onClick={this.setToStar} />
        <Star i={3} stars={stars} onClick={this.setToStar} />
        <Star i={4} stars={stars} onClick={this.setToStar} />
        <Star i={5} stars={stars} onClick={this.setToStar} />
      </div>
    );
  }
}
