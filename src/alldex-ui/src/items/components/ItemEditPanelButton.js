import React, { Component } from "react";
import UIPanel from "../../library/UIPanel/UIPanel";
import ItemEditPanelContainer from "../containers/ItemEditPanelContainer";

export default class ItemEditPanelButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      panelVisible: false
    };
    this.toggleVisibility = this.toggleVisibility.bind(this);
  }

  toggleVisibility() {
    this.setState({
      panelVisible: !this.state.panelVisible
    });
  }

  render() {
    return (
      <>
        {this.props.editingEnabled && (
          <button onClick={this.toggleVisibility}>Edit</button>
        )}
        <UIPanel visible={this.state.panelVisible}>
          <ItemEditPanelContainer
            onClose={this.toggleVisibility}
            id={parseInt(this.props.itemData.id, 10)}
          />
        </UIPanel>
      </>
    );
  }
}
