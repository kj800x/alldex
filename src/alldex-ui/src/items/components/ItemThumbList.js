import React, { Component } from "react";

import "./ItemThumbList.css";

import ItemThumbContainer from "../containers/ItemThumbContainer";

export default class ItemThumbList extends Component {
  render() {
    return (
      <div className="item-thumb-list">
        {this.props.itemIds.map(itemId => (
          <ItemThumbContainer id={itemId} key={itemId} />
        ))}
      </div>
    );
  }
}
