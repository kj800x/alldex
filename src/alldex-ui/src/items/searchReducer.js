import { SEARCH_ITEMS } from "./actions";
import { handleActions } from "redux-actions";
import { STARTED, SUCCEEDED, FAILED, UNINITIALIZED } from "../util/fetchStatus";

const defaultState = {
  fetchStatus: UNINITIALIZED,
  query: null,
  results: null
};

export default handleActions(
  {
    [SEARCH_ITEMS.STARTED]: (
      state,
      {
        payload: {
          args: { query }
        }
      }
    ) => {
      return {
        fetchStatus: STARTED,
        query: query,
        results: null
      };
    },
    [SEARCH_ITEMS.SUCCEEDED]: (
      state,
      {
        payload: {
          args: { query },
          response
        }
      }
    ) => {
      return {
        fetchStatus: SUCCEEDED,
        query: query,
        results: response
      };
    },
    [SEARCH_ITEMS.FAILED]: (
      state,
      {
        payload: {
          args: { query }
        }
      }
    ) => {
      return {
        fetchStatus: FAILED,
        query: query,
        results: null
      };
    }
  },
  defaultState
);
