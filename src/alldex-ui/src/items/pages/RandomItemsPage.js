import React from "react";

import SearchContainer from "../containers/SearchContainer";
import PaginationWidget from "../../library/paginationWidget/PaginationWidget";

import { RESULTS_PER_PAGE } from "../../constants";
import ItemSearch from "../components/ItemSearch";
import RoutingConfig from "routing/RoutingConfig";
import RandomSlideshowPage from "./RandomSlideshowPage";
import LinkLike from "../../library/linkLike/LinkLike";

const RandomItemsPage = () => {
  return (
    <SearchContainer
      query={{
        filter: {},
        ordering: "random",
        offset: 0,
        limit: RESULTS_PER_PAGE
      }}
      renderResults={({ forceReSearch, itemIds }) => (
        <ItemSearch
          itemIds={itemIds}
          header={() => (
            <div>
              <h2 className="thumb-page-header">
                Random (
                <LinkLike target={RoutingConfig.urlFor(RandomSlideshowPage)}>
                  Slides
                </LinkLike>
                )
              </h2>
            </div>
          )}
          controls={() => (
            <PaginationWidget
              mode={PaginationWidget.MODE.RANDOM}
              nextButtonTarget={forceReSearch}
            />
          )}
        />
      )}
    />
  );
};

export default RandomItemsPage;
