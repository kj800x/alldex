import React from "react";
import parse from "s-expression";

import { RESULTS_PER_PAGE } from "../../constants";

import SearchContainer from "../containers/SearchContainer";
import PaginationWidget from "../../library/paginationWidget/PaginationWidget";
import RoutingConfig from "../../routing/RoutingConfig";
import { ALBUM_PREFIX } from "../../albums/constants";
import ItemSearch from "../components/ItemSearch";
import SearchSlideshowPage from "./SearchSlideshowPage";
import LinkLike from "../../library/linkLike/LinkLike";

const TYPE_PREFIX = "type:";

// TODO remove dupl
const stripPrefix = prefix => value => value.substring(prefix.length);

function isOperation(tok) {
  return tok === "AND" || tok === "OR";
}

// "foo" => {"tag": "foo"}
// "foo bar" => {"AND": [{"tag": "foo"}, {"tag": "bar"}]}
// "OR foo bar" => {"OR": [{"tag": "foo"}, {"tag": "bar"}]}
// "OR foo album:bar" => {"OR": [{"tag": "foo"}, {"album": "bar"}]}
// "NOT (OR foo album:bar)" => {"NOT": {"OR": [{"tag": "foo"}, {"album": "bar"}]}}
// "ID < 5" => {"ids": ["<", 5]}
// "ID 1 5" => {"ids": [1, 5]}
const convert = sexp => {
  if (Array.isArray(sexp)) {
    let out = {};
    let operation;
    if (sexp[0] === "ID") {
      const [, ...rest] = sexp;
      return { ids: rest };
    }
    if (sexp[0] === "NOT") {
      return { NOT: convert(sexp[1]) };
    }
    if (isOperation(sexp[0])) {
      operation = sexp[0];
      out[operation] = [];
    } else {
      operation = "AND";
      out[operation] = [convert(sexp[0])];
    }
    for (var i = 1; i < sexp.length; i++) {
      out[operation].push(convert(sexp[i]));
    }
    return out;
  } else {
    if (sexp.startsWith(ALBUM_PREFIX)) {
      return { album: stripPrefix(ALBUM_PREFIX)(sexp) };
    } else if (sexp.startsWith(TYPE_PREFIX)) {
      return { type: stripPrefix(TYPE_PREFIX)(sexp).toUpperCase() };
    } else {
      return { tag: sexp };
    }
  }
};

export const queryParser = query => {
  const sexp = parse("(" + query + ")");
  const res = convert(sexp);
  console.log(res);
  return res;
};

const getFilter = query => {
  return queryParser(query);
};

const getOrdering = query => {
  return "desc";
};

const SearchPage = ({
  match: {
    params: { query, page }
  }
}) => {
  return (
    <SearchContainer
      query={{
        filter: getFilter(query),
        ordering: getOrdering(query),
        offset: (parseInt(page, 10) - 1) * RESULTS_PER_PAGE,
        limit: RESULTS_PER_PAGE
      }}
      renderResults={({ itemIds, totalResultsCount }) => (
        <ItemSearch
          itemIds={itemIds}
          header={() => (
            <h2 className="thumb-page-header">
              Search results for "{query}" (
              <LinkLike
                target={RoutingConfig.urlFor(SearchSlideshowPage, {
                  query,
                  position: 1
                })}
              >
                Slides
              </LinkLike>
              )
            </h2>
          )}
          controls={() => (
            <PaginationWidget
              mode={PaginationWidget.MODE.STANDARD}
              backButtonTarget={RoutingConfig.urlFor(SearchPage, {
                query,
                page: parseInt(page) - 1
              })}
              nextButtonTarget={RoutingConfig.urlFor(SearchPage, {
                query,
                page: parseInt(page) + 1
              })}
              currentPage={page}
              pageCount={Math.ceil(totalResultsCount / RESULTS_PER_PAGE)}
            />
          )}
        />
      )}
    />
  );
};

export default SearchPage;
