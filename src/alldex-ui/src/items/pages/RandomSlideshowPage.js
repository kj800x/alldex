import React from "react";

import SearchContainer from "../containers/SearchContainer";
import ItemViewerContainer from "../containers/ItemViewerContainer";
import RandomSlideshowIndicatorWidget from "../components/RandomSlideshowIndicatorWidget";

const SearchSlideshowPage = ({
  match: {
    params: { query, position }
  }
}) => {
  return (
    <SearchContainer
      query={{
        filter: {},
        ordering: "random",
        offset: 0,
        limit: 1
      }}
      renderResults={({ itemIds, forceReSearch }) => (
        <ItemViewerContainer
          id={itemIds[0]}
          bannerHeader={() => (
            <RandomSlideshowIndicatorWidget forceRefresh={forceReSearch} />
          )}
        />
      )}
    />
  );
};

export default SearchSlideshowPage;
