import React from "react";
import parse from "s-expression";

import SearchContainer from "../containers/SearchContainer";
import { ALBUM_PREFIX } from "../../albums/constants";
import SearchSlideshowIndicatorWidgetContainer from "../containers/SearchSlideshowIndicatorWidgetContainer";
import ItemViewerContainer from "../containers/ItemViewerContainer";

const TYPE_PREFIX = "type:";

// TODO remove dupl
const stripPrefix = prefix => value => value.substring(prefix.length);

function isOperation(tok) {
  return tok === "AND" || tok === "OR";
}

// "foo" => {"tag": "foo"}
// "foo bar" => {"AND": [{"tag": "foo"}, {"tag": "bar"}]}
// "OR foo bar" => {"OR": [{"tag": "foo"}, {"tag": "bar"}]}
// "OR foo album:bar" => {"OR": [{"tag": "foo"}, {"album": "bar"}]}
// "NOT (OR foo album:bar)" => {"NOT": {"OR": [{"tag": "foo"}, {"album": "bar"}]}}
// "ID < 5" => {"ids": ["<", 5]}
// "ID 1 5" => {"ids": [1, 5]}
const convert = sexp => {
  if (Array.isArray(sexp)) {
    let out = {};
    let operation;
    if (sexp[0] === "ID") {
      const [, ...rest] = sexp;
      return { ids: rest };
    }
    if (sexp[0] === "NOT") {
      return { NOT: convert(sexp[1]) };
    }
    if (isOperation(sexp[0])) {
      operation = sexp[0];
      out[operation] = [];
    } else {
      operation = "AND";
      out[operation] = [convert(sexp[0])];
    }
    for (var i = 1; i < sexp.length; i++) {
      out[operation].push(convert(sexp[i]));
    }
    return out;
  } else {
    if (sexp.startsWith(ALBUM_PREFIX)) {
      return { album: stripPrefix(ALBUM_PREFIX)(sexp) };
    } else if (sexp.startsWith(TYPE_PREFIX)) {
      return { type: stripPrefix(TYPE_PREFIX)(sexp).toUpperCase() };
    } else {
      return { tag: sexp };
    }
  }
};

export const queryParser = query => {
  const sexp = parse("(" + query + ")");
  const res = convert(sexp);
  console.log(res);
  return res;
};

const getOrdering = query => {
  return "desc";
};

const getFilter = query => {
  return queryParser(query);
};

const SearchSlideshowPage = ({
  match: {
    params: { query, position }
  }
}) => {
  return (
    <SearchContainer
      query={{
        filter: getFilter(query),
        ordering: getOrdering(query),
        offset: parseInt(position, 10) - 1,
        limit: 1
      }}
      renderResults={({ itemIds, totalResultsCount }) => (
        <ItemViewerContainer
          id={itemIds[0]}
          bannerHeader={() => (
            <SearchSlideshowIndicatorWidgetContainer
              position={parseInt(position, 10)}
              totalResultsCount={totalResultsCount}
              query={query}
            />
          )}
        />
      )}
    />
  );
};

export default SearchSlideshowPage;
