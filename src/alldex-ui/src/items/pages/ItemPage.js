import React from "react";

import ItemViewerContainer from "../containers/ItemViewerContainer";

const ItemPage = ({
  match: {
    params: { itemId }
  }
}) => {
  return <ItemViewerContainer id={parseInt(itemId, 10)} />;
};

export default ItemPage;
