import * as api from "./api";
import apiAction, { buildApiActions } from "../util/apiAction";

export const FETCH_ITEM = buildApiActions("FETCH_ITEM");
export const fetchItem = apiAction(FETCH_ITEM, api.getItem);

export const SEARCH_ITEMS = buildApiActions("SEARCH_ITEMS");
export const searchItems = apiAction(SEARCH_ITEMS, api.searchItems);

export const UPDATE_ITEM = buildApiActions("UPDATE_ITEM");
export const updateItem = apiAction(UPDATE_ITEM, api.patchItem);
