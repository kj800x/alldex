import { gql } from "apollo-server-express";
import db from "alldex-data/db";
import { searchIds, searchTotal } from "alldex-data/items";

export const typeDefs = gql`
  enum CollectionVisibility {
    PUBLIC
    DEFAULT
    HIDDEN
  }

  type Collection {
    id: Int
    title: String
    visibility: CollectionVisibility
  }

  type TagGroup {
    id: Int
    value: String
    color: String
    isHidden: Boolean
    isDeleted: Boolean
    special: String
    collection: Collection
    tags: [Tag]
  }

  type Tag {
    id: Int
    isDeleted: Boolean
    value: String
    description: String
    tagGroup: TagGroup
    collection: Collection
    items: [Item]
  }

  type Album {
    id: Int
    isDeleted: Boolean
    title: String
    description: String
    collection: Collection
    entries: [AlbumEntry]
  }

  type AlbumEntry {
    album: Album
    item: Item
    position: Int
  }

  interface Item {
    id: Int
    isDeleted: Boolean
    sourceUrl: String
    title: String
    description: String
    fingerprint: [Int]
    sourceKey: String
    albums: [AlbumEntry]
    tags: [Tag]
    duplicates: [Duplicate]
    collection: Collection
  }

  type ImageItem implements Item {
    id: Int
    isDeleted: Boolean
    sourceUrl: String
    title: String
    description: String
    fingerprint: [Int]
    sourceKey: String
    albums: [AlbumEntry]
    tags: [Tag]
    duplicates: [Duplicate]
    collection: Collection
    file: String
  }

  type ImageSetItem implements Item {
    id: Int
    isDeleted: Boolean
    sourceUrl: String
    title: String
    description: String
    fingerprint: [Int]
    sourceKey: String
    albums: [AlbumEntry]
    tags: [Tag]
    duplicates: [Duplicate]
    collection: Collection
    file: String
  }

  type TextItem implements Item {
    id: Int
    isDeleted: Boolean
    sourceUrl: String
    title: String
    description: String
    fingerprint: [Int]
    sourceKey: String
    albums: [AlbumEntry]
    tags: [Tag]
    duplicates: [Duplicate]
    collection: Collection
    content: String
  }

  type VideoItem implements Item {
    id: Int
    isDeleted: Boolean
    sourceUrl: String
    title: String
    description: String
    fingerprint: [Int]
    sourceKey: String
    albums: [AlbumEntry]
    tags: [Tag]
    duplicates: [Duplicate]
    collection: Collection
    file: String
  }

  type UrlItem implements Item {
    id: Int
    isDeleted: Boolean
    sourceUrl: String
    title: String
    description: String
    fingerprint: [Int]
    sourceKey: String
    albums: [AlbumEntry]
    tags: [Tag]
    duplicates: [Duplicate]
    collection: Collection
    url: String
  }

  type Duplicate {
    itemA: Item
    itemB: Item
    similarity: Float
  }

  type WizardKeybind {
    id: Int
    key: String
    action: String
  }

  type Wizard {
    id: Int
    name: String
    keybinds: [WizardKeybind]
  }

  type Filter {
    id: Int
    name: String
    query: String
    defaultStatus: String
  }

  type DuplicatesPage {
    results: [Duplicate]
    offset: Int
    pageSize: Int
    total: Int
    hasMore: Boolean
  }

  type ItemsPage {
    results: [Item]
    offset: Int
    pageSize: Int
    total: Int
    hasMore: Boolean
  }

  type Query {
    getItem(id: Int): Item
    getItems(
      filter: String
      ordering: String
      offset: Int
      pageSize: Int
    ): ItemsPage
    getWizards: [Wizard]
    getFilters: [Filter]
    getTagGroups: [TagGroup]
    getTag(id: Int): Tag
    getTagByName(value: String): Tag
    getTags: [Tag]
    getAlbums: [Album]
    getAlbum(id: Int): Album
    getAlbumByName(title: String): Album
    getDuplicates(offset: Int, pageSize: Int): DuplicatesPage
    getCollections: [Collection]
  }

  input ItemInput {
    id: Int
    isDeleted: Boolean
    sourceUrl: String
    title: String
    description: String
    sourceKey: String
  }

  input TagInput {
    id: Int
    isDeleted: Boolean
    value: String
    description: String
    tagGroupId: Int
  }

  input TagGroupInput {
    id: Int
    isDeleted: Boolean
    value: String
    color: String
    isHidden: Boolean
  }

  input AlbumInput {
    id: Int
    isDeleted: Boolean
    title: String
    description: String
  }

  type Mutation {
    updateItem(item: ItemInput): Item
    upsertTag(tag: TagInput): Tag
    upsertAlbum(album: AlbumInput): Album

    associateTagAndItem(itemId: Int, tagId: Int): Item
    associateAlbumAndItem(itemId: Int, albumId: Int): Item
    dissociateTagAndItem(itemId: Int, tagId: Int): Item
    dissociateAlbumAndItem(itemId: Int, albumId: Int): Item

    reorderItemInAlbum(itemId: Int, albumId: Int, newPosition: Int): AlbumEntry

    importItems(sources: [String], tags: [String], albums: [String]): Tag
    reimportItem(itemId: Int): Item
    enhanceItem(itemId: Int, enhancementSource: String): Item
    bulkUpdate(filter: String, action: String): [Item]

    runJob(jobTitle: String, parameters: String): String

    createWizard: Wizard
    updateWizard(id: Int, name: String): Wizard
    addWizardKeybind(wizardId: Int): WizardKeybind
    updateWizardKeybind(id: Int, key: String, action: String): WizardKeybind
    deleteWizardKeybind(id: Int): Int
    deleteWizard(id: Int): Int
    createFilter: Filter
    updateFilter(id: Int, name: String, query: String, defaultStatus: String): Filter
    deleteFilter(id: Int): Int
  }
`;

const unbox = async p => (await p)[0];

const standardRetrieve = table => column => id =>
  unbox(
    db(table)
      .pluck(column)
      .where({ id })
  );

const abstractItemResolvers = {
  id: id => {
    return id;
  },
  isDeleted: standardRetrieve("items")("isDeleted"),
  sourceUrl: standardRetrieve("items")("sourceUrl"),
  title: standardRetrieve("items")("title"),
  description: standardRetrieve("items")("description"),
  fingerprint: standardRetrieve("items")("fingerprint"),
  sourceKey: standardRetrieve("items")("sourceKey"),
  collection: standardRetrieve("items")("collection_id"),
  albums: id =>
    db("albummap")
      .where({ item_id: id })
      .select("album_id", "item_id"),
  tags: id =>
    db("tagmap")
      .where({ item_id: id })
      .pluck("tag_id"),
  duplicates: id =>
    db("duplicates")
      .where({ item_a_id: id })
      .orWhere({ item_b_id: id })
      .select("item_a_id", "item_b_id")
};

export const resolvers = {
  Collection: {
    id: id => id,
    title: standardRetrieve("collections")("title"),
    visibility: standardRetrieve("collections")("visibility")
  },
  TagGroup: {
    id: id => id,
    value: standardRetrieve("taggroups")("value"),
    color: standardRetrieve("taggroups")("color"),
    isHidden: standardRetrieve("taggroups")("isHidden"),
    isDeleted: () => false,
    special: standardRetrieve("taggroups")("special"),
    collection: standardRetrieve("taggroups")("collection_id"),
    tags: id =>
      db("tags")
        .where({ taggroup_id: id })
        .pluck("id")
  },
  Tag: {
    id: id => id,
    isDeleted: standardRetrieve("tags")("isDeleted"),
    value: standardRetrieve("tags")("value"),
    description: standardRetrieve("tags")("description"),
    tagGroup: standardRetrieve("tags")("taggroup_id"),
    collection: standardRetrieve("tags")("collection_id"),
    items: id =>
      db("tagmap")
        .where({ tag_id: id })
        .pluck("item_id")
  },
  Album: {
    id: id => id,
    isDeleted: standardRetrieve("albums")("isDeleted"),
    title: standardRetrieve("albums")("title"),
    description: standardRetrieve("albums")("description"),
    collection: standardRetrieve("albums")("collection_id"),
    entries: id =>
      db("albummap")
        .where({ album_id: id })
        .select("album_id", "item_id")
  },
  AlbumEntry: {
    album: ({ album_id }) => album_id,
    item: ({ item_id }) => item_id,
    position: ({ album_id, item_id }) =>
      unbox(
        db("albummap")
          .where({ album_id, item_id })
          .pluck("position")
      )
  },
  DuplicatesPage: {
    results: ({ offset, pageSize }) =>
      db("duplicates")
        .select(["item_a_id", "item_b_id"])
        .orderBy("similarity", "desc")
        .offset(offset)
        .limit(pageSize),
    offset: ({ offset }) => offset,
    pageSize: ({ pageSize }) => pageSize,
    total: async () => (await unbox(db("duplicates").count()))["count(*)"],
    hasMore: async ({ offset, pageSize }) =>
      offset + pageSize < (await unbox(db("duplicates").count()))["count(*)"]
  },
  ItemsPage: {
    results: ({ offset, pageSize, filter, ordering }) =>
      searchIds({ filter, ordering, limit: pageSize, offset }),
    offset: ({ offset }) => offset,
    pageSize: ({ pageSize }) => pageSize,
    total: ({ filter }) => searchTotal({ filter }),
    hasMore: async ({ offset, pageSize, filter }) =>
      offset + pageSize < (await searchTotal({ filter }))
  },
  Duplicate: {
    itemA: ({ item_a_id }) => item_a_id,
    itemB: ({ item_b_id }) => item_b_id,
    similarity: ({ item_a_id, item_b_id }) =>
      unbox(
        db("duplicates")
          .where({ item_a_id, item_b_id })
          .pluck("similarity")
      )
  },
  Filter: {
    id: id => id,
    name: id =>
      unbox(
        db("filters")
          .where({ id })
          .pluck("name")
      ),
    query: id =>
      unbox(
        db("filters")
          .where({ id })
          .pluck("query")
      ),
    defaultStatus: id =>
      unbox(
        db("filters")
          .where({ id })
          .pluck("defaultStatus")
      ),
  },
  WizardKeybind: {
    id: id => id,
    key: id =>
      unbox(
        db("wizard_keybinds")
          .where({ id })
          .pluck("key")
      ),
    action: id =>
      unbox(
        db("wizard_keybinds")
          .where({ id })
          .pluck("action")
      )
  },
  Wizard: {
    id: id => id,
    name: id =>
      unbox(
        db("wizards")
          .where({ id })
          .pluck("name")
      ),
    keybinds: wizard_id =>
      db("wizard_keybinds")
        .where({ wizard_id })
        .pluck("id")
  },
  Item: {
    __resolveType: async item_id => {
      if ((await db("images").where({ item_id })).length > 0) {
        return "ImageItem";
      }
      if ((await db("imagesets").where({ item_id })).length > 0) {
        return "ImageSetItem";
      }
      if ((await db("texts").where({ item_id })).length > 0) {
        return "TextItem";
      }
      if ((await db("videos").where({ item_id })).length > 0) {
        return "VideoItem";
      }
      if ((await db("urls").where({ item_id })).length > 0) {
        return "UrlItem";
      }
    }
  },
  ImageItem: {
    ...abstractItemResolvers,
    file: item_id =>
      unbox(
        db("images")
          .where({ item_id })
          .pluck("file")
      )
  },
  ImageSetItem: {
    ...abstractItemResolvers,
    file: item_id =>
      unbox(
        db("imagesets")
          .where({ item_id })
          .pluck("file")
      )
  },
  TextItem: {
    ...abstractItemResolvers,
    content: item_id =>
      unbox(
        db("texts")
          .where({ item_id })
          .pluck("content")
      )
  },
  VideoItem: {
    ...abstractItemResolvers,
    file: item_id =>
      unbox(
        db("videos")
          .where({ item_id })
          .pluck("file")
      )
  },
  UrlItem: {
    ...abstractItemResolvers,
    url: item_id =>
      unbox(
        db("urls")
          .where({ item_id })
          .pluck("url")
      )
  },
  Query: {
    getTagGroups: () => db("taggroups").pluck("id"),

    getItem: (_parent, { id }) => id,
    getItems: (_parent, { filter, ordering, pageSize, offset }) => ({
      pageSize: pageSize || 25,
      offset: offset || 0,
      filter: JSON.parse(filter || "{}"),
      ordering: ordering
    }),

    getWizards: () => db("wizards").pluck("id"),
    getFilters: () => db("filters").pluck("id"),

    getTag: (_parent, { id }) => id,
    getTags: () => db("tags").pluck("id"),
    getTagByName: (_parent, { value }) =>
      unbox(
        db("tags")
          .pluck("id")
          .where({ value })
      ),

    getAlbum: (_parent, { id }) => id,
    getAlbums: () => db("albums").pluck("id"),
    getAlbumByName: (_parent, { title }) =>
      unbox(
        db("albums")
          .pluck("id")
          .where({ title })
      ),

    getDuplicates: (_parent, { pageSize, offset }) => ({
      pageSize: pageSize || 25,
      offset: offset || 0
    }),

    getCollections: () => db("collections").pluck("id")
  },
  Mutation: {
    upsertAlbum: (_parent, { album: { id, isDeleted, title, description } }) =>
      upsert("albums", { id, isDeleted, title, description }, "id", "id"),
    upsertTag: (
      _parent,
      { tag: { id, isDeleted, value, description, tagGroupId } }
    ) =>
      upsert(
        "tags",
        { id, isDeleted, value, description, taggroup_id: tagGroupId },
        "id",
        "id"
      ),
    createWizard: () => unbox(db("wizards").insert({ "name": "New Wizard" })),
    updateWizard: async (_parent, { id, name }) => { await db("wizards").where({ id }).update({ name }); return id },
    addWizardKeybind: (_parent, { wizardId }) => unbox(db("wizard_keybinds").insert({ "key": "", "action": "", "wizard_id": wizardId })),
    updateWizardKeybind: async (_parent, { id, key, action }) => { await db("wizard_keybinds").where({ id }).update({ key, action }); return id },
    deleteWizardKeybind: async (_parent, { id }) => { await db("wizard_keybinds").where({ id }).del(); return id },
    deleteWizard: async (_parent, { id }) => {
      await db("wizard_keybinds").where({ wizard_id: id }).del();
      await db("wizards").where({ id }).del();
      return id;
    },
    createFilter: () => unbox(db("filters").insert({ "name": "New Filter", "query": "", "defaultStatus": "" })),
    updateFilter: async (_parent, { id, name, query, defaultStatus }) => { await db("filters").where({ id }).update({ name, query, defaultStatus }); return id },
    deleteFilter: async (_parent, { id }) => { await db("filters").where({ id }).del(); return id },
  }
};

const upsert = async (table, values, key, returning) => {
  if (values[key] === undefined) {
    return await unbox(db(table).insert(values, [returning]));
  } else {
    await db(table)
      .where({ [key]: values[key] })
      .update(values);
    return await unbox(
      db(table)
        .where({ [key]: values[key] })
        .pluck(returning)
    );
  }
};

const peek = arg => console.log(arg) || arg;
