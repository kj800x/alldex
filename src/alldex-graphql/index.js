import { ApolloServer } from "apollo-server-express";
import { typeDefs, resolvers } from "./schema";
import express from "express";
import path from "path";

// Hack to get __dirname in an ES6 module:
const __dirname = path.resolve(
  path.dirname(decodeURI(new URL(import.meta.url).pathname))
);

const apolloServer = new ApolloServer({
  // These will be defined for both new or existing servers
  typeDefs,
  resolvers,
  tracing: true,
  playground: {
    cdnUrl: "http://localhost:8000/graphqlstatic",
    version: "1.7.26"
  }
});

export const applyGraphql = app => {
  app.use(
    "/graphqlstatic",
    express.static(path.join(__dirname, "playground_static"))
  );
  apolloServer.applyMiddleware({ app });
};
