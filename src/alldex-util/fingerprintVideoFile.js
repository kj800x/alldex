import thumbsupply from "thumbsupply";
import fingerprintImageFile from "./fingerprintImageFile";

export default async file => {
  const thumbnail = await thumbsupply.generateThumbnail(file);
  return await fingerprintImageFile(thumbnail);
};
