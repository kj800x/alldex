import { exec } from "child_process";

const pipeline = [
  "-resize 128x128",
  "-modulate 100,0,100",
  "-blur 0x3",
  "-auto-level",
  "-equalize",
  "-resize 8x8!",
  "-colorspace gray",
  "-depth 4"
];

export default async filename => {
  const framePosition = filename.indexOf(".gif") !== -1 ? "[0]" : "";
  const command = `convert \"${filename}\"${framePosition} ${pipeline.join(
    " "
  )} gray:-`;
  return await new Promise((accept, reject) => {
    exec(command, { encoding: "buffer" }, function(err, stdout) {
      if (stdout.length !== 32) {
        console.error("Fingerprint was not 32 bytes!!", stdout, err);
        reject("Fingerprint was not 32 bytes!!");
      }
      if (err) {
        reject(err);
      } else {
        accept(stdout);
      }
    });
  });
};
