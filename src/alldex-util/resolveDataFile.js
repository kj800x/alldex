import path from "path";

// Hack to get __dirname in an ES6 module:
const __dirname = path.resolve(
  path.dirname(decodeURI(new URL(import.meta.url).pathname))
);

// Resolve a file in the data directory
// Input: A "file" value stored in the database
// Output: A string containing the absolute path to that file
export default file =>
  __dirname +
  "/../../data/" +
  decodeURI(file.split("?")[0])
    .replace("%3F", "?")
    .replace("\\'", "'");
