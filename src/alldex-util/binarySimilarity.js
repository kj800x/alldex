import xor from "buffer-xor";

export default function binarySimilarity(bufferA, bufferB) {
  const xorBuffer = xor(bufferA, bufferB);
  let acc = 0;
  for (let byte of xorBuffer) {
    const nbyte = ~byte;
    const high = (nbyte & 0xf0) >> 4;
    const low = nbyte & 0x0f;
    acc += high + low;
  }
  return acc / (2.0 * xorBuffer.length * 0x0f);
}
