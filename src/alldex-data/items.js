import db from "./db";

import { removeTagMapEntries } from "./tags";
import { removeAlbumMapEntries } from "./albums";

import knexHelpers from "./util/knexHelpers";
const { createdNow, filterKeys, updatedNow } = knexHelpers(db);
const USER_SAFE_KEYS = [
  "sourceUrl",
  "isDeleted",
  "title",
  "description",
  "fingerprint",
  "sourceKey"
];
const makeSafe = object => filterKeys(USER_SAFE_KEYS, object);

export const TABLE_LOOKUP_TABLE = {
  IMAGE: "images",
  IMAGESET: "imagesets",
  TEXT: "texts",
  URL: "urls",
  VIDEO: "videos"
};

const firstElement = arr => arr[0];

export const fetchHydration = async id => {
  const imagesPromise = db("images")
    .select("file")
    .where({ item_id: id });
  const imagesetsPromise = db("imagesets")
    .select("file")
    .where({ item_id: id });
  const textsPromise = db("texts")
    .select("content")
    .where({ item_id: id });
  const urlsPromise = db("urls")
    .select("url")
    .where({ item_id: id });
  const videosPromise = db("videos")
    .select("file")
    .where({ item_id: id });

  const [images, imagesets, texts, urls, videos] = await Promise.all([
    imagesPromise,
    imagesetsPromise,
    textsPromise,
    urlsPromise,
    videosPromise
  ]);
  if (images.length !== 0) {
    return {
      type: "IMAGE",
      ...images[0]
    };
  }
  if (imagesets.length !== 0) {
    return {
      type: "IMAGESET",
      ...imagesets[0]
    };
  }
  if (texts.length !== 0) {
    return {
      type: "TEXT",
      ...texts[0]
    };
  }
  if (urls.length !== 0) {
    return {
      type: "URL",
      ...urls[0]
    };
  }
  if (videos.length !== 0) {
    return {
      type: "VIDEO",
      ...videos[0]
    };
  }
};

export const fetchHydratedItem = async id => {
  const tagmapPromise = db("tagmap")
    .pluck("tag_id")
    .leftJoin("tags", "tags.id", "tag_id")
    .where({ item_id: id, isDeleted: false });
  const albummapPromise = db("albummap")
    .select(["album_id", "position"])
    .leftJoin("albums", "albums.id", "album_id")
    .where({ item_id: id, isDeleted: false });
  const itemDataPromise = db("items")
    .select([
      "title",
      "sourceUrl",
      "description",
      "fingerprint",
      "isDeleted",
      "sourceKey"
    ])
    .where({ id })
    .first();
  const hydratedObjectPromise = fetchHydration(id);
  const [tagIds, albumResults, itemData, hydratedObject] = await Promise.all([
    tagmapPromise,
    albummapPromise,
    itemDataPromise,
    hydratedObjectPromise
  ]);

  if (itemData.isDeleted) {
    return { id, tags: [], albums: [] }; // TODO: what are we supposed to return when the item doesn't exist
  }

  return {
    id,
    ...itemData,
    ...hydratedObject,
    tags: tagIds,
    albums: albumResults
  };
};

export const create = async payload =>
  db("items")
    .insert(
      createdNow({
        title: payload.title,
        sourceUrl: payload.url,
        description: payload.description
      })
    )
    .then(firstElement);

export const update = (id, payload) =>
  db("items")
    .update(updatedNow(makeSafe(payload)))
    .where({ id });

export const setFingerprint = (id, fingerprint) =>
  db("items")
    .update(updatedNow({ fingerprint }))
    .where({ id });

export const mergeItems = ({}) => Promise.reject("Method not implemented");

export const hardDel = id => {
  function delFromSubTable(hydratedObject) {
    if (hydratedObject) {
      const table = TABLE_LOOKUP_TABLE[hydratedObject.type];
      return db(table)
        .where({ item_id: id })
        .del();
    }
  }

  function delFromMainTable() {
    return db("items")
      .where({ id })
      .del();
  }

  const removeFromSubtablePromise = fetchHydration(id).then(delFromSubTable);
  const removeTagMapEntriesPromise = removeTagMapEntries(id);
  const removeAlbumMapEntriesPromise = removeAlbumMapEntries(id);

  return Promise.all([
    removeFromSubtablePromise,
    removeAlbumMapEntriesPromise,
    removeTagMapEntriesPromise
  ]).then(delFromMainTable);
};

export const softDel = id => {
  return db("items")
    .update(updatedNow({ isDeleted: true }))
    .where({ id });
};

export const del = softDel;

// An ItemType is one of:
//  - "IMAGE"
//  - "IMAGESET"
//  - "VIDEO"
//  - "URL"
//  - "TEXT"

// A Filter is one of
//   - {"tag": "... TAG NAME ..."}
//   - {"tagId": TAG_ID}
//   - {"album": "... ALBUM NAME ..."}
//   - {"albumId": ALBUM_ID}
//   - {"ids": [... ID VALUES ...] || [('>' || '<' || '>=' || '<='), COMP_VALUE]}
//   - {"type": ItemType}
//   - {"OR": [... Filter ...]}
//   - {"AND": [... Filter ...]}
//   - {"NOT": Filter}
function applyFilter(filter) {
  return builder => {
    if (!filter) {
      return builder;
    }
    if (filter.tag) {
      return builder.whereIn(
        ["id"],
        db("tagmap")
          .innerJoin("tags", "tagmap.tag_id", "tags.id")
          .select("item_id")
          .where({ "tags.value": filter.tag })
      );
    } else if (filter.tagId) {
      return builder.whereIn(
        ["id"],
        db("tagmap")
          .select("item_id")
          .where({ tag_id: filter.tagId })
      );
    } else if (filter.album) {
      return builder.whereIn(
        ["id"],
        db("albummap")
          .innerJoin("albums", "albummap.album_id", "albums.id")
          .select("item_id")
          .where({ "albums.title": filter.album })
      );
    } else if (filter.albumId) {
      return builder.whereIn(
        ["id"],
        db("albummap")
          .select("item_id")
          .where({ album_id: filter.albumId })
      );
    } else if (filter.ids) {
      if (["<", ">", "<=", ">="].includes(filter.ids[0])) {
        return builder.where("id", filter.ids[0], filter.ids[1]);
      } else {
        return builder.whereIn(["id"], filter.ids);
      }
    } else if (filter.type) {
      return builder.whereIn(
        ["id"],
        db(TABLE_LOOKUP_TABLE[filter.type]).select("item_id")
      );
    } else if (filter.OR) {
      return filter.OR.reduce((b, f) => b.orWhere(applyFilter(f)), builder);
    } else if (filter.AND) {
      return filter.AND.reduce((b, f) => b.andWhere(applyFilter(f)), builder);
    } else if (filter.NOT) {
      return builder.whereNot(applyFilter(filter.NOT));
    }
    return builder;
  };
}

// TODO: Some of this should probably be pulled out into a service
// An ordering is one of
//    - "asc"
//    - "desc"
//    - "random"
//    - {"album": "...ALBUM NAME..."}
//    - {"albumId": ALBUM_ID}
function applyOrdering(ordering) {
  ordering = ordering || "asc";
  return query => {
    if (ordering === "asc") {
      return query.orderBy("id", "asc");
    } else if (ordering === "desc") {
      return query.orderBy("id", "desc");
    } else if (ordering === "random") {
      return query.orderBy(db.raw("RANDOM()"));
    } else if (ordering.albumId) {
      return query
        .join("albummap", "items.id", "albummap.item_id")
        .where("albummap.album_id", ordering.albumId)
        .orderBy("albummap.position", "asc");
    } else if (ordering.album) {
      return query
        .join("albummap", "items.id", "albummap.item_id")
        .where(
          "albummap.album_id",
          db("albums")
            .select("id")
            .where("title", ordering.album)
        )
        .orderBy("albummap.position", "asc");
    }
  };
}

export const searchIds = async ({ filter, ordering, limit, offset }) => {
  let query = db("items")
    .pluck("id")
    .where(applyFilter(filter))
    .andWhere({ isDeleted: false });

  if (limit) {
    query = query.limit(limit);
  }

  if (offset) {
    query = query.offset(offset);
  }

  return await applyOrdering(ordering)(query);
};

export const searchTotal = async ({ filter }) => {
  return await db("items")
    .where(applyFilter(filter))
    .andWhere({ isDeleted: false })
    .count()
    .then(res => res[0]["count(*)"]);
};

export const search = async ({ filter, ordering, limit, offset }) => {
  const resultIds = await searchIds({ filter, ordering, limit, offset });

  return {
    count: await searchTotal({ filter }),
    results: await Promise.all(resultIds.map(fetchHydratedItem))
  };
};

const isInteger = str => /^[0-9]*$/.test(str);

export const fetchItemBySourceKey = async sourceKey => {
  const id = isInteger(sourceKey)
    ? parseInt(sourceKey, 10)
    : (await db("items")
        .where("sourceKey", sourceKey)
        .pluck("id"))[0];
  return id ? await fetchHydratedItem(id) : null;
};
