import db from "./db";

export const fetchDuplicatesForId = async ({ limit, offset, id }) => {
  return await db("duplicates")
    .select("item_a_id", "item_b_id", "similarity")
    .where("item_a_id", id)
    .orWhere("item_b_id", id)
    .orderBy("similarity", "desc")
    .limit(limit)
    .offset(offset)
    .map(r =>
      r.item_a_id == id
        ? {
            id: r.item_b_id,
            similarity: r.similarity
          }
        : {
            id: r.item_a_id,
            similarity: r.similarity
          }
    );
};

export const fetchDuplicates = async ({ limit, offset }) => {
  return await db("duplicates")
    .select("item_a_id", "item_b_id", "similarity")
    .orderBy("similarity", "desc")
    .limit(limit)
    .offset(offset)
    .map(r => ({
      ids: [r.item_a_id, r.item_b_id],
      similarity: r.similarity
    }));
};
