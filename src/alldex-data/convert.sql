-- A script to convert from the old program's database schema into the new program's database schema
-- Run it like so:
-- cp db.db newdb.db; sqlite3 newdb.db < convert.sql

PRAGMA foreign_keys=OFF;

-- Old database schema:
-- CREATE TABLE `photos` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `file` TEXT, `sourceUrl` TEXT, `fingerprint` BLOB, `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL);
-- CREATE TABLE sqlite_sequence(name,seq);
-- CREATE TABLE `albums` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `title` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL);
-- CREATE TABLE `taggroups` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `value` VARCHAR(255), `color` VARCHAR(255), `hidden` TINYINT(1), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL);
-- CREATE TABLE `tags` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `value` VARCHAR(255), `description` TEXT, `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, `taggroupId` INTEGER REFERENCES `taggroups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE);
-- CREATE TABLE `albummap` (`ordering` INTEGER, `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, `photoId` INTEGER NOT NULL REFERENCES `photos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, `albumId` INTEGER NOT NULL REFERENCES `albums` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, PRIMARY KEY (`photoId`, `albumId`));
-- CREATE TABLE `tagmap` (`createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, `photoId` INTEGER NOT NULL REFERENCES `photos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, `tagId` INTEGER NOT NULL REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, PRIMARY KEY (`photoId`, `tagId`));
-- CREATE TABLE `photofingerprintscores` (`score` DOUBLE PRECISION, `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, `photoAId` INTEGER NOT NULL REFERENCES `photos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, `photoBId` INTEGER NOT NULL REFERENCES `photos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, PRIMARY KEY (`photoAId`, `photoBId`));
-- CREATE INDEX `photofingerprintscores_score` ON `photofingerprintscores` (`score`);
-- CREATE INDEX `photofingerprintscores_score_photo_a_id` ON `photofingerprintscores` (`score`, `photoAId`);
-- CREATE INDEX `photofingerprintscores_score_photo_b_id` ON `photofingerprintscores` (`score`, `photoBId`);

-- Rename tables to get out of way of new tables
ALTER TABLE photos RENAME TO old_photos;
ALTER TABLE albums RENAME TO old_albums;
ALTER TABLE taggroups RENAME TO old_taggroups;
ALTER TABLE tags RENAME TO old_tags;
ALTER TABLE albummap RENAME TO old_albummap;
ALTER TABLE tagmap RENAME TO old_tagmap;

-- Drop table with no equivilant
DROP TABLE photofingerprintscores;

-- Create new schema
CREATE TABLE `knex_migrations` (`id` integer not null primary key autoincrement, `name` varchar(255), `batch` integer, `migration_time` datetime);
CREATE TABLE `knex_migrations_lock` (`index` integer not null primary key autoincrement, `is_locked` integer);
CREATE TABLE `items` (`id` integer not null primary key autoincrement, `created_at` datetime, `updated_at` datetime, `title` varchar(255), `sourceUrl` varchar(1024), `description` text);
CREATE TABLE `images` (`id` integer not null primary key autoincrement, `item_id` integer, `file` varchar(1024), `created_at` datetime, `updated_at` datetime, foreign key(`item_id`) references `items`(`id`));
CREATE TABLE `imagesets` (`id` integer not null primary key autoincrement, `item_id` integer, `file` varchar(1024), `created_at` datetime, `updated_at` datetime, foreign key(`item_id`) references `items`(`id`));
CREATE TABLE `texts` (`id` integer not null primary key autoincrement, `item_id` integer, `content` text, `created_at` datetime, `updated_at` datetime, foreign key(`item_id`) references `items`(`id`));
CREATE TABLE `videos` (`id` integer not null primary key autoincrement, `item_id` integer, `file` varchar(1024), `created_at` datetime, `updated_at` datetime, foreign key(`item_id`) references `items`(`id`));
CREATE TABLE `urls` (`id` integer not null primary key autoincrement, `item_id` integer, `url` varchar(1024), `created_at` datetime, `updated_at` datetime, foreign key(`item_id`) references `items`(`id`));
CREATE TABLE `tags` (`id` integer not null primary key autoincrement, `taggroup_id` integer, `value` varchar(255), `description` text, `created_at` datetime, `updated_at` datetime, foreign key(`taggroup_id`) references `taggroups`(`id`));
CREATE TABLE `taggroups` (`id` integer not null primary key autoincrement, `value` varchar(255), `color` varchar(6), `isHidden` boolean, `special` varchar(7), `created_at` datetime, `updated_at` datetime);
CREATE TABLE `tagmap` (`tag_id` integer, `item_id` integer, foreign key(`tag_id`) references `tags`(`id`), foreign key(`item_id`) references `items`(`id`), primary key (`tag_id`, `item_id`));
CREATE TABLE `albums` (`id` integer not null primary key autoincrement, `title` varchar(255), `description` text, `created_at` datetime, `updated_at` datetime);
CREATE TABLE `albummap` (`album_id` integer, `item_id` integer, `position` integer, foreign key(`album_id`) references `albums`(`id`), foreign key(`item_id`) references `items`(`id`), primary key (`album_id`, `item_id`));

-- Insert knex migration info
INSERT INTO knex_migrations VALUES(1,'0000-initial.js',1,1546411491203);
INSERT INTO knex_migrations_lock VALUES(1,0);

-- Copy data over
INSERT INTO `taggroups` (id, value, color, isHidden, special, created_at, updated_at) SELECT id, value, color, hidden, NULL, createdAt, updatedAt from `old_taggroups`;
INSERT INTO `tags` (id, taggroup_id, value, description, created_at, updated_at) SELECT id, taggroupId, value, description, createdAt, updatedAt from `old_tags`;
INSERT INTO `albums` (id, title, description, created_at, updated_at) SELECT id, title, '', createdAt, updatedAt from `old_albums`;
INSERT INTO `items` (id, created_at, updated_at, title, sourceUrl, description) SELECT id, createdAt, updatedAt, '', sourceUrl, '' from `old_photos`;
INSERT INTO `images` (id, item_id, file, created_at, updated_at) SELECT id, id, file, createdAt, updatedAt from `old_photos`;
INSERT INTO `tagmap` (tag_id, item_id) SELECT tagId, photoId from 'old_tagmap';
INSERT INTO `albummap` (album_id, item_id, position) SELECT albumId, photoId, ordering from 'old_albummap';

-- Change color conventions for taggroups and add special attributes to the special taggroups
UPDATE taggroups SET color = ltrim(color, '#');
UPDATE taggroups SET special = 'default' where id = 2;
UPDATE taggroups SET special = 'import' where id = 1; 

-- Drop old tables
DROP TABLE old_photos;
DROP TABLE old_albums;
DROP TABLE old_taggroups;
DROP TABLE old_tags;
DROP TABLE old_albummap;
DROP TABLE old_tagmap;

PRAGMA foreign_keys=ON;

