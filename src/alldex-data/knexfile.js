module.exports = {
  client: "sqlite3",
  connection: {
    filename: "../../config/db.db"
  },
  useNullAsDefault: true,
  pool: {
    afterCreate: (conn, cb) => conn.run("PRAGMA foreign_keys = ON", cb)
  }
};
