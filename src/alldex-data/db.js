import knex from "knex";

import knexfile from "./knexfile";

const knexdb = knex(knexfile);

async function init() {
  await knexdb.migrate.latest({
    directory: "../alldex-data/migrations"
  });

  await knexdb.seed.run({
    directory: "../alldex-data/seeds"
  });

  console.log("DB up to date");
}

const initPromise = init();

export const ready = () => initPromise;

export default knexdb;
