import db from "./db";

import helpers from "./util/knexHelpers";
const {
  filterKeys,
  insertIfNotExists,
  createdNow,
  updatedNow,
  flattenSingles
} = helpers(db);
const USER_SAFE_KEYS = ["title", "description"];
const makeSafe = object => filterKeys(USER_SAFE_KEYS, object);

const firstElement = arr => arr[0] || null;

export const fetch = id =>
  db("albums")
    .select(["title", "description", "id"])
    .where({ id })
    .andWhere({ isDeleted: false })
    .then(firstElement);

export const fetchAll = () =>
  db("albums")
    .select(["id", "title"])
    .where({ isDeleted: false });

export const create = data =>
  insertIfNotExists("albums", createdNow(makeSafe(data)), ["title"], "id");

export const ensure = async albumNames => {
  const albumData = albumNames.map(name => ({ title: name, description: "" }));

  const res = await Promise.all(albumData.map(create));
  return flattenSingles(res);
};

export const update = (id, data) =>
  db("albums")
    .update(updatedNow(makeSafe(data)))
    .where({ id });

export const hardDel = async id => {
  // Remove entries from the tagmap where it's tag is this
  await db("albummap")
    .where({ album_id: id })
    .del();

  // Delete the tag itself
  return db("albums")
    .where({ id })
    .del();
};

export const softDel = id => {
  return db("albums")
    .update(updatedNow({ isDeleted: true }))
    .where({ id });
};

export const del = softDel;

export const associateItem = async (album_id, item_id) => {
  // Find length of album, used for the new element's position
  const position =
    (await db("albummap")
      .count()
      .where({ album_id }))[0]["count(*)"] + 1;
  return (await insertIfNotExists(
    "albummap",
    { album_id, item_id, position },
    ["album_id", "item_id"],
    "position"
  ))[0];
};

export const disassociateItem = async (album_id, item_id) => {
  // Find entry's position
  const position = await db("albummap")
    .pluck("position")
    .where({ album_id, item_id })
    .then(firstElement);

  // Remove albummap entry
  await db("albummap")
    .where({ album_id, item_id })
    .del();

  // Adjust position of following elements
  return db("albummap")
    .decrement("position")
    .where({ album_id })
    .andWhere("position", ">", position);
};

export const removeAlbumMapEntries = async item_id => {
  const album_ids = await db("albummap")
    .pluck("album_id")
    .where({ item_id });

  return Promise.all(
    album_ids.map(album_id => disassociateItem(album_id, item_id))
  );
};
