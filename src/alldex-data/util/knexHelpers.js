const filterKeys = (matchingKeys, obj) => {
  const out = {};
  matchingKeys.forEach(key => {
    if (obj[key] !== undefined) {
      out[key] = obj[key];
    }
  });
  return out;
};

module.exports = knex => ({
  filterKeys: filterKeys,
  flattenSingles: arr => arr.map(elem => elem[0]), // TODO this can be pulled out into a generic helper
  createdNow: obj => Object.assign({}, { created_at: knex.fn.now() }, obj),
  updatedNow: obj => Object.assign({}, { updated_at: knex.fn.now() }, obj),
  insertIfNotExists: (table, obj, matchingKeys, returning = matchingKeys[0]) =>
    knex(table)
      .pluck(returning)
      .where(filterKeys(matchingKeys, obj))
      .then(results => {
        if (results.length === 0) {
          return knex(table)
            .insert(obj)
            .then(() => {
              return knex(table)
                .pluck(returning)
                .where(filterKeys(matchingKeys, obj));
            });
        }
        return results;
      })
});
