// Adds sourceKey and index to items

exports.up = function(knex) {
  return Promise.all([
    knex.schema.table("items", function(t) {
      t.string("sourceKey");
      t.index("sourceKey");
    })
  ]);
};

exports.down = function(knex) {
  return Promise.all([
    knex.schema.table("items", function(t) {
      t.dropColumn("sourceKey");
    })
  ]);
};
