exports.up = function(knex) {
  return Promise.all([
    knex.schema.createTable("filters", function(t) {
      t.increments();
      t.string("name", 128);
      t.string("query", 1024);
      t.string("defaultStatus", 7) // "require", "exclude", or ""
    }),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists("filters"),
  ]);
};

exports.config = { transaction: false };
