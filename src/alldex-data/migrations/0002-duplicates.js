// Adds duplicates table and duplicate metadata table

exports.up = function(knex) {
  return Promise.all([
    knex.schema.createTable("duplicates", function(t) {
      t.integer("item_a_id").unsigned();
      t.integer("item_b_id").unsigned();
      t.foreign("item_a_id").references("items.id");
      t.foreign("item_b_id").references("items.id");
      t.float("similarity");
      t.primary(["item_a_id", "item_b_id"]);
      t.index("similarity");
    }),

    knex.schema.createTable("duplicateMetadata", function(t) {
      t.integer("maxItem").unsigned();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists("duplicates"),
    knex.schema.dropTableIfExists("duplicateMetadata")
  ]);
};
