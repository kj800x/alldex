// Adds soft deleted flag

exports.up = function(knex) {
  return Promise.all([
    knex.schema.table("items", function(t) {
      t.boolean("isDeleted")
        .notNullable()
        .defaultTo(false);
    }),
    knex.schema.table("tags", function(t) {
      t.boolean("isDeleted")
        .notNullable()
        .defaultTo(false);
    }),
    knex.schema.table("albums", function(t) {
      t.boolean("isDeleted")
        .notNullable()
        .defaultTo(false);
    })
  ]);
};

exports.down = function(knex) {
  return Promise.all([
    knex.schema.table("items", function(t) {
      t.dropColumn("isDeleted");
    }),
    knex.schema.table("tags", function(t) {
      t.dropColumn("isDeleted");
    }),
    knex.schema.table("albums", function(t) {
      t.dropColumn("isDeleted");
    })
  ]);
};
