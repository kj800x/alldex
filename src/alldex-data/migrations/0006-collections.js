// Adds jobs infrastructure

exports.up = function(knex) {
  return Promise.all([
    knex.schema.createTable("collections", function(t) {
      t.increments();
      t.string("title");
      t.enu("visibility", ["PUBLIC", "DEFAULT", "HIDDEN"]).defaultTo("DEFAULT");
      t.timestamps();
    }),

    knex.schema.table("albums", t => {
      t.integer("collection_id")
        .unsigned()
        .defaultTo(1);
      t.foreign("collection_id").references("collections.id");
    }),

    knex.schema.table("items", t => {
      t.integer("collection_id")
        .unsigned()
        .defaultTo(1);
      t.foreign("collection_id").references("collections.id");
    }),

    knex.schema.table("taggroups", t => {
      t.integer("collection_id")
        .unsigned()
        .defaultTo(1);
      t.foreign("collection_id").references("collections.id");
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists("missions"),
    knex.schema.dropTableIfExists("flows"),
    knex.schema.dropTableIfExists("tasks"),
    knex.schema.dropTableIfExists("subscriptions")
  ]);
};
