exports.up = function(knex) {
  return Promise.all([
    // Item table
    knex.schema.createTable("items", function(t) {
      t.increments();
      t.timestamps();
      t.string("title");
      t.string("sourceUrl", 1024);
      t.text("description");
    }),

    // Item type tables
    knex.schema.createTable("images", function(t) {
      t.increments();
      t.integer("item_id").unsigned();
      t.foreign("item_id").references("items.id");
      t.string("file", 1024);
      t.timestamps();
    }),
    knex.schema.createTable("imagesets", function(t) {
      t.increments();
      t.integer("item_id").unsigned();
      t.foreign("item_id").references("items.id");
      t.string("file", 1024);
      t.timestamps();
    }),
    knex.schema.createTable("texts", function(t) {
      t.increments();
      t.integer("item_id").unsigned();
      t.foreign("item_id").references("items.id");
      t.text("content");
      t.timestamps();
    }),
    knex.schema.createTable("videos", function(t) {
      t.increments();
      t.integer("item_id").unsigned();
      t.foreign("item_id").references("items.id");
      t.string("file", 1024);
      t.timestamps();
    }),
    knex.schema.createTable("urls", function(t) {
      t.increments();
      t.integer("item_id").unsigned();
      t.foreign("item_id").references("items.id");
      t.string("url", 1024);
      t.timestamps();
    }),

    // Tag tables
    knex.schema.createTable("tags", function(t) {
      t.increments();
      t.integer("taggroup_id").unsigned();
      t.foreign("taggroup_id").references("taggroups.id");
      t.string("value");
      t.text("description");
      t.timestamps();
    }),
    knex.schema.createTable("taggroups", function(t) {
      t.increments();
      t.string("value");
      t.string("color", 6);
      t.boolean("isHidden");
      t.string("special", 7);
      t.timestamps();
    }),
    knex.schema.createTable("tagmap", function(t) {
      t.integer("tag_id").unsigned();
      t.integer("item_id").unsigned();
      t.foreign("tag_id").references("tags.id");
      t.foreign("item_id").references("items.id");
      t.primary(["tag_id", "item_id"]);
    }),

    // Album tables
    knex.schema.createTable("albums", function(t) {
      t.increments();
      t.string("title");
      t.text("description");
      t.timestamps();
    }),
    knex.schema.createTable("albummap", function(t) {
      t.integer("album_id").unsigned();
      t.integer("item_id").unsigned();
      t.foreign("album_id").references("albums.id");
      t.foreign("item_id").references("items.id");
      t.integer("position").unsigned();
      t.primary(["album_id", "item_id"]);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists("items"),
    knex.schema.dropTableIfExists("images"),
    knex.schema.dropTableIfExists("videos"),
    knex.schema.dropTableIfExists("texts"),
    knex.schema.dropTableIfExists("urls"),
    knex.schema.dropTableIfExists("tags"),
    knex.schema.dropTableIfExists("taggroups"),
    knex.schema.dropTableIfExists("tagmap"),
    knex.schema.dropTableIfExists("albums"),
    knex.schema.dropTableIfExists("albummap")
  ]);
};

exports.config = { transaction: false };
