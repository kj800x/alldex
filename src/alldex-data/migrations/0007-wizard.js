exports.up = function(knex) {
  return Promise.all([
    knex.schema.createTable("wizards", function(t) {
      t.increments();
      t.string("name");
    }),

    knex.schema.createTable("wizard_keybinds", function(t) {
      t.increments();
      t.integer("wizard_id").unsigned();
      t.foreign("wizard_id").references("wizards.id");
      t.string("key", 1);
      t.string("action", 1024);
    }),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists("wizards"),
    knex.schema.dropTableIfExists("wizard_keybinds")
  ]);
};

exports.config = { transaction: false };
