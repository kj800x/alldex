// Adds a fingerprint column to the items table

exports.up = function(knex) {
  return knex.schema.table("items", function(t) {
    t.binary("fingerprint");
  });
};

exports.down = function(knex) {
  return knex.schema.table("items", function(t) {
    t.dropColumn("fingerprint");
  });
};
