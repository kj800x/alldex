import db from "./db";

import helpers from "./util/knexHelpers";
const {
  filterKeys,
  insertIfNotExists,
  createdNow,
  updatedNow,
  flattenSingles
} = helpers(db);
const USER_SAFE_KEYS = ["item_id", "file"]; // TODO is item_id safe? FIXME
const makeSafe = object => filterKeys(USER_SAFE_KEYS, object);

export const fetch = id =>
  db("images")
    .select(["item_id", "file"])
    .where({ id });

export const fetchAll = () => db("images").select(["id", "item_id", "file"]);

export const create = data =>
  insertIfNotExists("images", createdNow(makeSafe(data)), ["item_id"], "id");

export const update = (id, data) =>
  db("images")
    .update(updatedNow(makeSafe(data)))
    .where({ id });

export const del = id =>
  db("images")
    .where({ id })
    .del();
