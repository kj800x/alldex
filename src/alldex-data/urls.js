import db from "./db";

import helpers from "./util/knexHelpers";
const {
  filterKeys,
  insertIfNotExists,
  createdNow,
  updatedNow,
  flattenSingles
} = helpers(db);
const USER_SAFE_KEYS = ["item_id", "url"]; // TODO is item_id safe? FIXME
const makeSafe = object => filterKeys(USER_SAFE_KEYS, object);

export const fetch = id =>
  db("urls")
    .select(["item_id", "url"])
    .where({ id });

export const fetchAll = () => db("urls").select(["id", "item_id", "url"]);

export const create = data =>
  insertIfNotExists("urls", createdNow(makeSafe(data)), ["item_id"], "id");

export const update = (id, data) =>
  db("urls")
    .update(updatedNow(makeSafe(data)))
    .where({ id });

export const del = id =>
  db("urls")
    .where({ id })
    .del();
