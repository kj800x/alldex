import db from "./db";

import helpers from "./util/knexHelpers";
const { filterKeys, createdNow, updatedNow } = helpers(db);
const USER_SAFE_KEYS = ["value", "color", "isHidden"];
const makeSafe = object => filterKeys(USER_SAFE_KEYS, object);

const firstElement = arr => arr[0] || null;

export const fetch = id =>
  db("taggroups")
    .select(["value", "color", "isHidden", "special", "id"])
    .where({ id })
    .then(firstElement);

export const update = (id, patchObject) =>
  db("taggroups")
    .update(updatedNow(makeSafe(patchObject)))
    .where({ id });

export const fetchAll = () =>
  db("taggroups").select(["id", "value", "color", "isHidden", "special"]);

export const create = data =>
  db("taggroups").insert(createdNow(makeSafe(data)));

export const getDefaultTagGroupId = () =>
  db("taggroups")
    .pluck("id")
    .where({ special: "default" })
    .then(firstElement);

export const getImportTagGroupId = () =>
  db("taggroups")
    .pluck("id")
    .where({ special: "import" })
    .then(firstElement);

export const del = async id => {
  id = parseInt(id, 10);

  // Find the default and import tag group ids
  // Prevent the user from deleting one of those
  const [defaultTagGroupId, importTagGroupId] = await Promise.all([
    getDefaultTagGroupId(),
    getImportTagGroupId()
  ]);
  if (id === defaultTagGroupId || id === importTagGroupId) {
    throw new Error("Cannot delete default or import tag group");
  }

  // Update all tags in the group we are deleting, and update them to the default group
  await db("tags")
    .update({ taggroup_id: defaultTagGroupId })
    .where({ taggroup_id: id });

  // Delete the tag group
  return db("taggroups")
    .where({ id })
    .del();
};
