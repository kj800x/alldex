import db from "./db";

import { getDefaultTagGroupId } from "./taggroups";

import helpers from "./util/knexHelpers";
const {
  filterKeys,
  insertIfNotExists,
  createdNow,
  updatedNow,
  flattenSingles
} = helpers(db);
const USER_SAFE_KEYS = ["taggroup_id", "value", "description"];
const makeSafe = object => filterKeys(USER_SAFE_KEYS, object);

const firstElement = arr => arr[0] || null;

export const fetch = id =>
  db("tags")
    .select(["value", "description", "taggroup_id", "id"])
    .where({ id })
    .andWhere({ isDeleted: false })
    .then(firstElement);

export const fetchAll = () =>
  db("tags")
    .select(["id", "value", "taggroup_id"])
    .where({ isDeleted: false });

export const create = async data => {
  if (data.taggroup_id == undefined) {
    data.taggroup_id = await getDefaultTagGroupId();
  }
  const id = (await insertIfNotExists(
    "tags",
    createdNow(makeSafe(data)),
    ["value"],
    "id"
  ))[0];
  return fetch(id);
};

export const ensure = async tagData => {
  const elements = await Promise.all(tagData.map(create));
  return elements.map(elem => elem.id);
};

export const update = (id, data) =>
  db("tags")
    .update(updatedNow(makeSafe(data)))
    .where({ id });

export const hardDel = async id => {
  // Remove entries from the tagmap where it's tag is this
  await db("tagmap")
    .where({ tag_id: id })
    .del();

  // Delete the tag itself
  return db("tags")
    .where({ id })
    .del();
};

export const softDel = id => {
  return db("tags")
    .update(updatedNow({ isDeleted: true }))
    .where({ id });
};

export const del = softDel;

export const merge = async (idA, idB) => {
  // Merge tag B into A. (deletes B, retains A)

  // Find cases where A and B are both tagmapped to an item, and delete the tagmap entries for B
  const doubleEntries = await db("tagmap")
    .joinRaw("cross join tagmap as tagmap2 on tagmap.item_id = tagmap2.item_id")
    .where("tagmap.tag_id", idA)
    .andWhere(db.raw("tagmap2.tag_id"), idB)
    .columns("tagmap.item_id", db.raw("tagmap2.tag_id"));

  for (let doubleEntry of doubleEntries) {
    await db("tagmap")
      .where(doubleEntry)
      .del(); // TODO: Can I do this in parallel (can I do it all at once somehow?)
  }

  // Migrate cases where an item was tagged with tag B but not A
  await db("tagmap")
    .where("tag_id", idB)
    .update({ tag_id: idA });

  // Delete tag B
  await hardDel(idB);

  return "OK";
};

export const removeTagMapEntries = item_id =>
  db("tagmap")
    .where({ item_id })
    .del();

export const associateItem = (tag_id, item_id) =>
  insertIfNotExists("tagmap", { tag_id, item_id }, ["tag_id", "item_id"]);

export const disassociateItem = (tag_id, item_id) =>
  db("tagmap")
    .where({ tag_id, item_id })
    .del();
