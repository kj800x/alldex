import db from "./db";

import helpers from "./util/knexHelpers";
const {
  filterKeys,
  insertIfNotExists,
  createdNow,
  updatedNow,
  flattenSingles
} = helpers(db);
const USER_SAFE_KEYS = ["item_id", "file"]; // TODO is item_id safe? FIXME
const makeSafe = object => filterKeys(USER_SAFE_KEYS, object);

export const fetch = id =>
  db("videos")
    .select(["item_id", "file"])
    .where({ id });

export const fetchAll = () => db("videos").select(["id", "item_id", "file"]);

export const create = data =>
  insertIfNotExists("videos", createdNow(makeSafe(data)), ["item_id"], "id");

export const update = (id, data) =>
  db("videos")
    .update(updatedNow(makeSafe(data)))
    .where({ id });

export const del = id =>
  db("videos")
    .where({ id })
    .del();
