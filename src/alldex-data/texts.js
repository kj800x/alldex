import db from "./db";

import helpers from "./util/knexHelpers";
const {
  filterKeys,
  insertIfNotExists,
  createdNow,
  updatedNow,
  flattenSingles
} = helpers(db);
const USER_SAFE_KEYS = ["item_id", "content"]; // TODO is item_id safe? FIXME
const makeSafe = object => filterKeys(USER_SAFE_KEYS, object);

export const fetch = id =>
  db("texts")
    .select(["item_id", "content"])
    .where({ id });

export const fetchAll = () => db("texts").select(["id", "item_id", "content"]);

export const create = data =>
  insertIfNotExists("texts", createdNow(makeSafe(data)), ["item_id"], "id");

export const update = (id, data) =>
  db("texts")
    .update(updatedNow(makeSafe(data)))
    .where({ id });

export const del = id =>
  db("texts")
    .where({ id })
    .del();
