const buildKnexHelpers = require("../util/knexHelpers");

exports.seed = function(knex) {
  const { createdNow, insertIfNotExists } = buildKnexHelpers(knex);

  return Promise.all([
    insertIfNotExists(
      "taggroups",
      createdNow({
        id: 1,
        special: "default",
        value: "default",
        color: "0000FF",
        isHidden: false
      }),
      ["special"]
    ),
    insertIfNotExists(
      "taggroups",
      createdNow({
        id: 2,
        special: "import",
        value: "import",
        color: "0000FF",
        isHidden: true
      }),
      ["special"]
    )
  ]);
};
