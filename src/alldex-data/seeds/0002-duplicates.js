exports.seed = function(knex) {
  return knex("duplicateMetadata")
    .select("maxItem")
    .then(results => {
      if (results.length === 0) {
        return knex("duplicateMetadata").insert({ maxItem: 0 });
      }
    });
};
