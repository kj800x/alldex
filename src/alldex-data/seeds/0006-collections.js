const buildKnexHelpers = require("../util/knexHelpers");

exports.seed = function(knex) {
  const { createdNow, insertIfNotExists } = buildKnexHelpers(knex);

  return Promise.all([
    insertIfNotExists(
      "collections",
      createdNow({
        id: 1,
        title: "default",
        visibility: "DEFAULT"
      }),
      ["id"]
    )
  ]);
};
