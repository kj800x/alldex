import { makeRouter } from "express-plus";

import {
  fetch,
  fetchAll,
  create,
  update,
  del,
  associateItem,
  disassociateItem
} from "alldex-data/albums";

const router = makeRouter();

router.put("/associate", ({ payload: { album_id, item_id } }) =>
  associateItem(album_id, item_id)
);
router.delete("/associate", ({ payload: { album_id, item_id } }) =>
  disassociateItem(album_id, item_id)
);
router.get("/", () => fetchAll());
router.post("/", ({ payload }) => create(payload));
router.get("/:id", ({ pathParams: { id } }) => fetch(id));
router.patch("/:id", ({ pathParams: { id }, payload }) => update(id, payload));
router.delete("/:id", ({ pathParams: { id } }) => del(id));

export default router.getRouter();
