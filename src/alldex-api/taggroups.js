import { makeRouter } from "express-plus";

import { fetch, fetchAll, update, create, del } from "alldex-data/taggroups";

const router = makeRouter();

router.get("/", () => fetchAll());
router.post("/", ({ payload }) => create(payload));
router.get("/:id", ({ pathParams: { id } }) => fetch(id));
router.patch("/:id", ({ pathParams: { id }, payload }) => update(id, payload));
router.delete("/:id", ({ pathParams: { id } }) => del(id));

export default router.getRouter();
