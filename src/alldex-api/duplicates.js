import { makeRouter } from "express-plus";

import { mergeItems } from "alldex-data/items";
import { fetchDuplicatesForId, fetchDuplicates } from "alldex-data/duplicates";

const router = makeRouter();

router.post("/merge", ({ payload }) => mergeItems(payload));
router.get(
  "/:id",
  ({ pathParams: { id }, queryParams: { limit = 30, offset = 0 } }) =>
    fetchDuplicatesForId({ id, limit, offset })
);
router.get("/", ({ queryParams: { limit = 30, offset = 0 } }) =>
  fetchDuplicates({ limit, offset })
);

export default router.getRouter();
