import express from "express";

import items from "./items";
import tags from "./tags";
import taggroups from "./taggroups";
import albums from "./albums";
import duplicates from "./duplicates";

const router = express.Router();

router.use("/items", items);
router.use("/tags", tags);
router.use("/taggroups", taggroups);
router.use("/albums", albums);
router.use("/duplicates", duplicates);

router.all("*", (_, res) => {
  res
    .status(405) // HTTP status 405: Method Not Allowed
    .send("ERROR: Method Not Allowed");
});

export default router;
