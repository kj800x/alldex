import { makeRouter } from "express-plus";

import {
  fetch,
  fetchAll,
  create,
  update,
  del,
  merge,
  associateItem,
  disassociateItem
} from "alldex-data/tags";

const router = makeRouter();

router.put("/associate", ({ payload: { tag_id, item_id } }) =>
  associateItem(tag_id, item_id)
);
router.delete("/associate", ({ payload: { tag_id, item_id } }) =>
  disassociateItem(tag_id, item_id)
);
router.post("/merge", ({ payload: [tag_a_id, tag_b_id] }) =>
  merge(tag_a_id, tag_b_id)
);
router.get("/", () => fetchAll());
router.post("/", ({ payload }) => create(payload));
router.get("/:id", ({ pathParams: { id } }) => fetch(id));
router.patch("/:id", ({ pathParams: { id }, payload }) => update(id, payload));
router.delete("/:id", ({ pathParams: { id } }) => del(id));

export default router.getRouter();
