import { makeRouter } from "express-plus";

import {
  del,
  fetchHydratedItem,
  create,
  search,
  update
} from "alldex-data/items";
import { importSources } from "alldex-service/import";
import { reimportItem } from "alldex-service/reimport";
import { syncItems } from "alldex-service/syncItems";
import { bulkAction } from "alldex-service/bulkAction";

const router = makeRouter();

router.post("/search", ({ payload }) => search(payload));
router.post("/:id/reimport", ({ pathParams: { id } }) => reimportItem(id));
router.get("/:id", ({ pathParams: { id } }) => fetchHydratedItem(id));
router.patch("/:id", ({ pathParams: { id }, payload }) => update(id, payload));
router.delete("/:id", ({ pathParams: { id } }) => del(id));
router.put("/import", ({ payload }) => importSources(payload));
router.put("/", ({ payload }) => create(payload));
router.put("/sync", ({ payload }) => syncItems(payload));
router.post("/bulk", ({ payload }) => bulkAction(payload));

export default router.getRouter();
