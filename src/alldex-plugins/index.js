import fs from "fs";

const serviceHandlers = {};

function registerService(service, fn) {
  serviceHandlers[service] = serviceHandlers[service]
    ? serviceHandlers[service]
    : [];
  serviceHandlers[service].push(fn);
}

async function loadPlugin(pluginName) {
  const plugin = await import(`../../config/plugins/${pluginName}`);
  console.log(
    `Loading plugin ${plugin.name} from config/plugins/${pluginName}`
  );
  plugin.load(registerService);
}

async function pluginDirectoryExists() {
  return new Promise(acc => {
    fs.access("../../config/plugins", err => {
      acc(!err);
    });
  });
}

function findPlugins() {
  return new Promise((acc, rej) => {
    pluginDirectoryExists().then(doesExist => {
      if (!doesExist) {
        acc([]);
      }
      fs.readdir("../../config/plugins", (err, items) => {
        if (err) {
          rej(err);
          return;
        }
        acc(items);
      });
    });
  });
}

export async function load() {
  const pluginNames = await findPlugins();
  for (let plugin of pluginNames) {
    await loadPlugin(plugin);
  }
  console.log("All plugins loaded");
}

export function getServices(serviceName) {
  return serviceHandlers[serviceName] || [];
}
