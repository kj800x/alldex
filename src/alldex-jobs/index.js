import * as plugins from "alldex-plugins";
import * as db from "alldex-data/db";
import process from "process";
import builtinJobs from "./jobs";
import JobLogger from "./util/JobLogger";
import JOB_BUNDLE from "./util/bundle";
import listAvailableJobs from "./util/listAvailableJobs";

const JOB_NAME_ARGUMENT = 2;
const JOB_ARGUMENTS_START = 3;

const banner = str =>
  `===================================\n== ${str} ==\n===================================`;

async function main() {
  await plugins.load();

  const jobs = plugins.getServices("job").concat(builtinJobs);

  const jobName = process.argv[JOB_NAME_ARGUMENT];
  if (jobName === undefined || jobName === "--help") {
    listAvailableJobs(jobs);
  } else {
    const job = jobs.find(job => job.name === jobName);
    if (job) {
      const logger = new JobLogger(job.name);
      const dryRun = process.argv.includes("--dryRun");
      const forceProcessAll = process.argv.includes("--forceProcessAll");
      const args = process.argv.slice(JOB_ARGUMENTS_START);
      if (dryRun) {
        console.log(banner("Dry Run is enabled!!"));
      }
      if (forceProcessAll) {
        console.log(banner("Force Process All is enabled!!"));
      }
      await db.ready();
      await job.run({
        args,
        bundle: JOB_BUNDLE,
        logger,
        dryRun,
        forceProcessAll
      });
      logger.print();
      logger.write({ config: { dryRun, forceProcessAll, args } });
      console.log("\nJob Completed");
    } else {
      console.warn(`Job ${jobName} not found`);
      listAvailableJobs(jobs);
    }
  }
  db.default.destroy();
}

main().catch(e => console.error(e));
