import { merge } from "alldex-data/tags";

export const name = "merge:duplicate:tags";

export async function run({ logger, bundle: { db }, dryRun }) {
  const duplicateTags = await db("tags")
    .joinRaw("cross join tags as tagsB")
    .where("tags.value", "=", db.raw("tagsB.value"))
    .andWhere("tags.id", "<", db.raw("tagsB.id"))
    .columns("tags.id", db.raw("tagsB.id as idB"));

  for (let result of duplicateTags) {
    console.log("merging", result.id, "and", result.idB);
    logger.log("mergedTags", result);
    if (!dryRun) {
      await merge(result.id, result.idB);
    }
  }
}
