import resolveDataFile from "alldex-util/resolveDataFile";
import fingerprintVideoFile from "alldex-util/fingerprintVideoFile";
import fingerprintImageFile from "alldex-util/fingerprintImageFile";
import { update as updateItem } from "alldex-data/items";

export const name = "generate:fingerprints";

export async function run({ bundle: { db }, logger, dryRun, forceProcessAll }) {
  const imageRows = forceProcessAll
    ? await db("images")
        .leftJoin("items", "items.id", "images.item_id")
        .select("item_id", "file")
    : await db("images")
        .leftJoin("items", "items.id", "images.item_id")
        .whereNull("items.fingerprint")
        .select("item_id", "file");
  for (let result of imageRows) {
    console.log("Image item is missing a fingerprint:", result.file);

    try {
      if (!dryRun) {
        const fingerprint = await fingerprintImageFile(
          resolveDataFile(result.file)
        );
        await updateItem(result.item_id, {
          fingerprint
        });
      }
      logger.log("success", result.item_id);
    } catch (e) {
      console.log("error", e);
      logger.log("fail", result.item_id);
    }
  }

  const videoRows = forceProcessAll
    ? await db("videos")
        .leftJoin("items", "items.id", "videos.item_id")
        .select("item_id", "file")
    : await db("videos")
        .leftJoin("items", "items.id", "videos.item_id")
        .whereNull("items.fingerprint")
        .select("item_id", "file");

  for (let result of videoRows) {
    console.log("Video item is missing a fingerprint:", result.file);

    try {
      if (!dryRun) {
        const fingerprint = await fingerprintVideoFile(
          resolveDataFile(result.file)
        );
        await updateItem(result.item_id, {
          fingerprint
        });
      }
      logger.log("success", result.item_id);
    } catch (e) {
      console.log("error", e);
      logger.log("fail", result.item_id);
    }
  }
}
