import { hardDel as hardDelTag } from "alldex-data/tags";
import { hardDel as hardDelAlbum } from "alldex-data/albums";

export const name = "remove:dead:tag:albums";

export async function run({ bundle: { db }, logger, dryRun }) {
  const deadTags = await db("tags")
    .leftJoin("tagmap", "tags.id", "tagmap.tag_id")
    .whereNull("tagmap.item_id")
    .select("id", "value", "taggroup_id", "description");
  for (let result of deadTags) {
    console.log("pruning", result.value);
    logger.log("tags", result);
    if (!dryRun) {
      await hardDelTag(result.id);
    }
  }

  const deadAlbums = await db("albums")
    .leftJoin("albummap", "albums.id", "albummap.album_id")
    .whereNull("albummap.item_id")
    .select("id", "title", "description");
  for (let result of deadAlbums) {
    console.log("pruning", "album:" + result.title);
    logger.log("albums", result);
    if (!dryRun) {
      await hardDelAlbum(result.id);
    }
  }
}
