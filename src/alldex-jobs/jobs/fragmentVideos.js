import resolveDataFile from "alldex-util/resolveDataFile";
import util from "util";
import fs from "fs";
import execa from "execa";
const fsMove = util.promisify(fs.rename);

export const name = "fragment:videos";

async function isFragmented(file) {
  const { stdout } = await execa("mp4info", [file]);
  return stdout.includes("fragments:  yes");
}

async function fragment(file) {
  await execa("mp4fragment", [file, `${file}.fragmented.mp4`]);
  await fsMove(`${file}.fragmented.mp4`, file);
}

export async function run({ bundle: { db }, logger, dryRun, forceProcessAll }) {
  const videoRows = await db("videos").select("id", "item_id", "file");
  let i = 0;
  for (let result of videoRows) {
    try {
      const file = resolveDataFile(result.file);

      console.log("Checking:", result.file, `... (${i}/${videoRows.length})`);
      if (!forceProcessAll && (await isFragmented(file))) {
        console.log("File is already fragmented");
      } else {
        console.log("File is not fragmented, trying to fragment now...");
        try {
          if (!dryRun) {
            await fragment(file);
          }
          logger.log("success", result.item_id);
        } catch (e) {
          logger.log("fail", result.item_id);
          console.log("Unable to fragment file:", e);
        }
      }
    } catch (e) {
      console.log("FATAL ERROR:", e);
    }
    i++;
  }
}
