import thumbsupply from "thumbsupply";
import resolveDataFile from "alldex-util/resolveDataFile";

const tryLookup = async file => {
  try {
    await thumbsupply.lookupThumbnail(file);
    return true;
  } catch (e) {
    return false;
  }
};

export const name = "generate:thumbnails";

export async function run({ bundle: { db }, logger, dryRun, forceProcessAll }) {
  const videoRows = await db("videos").select("id", "item_id", "file");
  for (let result of videoRows) {
    const file = resolveDataFile(result.file);

    if (forceProcessAll || !(await tryLookup(file))) {
      console.log("Generating thumbnail for:", result.file);
      try {
        if (!dryRun) {
          await thumbsupply.generateThumbnail(file, { forceCreate: true });
        }
        logger.log("success", result.item_id);
      } catch (e) {
        logger.log("fail", result.item_id);
        console.log("Failed to generate thumbnail!", e);
      }
    } else {
      console.log("Thumbnail OK for:", result.file);
    }
  }
}
