import fs from "fs";
import resolveDataFile from "alldex-util/resolveDataFile";

import { hardDel } from "alldex-data/items";

export const name = "remove:broken:items";

export async function run({ bundle: { db }, logger, dryRun }) {
  const imageRows = await db("images").select("id", "item_id", "file");
  for (let result of imageRows) {
    if (!fs.existsSync(resolveDataFile(result.file))) {
      console.log("Removing broken image item:", result.file);
      logger.log("images", result);
      if (!dryRun) {
        await hardDel(result.item_id);
      }
    }
  }

  const videoRows = await db("videos").select("id", "item_id", "file");
  for (let result of videoRows) {
    if (!fs.existsSync(resolveDataFile(result.file))) {
      console.log("Removing broken video item:", result.file);
      logger.log("videos", result);
      if (!dryRun) {
        await hardDel(result.item_id);
      }
    }
  }
}
