import * as generateFingerprints from "./generateFingerprints";
import * as generateThumbnails from "./generateThumbnails";
import * as identifyDuplicates from "./identifyDuplicates";
import * as removeBrokenItems from "./removeBrokenItems";
import * as removeDeadTagAlbums from "./removeDeadTagAlbums";
import * as removeSingleElementAlbums from "./removeSingleElementAlbums";
import * as mergeDuplicateTags from "./mergeDuplicateTags";
import * as fragmentVideos from "./fragmentVideos";

export default [
  generateFingerprints,
  generateThumbnails,
  identifyDuplicates,
  removeBrokenItems,
  removeDeadTagAlbums,
  removeSingleElementAlbums,
  mergeDuplicateTags,
  fragmentVideos
];
