import { hardDel } from "alldex-data/albums";

export const name = "remove:single:element:albums";

export async function run({ bundle: { db }, logger, dryRun }) {
  const singleElementAlbums = await db("albums")
    .leftJoin("albummap", "albums.id", "albummap.album_id")
    .groupBy("albums.id")
    .having(db.raw("count(*) = 1"));

  for (let result of singleElementAlbums) {
    console.log("pruning", "album:" + result.title);
    logger.log("albums", result);
    if (!dryRun) {
      await hardDel(result.id);
    }
  }
}
