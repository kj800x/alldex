import binarySimilarity from "alldex-util/binarySimilarity";

const SIMILARITY_THRESHOLD = 0.85;

export const name = "identify:duplicates";

export async function run({ bundle: { db }, dryRun, forceProcessAll }) {
  const maxAlreadyProcessedItem = forceProcessAll
    ? 0
    : (await db("duplicateMetadata").first("maxItem")).maxItem;

  console.log("Processing items with id >", maxAlreadyProcessedItem);

  const itemsToProcess = await db("items")
    .select("id", "fingerprint")
    .where("id", ">", maxAlreadyProcessedItem)
    .whereNotNull("fingerprint");

  let reference = [];

  if (itemsToProcess.length > 0) {
    reference = await db("items")
      .select("id", "fingerprint")
      .whereNotNull("fingerprint");
  }

  if (forceProcessAll && !dryRun) {
    await db("duplicates").truncate();
  }

  console.log("ItemsToProcess", itemsToProcess.map(e => e.id));
  for (let queuedItem of itemsToProcess) {
    console.log("Processing", queuedItem.id);
    console.log("reference", reference.map(e => e.id));
    for (let referenceItem of reference) {
      // Only build a triangular matrix
      if (queuedItem.id > referenceItem.id) {
        const similarity = binarySimilarity(
          queuedItem.fingerprint,
          referenceItem.fingerprint
        );
        console.log(
          queuedItem.id + " &\t" + referenceItem.id + ":\t " + similarity
        );
        if (similarity >= SIMILARITY_THRESHOLD) {
          if (!dryRun) {
            await db("duplicates").insert({
              item_a_id: queuedItem.id,
              item_b_id: referenceItem.id,
              similarity
            });
          }
        }
      }
    }
  }

  if (!dryRun) {
    await db("duplicateMetadata").update({
      maxItem: (await db("items").max("id"))[0]["max(`id`)"]
    });
  }
}
