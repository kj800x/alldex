import * as db from "alldex-data/db";

const JOB_BUNDLE = {
  db: db.default
};

export default JOB_BUNDLE;
