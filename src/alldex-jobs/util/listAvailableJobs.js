export default function listAvailableJobs(jobs) {
  console.log("\nAvailable Jobs:");
  for (let job of jobs) {
    console.log("  " + job.name);
  }
  console.log(
    "use --dryRun and --forceProcessAll flags to customize the job execution"
  );
}
