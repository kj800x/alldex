import fs from "fs";

import path from "path";

// Hack to get __dirname in an ES6 module:
const __dirname = path.resolve(
  path.dirname(decodeURI(new URL(import.meta.url).pathname))
);

const logDir = path.join(__dirname, "..", "..", "..", "logs");

export default class JobLogger {
  constructor(jobName) {
    this.jobName = jobName;
    this.jobResults = {};
  }

  log(key, entry) {
    if (!this.jobResults[key]) {
      this.jobResults[key] = [];
    }
    this.jobResults[key].push(entry);
  }

  isEmpty() {
    return Object.keys(this.jobResults).length == 0;
  }

  write({ config }) {
    if (!this.isEmpty()) {
      const jobPart = this.jobName.replace(/:/g, "-");
      const datePart = new Date().toISOString().replace(/:/g, "_");
      const logFileName = `${jobPart}.${datePart}.log`;
      if (!fs.existsSync(logDir)) {
        fs.mkdirSync(logDir);
      }
      fs.writeFileSync(
        path.join(logDir, logFileName),
        JSON.stringify({ ...this.jobResults, __config: config })
      );
    }
  }

  print() {
    console.log("\n== JOB RESULTS ==");
    console.log(this.jobResults);
  }
}
