import express from "express";

export class CustomError extends Error {
  constructor(httpStatusCode, httpResponse, suppressLog, ...args) {
    super(...args);
    this.httpStatusCode = httpStatusCode;
    this.httpResponse = httpResponse;
    this.suppressLog = suppressLog;
    Error.captureStackTrace(this, CustomError);
  }
}

function wrap(f) {
  return (request, response) => {
    function handle(value) {
      if (typeof value === "string") {
        response.send(value);
      } else if (value !== undefined) {
        response.setHeader("Content-Type", "application/json");
        response.send(JSON.stringify(value));
      }
    }

    function sendUpdate(json) {
      response.write(JSON.stringify(json));
    }

    function handleError(error) {
      if (error instanceof CustomError) {
        if (!error.suppressLog) {
          console.warn(error);
        }
        response.status(error.httpStatusCode);
        if (error.httpResponse) {
          response.send(error.httpResponse);
        }
      } else {
        console.error(error);
        response.status(500).send("Internal Server Error");
      }
    }

    try {
      Promise.resolve(
        f({
          request,
          response,
          pathParams: request.params,
          queryParams: request.query,
          payload: request.body
        })
      )
        .then(handle)
        .catch(handleError);
    } catch (error) {
      handleError(error);
    }
  };
}

export const makeRouter = () => {
  const router = express.Router();

  router.use(express.json());

  return {
    get: (path, fn) => router.get(path, wrap(fn)),
    post: (path, fn) => router.post(path, wrap(fn)),
    put: (path, fn) => router.put(path, wrap(fn)),
    delete: (path, fn) => router.delete(path, wrap(fn)),
    patch: (path, fn) => router.patch(path, wrap(fn)),
    getRouter: () => router
  };
};
