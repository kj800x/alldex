//

/**
 * Returns the value converted to a string.
 */
export default function toString(value) {
  return String(value);
}
