//
import curry from "./curry";

function bind(operation, context) {
  return operation.bind(context);
}

/**
 * Sets a function's `this` context. Similar to `Function.prototype.bind`.
 *
 * @example
 * bind(console.log, console);
 *
 * @param {Function} operation
 * @param {Object} context
 * @return {Function}
 */
export default curry(bind);
