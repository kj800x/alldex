//
import { Seq } from "immutable";

/**
 * Converts `subject` to a `Seq` if possible.
 *
 * @param  {Array|Iterable|Object|String} subject
 * @return {Seq}
 */
export default function toSeq(subject) {
  return Seq(subject);
}
