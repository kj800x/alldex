//

/**
 * Returns `true` if `subject` is `null`.
 *
 * @param  {any}  subject
 * @return {boolean}
 */
export default function isNull(subject) {
  return subject === null;
}
