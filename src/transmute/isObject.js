//

/**
 * Returns true if `value` is an Object.
 *
 * @param {any} value
 * @return {boolean}
 */
export default function isObject(value) {
  return typeof value === "object" && value !== null;
}
