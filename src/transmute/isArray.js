//

/**
 * Returns `true` if value is an Array.
 *
 * @param {any} value
 * @return {boolean}
 */
export default function isArray(value) {
  return Array.isArray(value);
}
