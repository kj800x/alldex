//

/**
 * Returns `true` if `subject` is `undefined`.
 *
 * @param  {any}  subject
 * @return {boolean}
 */
export default function isUndefined(subject) {
  return subject === undefined;
}
