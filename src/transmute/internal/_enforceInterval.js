//
export default function enforceInterval(interval) {
  if (typeof interval === "number" && interval >= 0) {
    return interval;
  }

  throw new Error(
    `expected \`interval\` to be a positive number but got \`${interval}\``
  );
}
