import express from "express";
import api from "alldex-api";
import semver from "semver";
import minimist from "minimist";
import { load as loadPlugins } from "alldex-plugins";
import { applyGraphql } from "alldex-graphql";
import path from "path";
import * as localproxy from "@kj800x/localproxy-client";
import db from "alldex-data/db";

import staticThumbs from "./staticThumbs";

const args = minimist(process.argv.slice(2));
const app = express();

const ARGUMENTS_HELP = "node index.js [-h|--help] [-p|--port PORT] [--open]";

const UI_BUILD_DIR = "../alldex-ui/build";
const DATA_DIR = "../../data";

async function startServer() {
  app.use("/staticThumbs", staticThumbs);

  app.use("/api", api);

  applyGraphql(app);

  const port = args.p || args.port || 8000;
  const interface_ = args.open ? "0.0.0.0" : "127.0.0.1";

  const localproxyApp = {
    id: "Alldex",
    name: "Alldex",
    routes: [
      {
        static: false,
        route: "/api",
        hostname: "localhost",
        port: port,
        trimRoute: false,
        priority: 0
      },
      {
        static: false,
        route: "/graphql",
        hostname: "localhost",
        port: port,
        trimRoute: false,
        priority: 0
      },
      {
        static: false,
        route: "/staticThumbs",
        hostname: "localhost",
        port: port,
        trimRoute: false,
        priority: 0
      },
      {
        static: true,
        route: "/",
        staticDir: path.resolve(UI_BUILD_DIR) + "/",
        indexFallback: true,
        priority: 0
      },
      {
        static: true,
        route: "/staticAssets",
        staticDir: path.resolve(DATA_DIR) + "/",
        priority: 0
      }
    ]
  };

  await localproxy.register(localproxyApp);

  const server = app.listen(port, interface_);
  console.log(`Listening on ${interface_}:${port}`);

  process.on("SIGINT", async () => {
    db.destroy();
    await localproxy.deregister(localproxyApp);
    server.close();
  });
}

async function start() {
  if (args.help || args.h) {
    console.log(ARGUMENTS_HELP);
    return;
  }

  if (semver.lt(process.version, "9.7.0")) {
    console.error(
      "This program uses dynamic imports which depend on nodejs ^9.7.0. Please upgrade."
    );
  } else {
    await loadPlugins();
    await startServer();
  }
}

start();
