import express from "express";
import thumbsupply from "thumbsupply";
import path from "path";
import os from "os";

const router = express.Router();

const DATA_BASE = "../../data";
const THUMB_ROOT = path.join(os.homedir(), ".cache", "thumbsupply");

router.get("*", async (req, res) => {
  try {
    const thumb = await thumbsupply.generateThumbnail(
      DATA_BASE + decodeURI(req.url)
    );
    res.sendFile(path.relative(THUMB_ROOT, thumb), {
      root: THUMB_ROOT
    });
  } catch (e) {
    console.log(e);
    res.status(404).end();
  }
});

export default router;
