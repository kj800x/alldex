import rp from "request-promise-native";
import cheerio from "cheerio";

import { getServices } from "alldex-plugins";
import { ensure as ensureTags } from "alldex-data/tags";
import { ensure as ensureAlbums } from "alldex-data/albums";

import { addTagGroupId } from "./index";
import { importItem } from "./importItem";
import BUILTIN_IMPORTERS from "./builtin-importers";

const buildArgs = async (sourceUrl, mode) => {
  switch (mode) {
    case "HTML": {
      const content = await rp(sourceUrl, {
        headers: {
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36"
        }
      });
      return [content, sourceUrl];
    }
    case "CHEERIO": {
      const content = await rp(sourceUrl, {
        headers: {
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36"
        }
      });
      return [cheerio.load(content), content, sourceUrl];
    }
    case "URL":
      return [sourceUrl];
    default:
      return [];
  }
};

const importerSort = (a, b) => b.rank - a.rank;
const sleep = ms => new Promise(acc => setTimeout(acc, ms));

const findItems = async sourceUrl => {
  const importers = getServices("import")
    .concat(BUILTIN_IMPORTERS)
    .sort(importerSort);

  for (let importer of importers) {
    console.log("Trying", importer.name);

    try {
      if (await importer.canImport(sourceUrl)) {
        console.log("Importing", sourceUrl, "with", importer.name);
        const args = await buildArgs(sourceUrl, importer.mode);
        const results = await importer.findItems(...args);
        if (importer.sleep) {
          await sleep(importer.sleep * 1000);
        }
        if (results.length !== 0) {
          return results;
        }
      }
    } catch (e) {
      console.error("Importer", importer.name, "had error", e);
    }
  }
  return [];
};

export const importSource = async (
  sourceUrl,
  tagIds,
  albumIds,
  importTagName
) => {
  const items = await findItems(sourceUrl);

  const results = [];

  for (let itemData of items) {
    const allItemTagIds = tagIds.concat(
      await ensureTags(await addTagGroupId(itemData.extraTags))
    );
    const allItemAlbumIds = albumIds.concat(
      await ensureAlbums(itemData.extraAlbums)
    );

    results.push(
      await importItem(
        sourceUrl,
        itemData,
        allItemTagIds,
        allItemAlbumIds,
        importTagName
      )
    );
  }
  return results;
};
