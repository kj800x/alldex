import { resolve } from "path";
import fs from "fs";

export default async function mkdirp(dir) {
  dir = resolve(dir);

  const dirs = dir.split("/");
  for (let i = 2; i <= dirs.length; i++) {
    // TODO FIXME why 2?
    const d = dirs.slice(0, i).join("/");
    if (!fs.existsSync(d)) {
      // TODO FIXME make this asynchronous
      fs.mkdirSync(d);
    }
  }
}
