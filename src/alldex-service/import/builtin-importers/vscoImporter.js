const canImport = url =>
  url.indexOf("vsco.co") !== -1 && url.indexOf("/media/") !== -1;

const absFindKey = 'og:image" content="';

const getUsername = url => new URL(url).pathname.split("/")[1];

const findItems = (content, url) => {
  var urlStartIdx = content.indexOf(absFindKey) + absFindKey.length;
  var urlEndIdx = content.indexOf("?", urlStartIdx);
  var url = content.substring(urlStartIdx, urlEndIdx);
  return [
    {
      TYPE: "IMAGE",
      url: url,
      extraTags: ["vsco", getUsername(url)],
      extraAlbums: []
    }
  ];
};

export default {
  canImport,
  findItems,
  mode: "HTML",
  rank: 1,
  name: "VSCO Importer"
};
