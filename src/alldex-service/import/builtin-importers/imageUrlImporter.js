import rp from "request-promise-native";

const isValidUrl = url => {
  try {
    new URL(url);
    return true;
  } catch (e) {
    return false;
  }
};

const canImport = async url => {
  if (!isValidUrl(url)) {
    return false;
  }

  const headers = await rp.head(url, {
    headers: {
      "User-Agent":
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36",
      Referer: url
    }
  });
  return headers["content-type"].indexOf("image") !== -1;
};

const findItems = url => {
  const parsedUrl = new URL(url);

  return [
    {
      TYPE: "IMAGE",
      url: url,
      extraTags: [parsedUrl.hostname],
      extraAlbums: []
    }
  ];
};

export default {
  canImport,
  findItems,
  mode: "URL",
  name: "Image Url Importer",
  rank: -5
};
