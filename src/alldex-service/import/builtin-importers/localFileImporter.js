const fs = require("fs");
const path = require("path");

const first = list => list[0];
const rest = list => list.slice(1);
const empty = list => list.length === 0;
const orMap = (func, list) =>
  empty(list) ? false : func(first(list)) || orMap(func, rest(list));

const IMPORT_DIR = "../../data";
const RESOLVED_IMPORT_DIR = path.resolve(IMPORT_DIR);

const KNOWN_TYPES = {
  ".jpeg": "IMAGE",
  ".png": "IMAGE",
  ".gif": "IMAGE",
  ".jpg": "IMAGE",
  ".txt": "TEXT",
  ".mp4": "VIDEO",
  ".zip": "IMAGESET"
};

const normalize = url =>
  url.startsWith("file://") ? url.replace("file://", "") : url;
const isLocalAbsolute = url => normalize(url).startsWith("/");
const isLocalRelative = url => {
  console.log("checking", path.resolve(RESOLVED_IMPORT_DIR, url));
  return fs.existsSync(path.resolve(RESOLVED_IMPORT_DIR, url));
};
const isTypeFor = url => type => url.endsWith(type);
const isKnownType = url => orMap(isTypeFor(url), Object.keys(KNOWN_TYPES));
const getType = url => Object.keys(KNOWN_TYPES).find(isTypeFor(url));

const canImport = url =>
  (isLocalAbsolute(url) || isLocalRelative(url)) && isKnownType(url);
const findItems = url => {
  return [
    {
      TYPE: KNOWN_TYPES[getType(url)],
      url: url,
      extraTags: [],
      extraAlbums: []
    }
  ];
};

export default {
  canImport,
  findItems,
  mode: "URL",
  rank: -99,
  name: "Local Importer"
};
