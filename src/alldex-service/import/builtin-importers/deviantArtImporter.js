const canImport = url =>
  url.indexOf("deviantart.com") !== -1 && url.indexOf("/art/") !== -1;

const resolvesrcset = srcset => {
  return srcset.split("w,")[srcset.split("w,").length - 1].split(" ")[0];
};

const getText = $ => {
  let main_content = $(".journal-wrapper .text");
  main_content.find("script").replaceWith("");
  main_content.find("br").replaceWith("\n");
  return main_content.text();
};

const getTitle = $ => {
  return $(".dev-title-container .title").text();
};

const getAuthor = $ => {
  return $(".dev-title-container .author .username").text();
};

const findItems = $ => {
  if ($(".journal-wrapper").length > 0) {
    return [
      {
        TYPE: "TEXT",
        content: getText($).trim(),
        title: getTitle($),
        description: $(".dev-description").text(),
        extraTags: ["deviantArt", getAuthor($)],
        extraAlbums: []
      }
    ];
  } else if ($(".dev-content-full").length > 0) {
    return [
      {
        TYPE: "IMAGE",
        url: $(".dev-content-full").attr("src"),
        title: getTitle($),
        description: $(".dev-description").text(),
        extraTags: ["deviantArt", getAuthor($)],
        extraAlbums: []
      }
    ];
  } else if ($("*[srcset]").length > 0) {
    return [
      {
        TYPE: "IMAGE",
        url: resolvesrcset($("*[srcset]").attr("srcset")),
        title: getTitle($),
        description: $(".dev-description").text(),
        extraTags: ["deviantArt", getAuthor($)],
        extraAlbums: []
      }
    ];
  }
};

export default {
  canImport,
  findItems,
  mode: "CHEERIO",
  rank: 1,
  name: "DeviantArt Importer",
  sleep: 2
};
