const canImport = () => true;

const isValidUrl = url => {
  try {
    new URL(url);
    return true;
  } catch (e) {
    return false;
  }
};

const findItems = url => {
  const extraTags = [];

  if (isValidUrl(url)) {
    const parsedUrl = new URL(url);
    extraTags.push(parsedUrl.hostname);
  }

  return [
    {
      TYPE: "URL",
      url: url,
      extraTags,
      extraAlbums: []
    }
  ];
};

export default {
  canImport,
  findItems,
  mode: "URL",
  rank: -100,
  name: "Url Fallback Importer"
};
