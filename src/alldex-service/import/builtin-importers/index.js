import URLFallbackImporter from "./urlFallbackImporter";
import localFileImporter from "./localFileImporter";
import vscoImporter from "./vscoImporter";
import deviantArtImporter from "./deviantArtImporter";
import imageUrlImporter from "./imageUrlImporter";

export default [
  localFileImporter,
  URLFallbackImporter,
  deviantArtImporter,
  vscoImporter,
  imageUrlImporter
];
