import download from "./download";
import * as path from "path";

import { create as createItem, setFingerprint } from "alldex-data/items";
import { associateItem as associateItemToTag } from "alldex-data/tags";
import { associateItem as associateItemToAlbum } from "alldex-data/albums";
import { create as createImage } from "alldex-data/images";
import { create as createUrl } from "alldex-data/urls";
import { create as createVideo } from "alldex-data/videos";
import { create as createText } from "alldex-data/texts";
import fingerprintImageFile from "alldex-util/fingerprintImageFile";
import fingerprintVideoFile from "alldex-util/fingerprintVideoFile";
import resolveDataFile from "alldex-util/resolveDataFile";

const IMPORT_DIR = "../../data";
const RESOLVED_IMPORT_DIR = path.resolve(IMPORT_DIR);

// Remove weird characters
const makeCleanFilename = weirdFilename =>
  decodeURIComponent(weirdFilename).replace(/[^a-zA-Z0-9-_\.]/g, "");
const getFilename = url =>
  makeCleanFilename(
    path.parse(
      new URL(!url.startsWith("http") ? "file://" + url : url).pathname
    ).base
  );
const removePrefix = prefix => str =>
  str.startsWith(prefix) ? str.substring(prefix.length) : str;
const makeRelativeToImportDir = removePrefix(RESOLVED_IMPORT_DIR + "/");

const isInImportDir = source => {
  if (source.startsWith("http")) {
    return false;
  }
  source = source.replace("file://", "");
  source = path.resolve(RESOLVED_IMPORT_DIR, source);
  return source.startsWith(RESOLVED_IMPORT_DIR);
};

const makeLocal = async (source, importTagName) => {
  if (isInImportDir(source)) {
    return makeRelativeToImportDir(
      path.resolve(RESOLVED_IMPORT_DIR, source.replace("file://", ""))
    );
  } else {
    const filename = getFilename(source);
    const inImportDirDownloadPath = `${importTagName}/${filename}`;
    const downloadTo = `${IMPORT_DIR}/${inImportDirDownloadPath}`;
    await download(source, downloadTo);
    return inImportDirDownloadPath;
  }
};

const TYPE_SPECIFIC_IMPORTER = {
  IMAGE: async (itemData, itemId, importTagName) => {
    const localFile = await makeLocal(itemData.url, importTagName);
    await createImage({ item_id: itemId, file: localFile });
    await setFingerprint(
      itemId,
      await fingerprintImageFile(resolveDataFile(localFile))
    );
  },
  URL: async (itemData, itemId) => {
    await createUrl({ item_id: itemId, url: itemData.url });
  },
  VIDEO: async (itemData, itemId, importTagName) => {
    const localFile = await makeLocal(itemData.url, importTagName);
    await createVideo({ item_id: itemId, file: localFile });
    await setFingerprint(
      itemId,
      await fingerprintVideoFile(resolveDataFile(localFile))
    );
  },
  TEXT: async (itemData, itemId) => {
    await createText({ item_id: itemId, content: itemData.content });
  }
};

export const importItem = async (
  sourceUrl,
  itemData,
  tagIds,
  albumIds,
  importTagName
) => {
  const itemId = await createItem({
    url: sourceUrl,
    title: itemData.title || undefined,
    description: itemData.description || undefined
  });

  await Promise.all([
    TYPE_SPECIFIC_IMPORTER[itemData.TYPE](itemData, itemId, importTagName),
    Promise.all(tagIds.map(tagId => associateItemToTag(tagId, itemId))),
    Promise.all(albumIds.map(albumId => associateItemToAlbum(albumId, itemId)))
  ]);

  return itemId;
};
