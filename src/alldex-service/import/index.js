import {
  getDefaultTagGroupId,
  getImportTagGroupId
} from "alldex-data/taggroups";
import { ensure as ensureTags } from "alldex-data/tags";
import { ensure as ensureAlbums } from "alldex-data/albums";

import { importSource } from "./importSource";

const mapTagName = defaultTagGroupId => tag => ({
  value: tag,
  taggroup_id: defaultTagGroupId
});

function formattedDate(date) {
  return date.toISOString();
}

async function buildImportTag() {
  const importTagGroupId = await getImportTagGroupId();
  return [
    {
      value: "imported_" + formattedDate(new Date()),
      taggroup_id: importTagGroupId
    }
  ];
}

export async function addTagGroupId(tags) {
  const defaultTagGroupId = await getDefaultTagGroupId();
  return tags.map(mapTagName(defaultTagGroupId));
}

export const importSources = async ({ sources, tags, albums }) => {
  const tagsWithGroups = await addTagGroupId(tags);
  const importTagSpec = await buildImportTag();
  const importTagName = importTagSpec[0].value;

  const [tagIds, importTagIds, albumIds] = await Promise.all([
    ensureTags(tagsWithGroups),
    ensureTags(importTagSpec),
    ensureAlbums(albums)
  ]);

  const allTagIds = tagIds.concat(importTagIds);

  for (let i = 0; i < sources.length; i++) {
    console.log("Importing Item " + (i + 1) + "/" + sources.length);
    await importSource(sources[i], allTagIds, albumIds, importTagName);
  }

  return { status: "complete", import_tag: importTagIds[0] };
};
