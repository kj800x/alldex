import request from "request";
import mkdirp from "./mkdirp";
import path from "path";
import fs from "fs";

const normalize = url =>
  url.startsWith("file://") ? url.replace("file://", "") : url;
const isLocal = url => normalize(url).startsWith("/");

function streamFromTo(rd, target) {
  const wr = fs.createWriteStream(target);
  return new Promise((resolve, reject) => {
    rd.on("error", reject);
    wr.on("error", reject);
    wr.on("finish", resolve);
    rd.pipe(wr);
  }).catch(error => {
    rd.destroy();
    wr.end();
    throw error;
  });
}

export default async function download(from, to) {
  mkdirp(path.dirname(to));

  if (isLocal(from)) {
    await streamFromTo(fs.createReadStream(normalize(from)), to);
  } else {
    await streamFromTo(
      request({ uri: from, method: "GET", headers: { Referer: from } }),
      to
    );
  }
}
