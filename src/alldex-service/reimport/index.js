import { fetchHydratedItem, del as delItem } from "alldex-data/items";
import { importSources } from "../import";
import { ensure as ensureTags } from "alldex-data/tags";
import { fetch as fetchAlbum } from "alldex-data/albums";
import { fetch as fetchTag } from "alldex-data/tags";
import { getImportTagGroupId } from "alldex-data/taggroups";

async function buildReimportTag(id) {
  const importTagGroupId = await getImportTagGroupId();
  return [{ value: `reimported_from_${id}`, taggroup_id: importTagGroupId }];
}

export const reimportItem = async id => {
  const item = await fetchHydratedItem(id);
  console.log(item);
  const source = item.sourceUrl;
  const [tags, albums] = await Promise.all([
    Promise.all(
      item.tags.map(fetchTag).map(async tagPromise => (await tagPromise).value)
    ),
    Promise.all(
      item.albums
        .map(entry => entry.album_id)
        .map(fetchAlbum)
        .map(async albumPromise => (await albumPromise).title)
    )
  ]);
  console.log(tags, albums);

  const reimportTagIds = await ensureTags(await buildReimportTag(id));

  const imported = await importSources({
    sources: [source],
    tags: [`reimported_from_${id}`, ...tags],
    albums: albums
  });
  await delItem(id);
  return { status: "complete", import_tag: reimportTagIds[0] };
};
