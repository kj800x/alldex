import {
  update,
  fetchItemBySourceKey,
  fetchHydratedItem
} from "alldex-data/items";
import {
  associateItem as associateAlbumItem,
  disassociateItem as dissociateAlbumItem,
  ensure as ensureAlbum
} from "alldex-data/albums";
import {
  associateItem as associateTagItem,
  disassociateItem as dissociateTagItem,
  ensure as ensureTag
} from "alldex-data/tags";
import { importSource } from "../import/importSource";

const syncItem = async (associators, sourceKey, syncData) => {
  let itemData = await fetchItemBySourceKey(sourceKey);

  if (itemData === null) {
    // Create the item
    const tagIds = (syncData["tags"] || []).map(
      tagName => associators.tags[tagName]
    );
    const albumIds = (syncData["albums"] || []).map(
      albumTitle => associators.albums[albumTitle]
    );

    itemData = await fetchHydratedItem(
      (await importSource(
        syncData.sourceUrl,
        tagIds,
        albumIds,
        `SYNC_${new Date().toISOString()}`
      ))[0]
    );
  } else {
    // Update the item
    for (const tagName of syncData["tags"] || []) {
      await associateTagItem(associators.tags[tagName], itemData.id);
    }
    for (const albumTitle of syncData["albums"] || []) {
      await associateAlbumItem(associators.albums[albumTitle], itemData.id);
    }
    for (const tagName of syncData["-tags"] || []) {
      await dissociateTagItem(associators.tags[tagName], itemData.id);
    }
    for (const albumTitle of syncData["-albums"] || []) {
      await dissociateAlbumItem(associators.albums[albumTitle], itemData.id);
    }
  }

  const updatedProperty = prop =>
    syncData.hasOwnProperty(prop) ? syncData[prop] : itemData[prop];
  await update(itemData.id, {
    isDeleted: updatedProperty("isDeleted"),
    title: updatedProperty("title"),
    description: updatedProperty("description"),
    sourceUrl: updatedProperty("sourceUrl"),
    sourceKey: syncData.hasOwnProperty("sourceKey")
      ? syncData["sourceKey"]
      : sourceKey
  });
};

const prefetchAssociators = async payload => {
  const tagsSet = new Set();
  const albumsSet = new Set();

  for (const syncData of Object.values(payload)) {
    for (const tag of syncData["tags"] || []) {
      tagsSet.add(tag);
    }
    for (const tag of syncData["-tags"] || []) {
      tagsSet.add(tag);
    }
    for (const tag of syncData["albums"] || []) {
      albumsSet.add(tag);
    }
    for (const tag of syncData["-albums"] || []) {
      albumsSet.add(tag);
    }
  }

  const tags = [...tagsSet];
  const albums = [...albumsSet];

  const [tagsData, albumsData] = await Promise.all([
    ensureTag(tags.map(tag => ({ value: tag }))),
    ensureAlbum(albums)
  ]);

  const mappedTags = {};
  const mappedAlbums = {};
  for (let i = 0; i < tags.length; i++) {
    mappedTags[tags[i]] = tagsData[i];
  }
  for (let i = 0; i < albums.length; i++) {
    mappedAlbums[albums[i]] = albumsData[i];
  }

  return {
    tags: mappedTags,
    albums: mappedAlbums
  };
};

export const syncItems = async payload => {
  const associators = await prefetchAssociators(payload);
  const sourceKeys = Object.keys(payload);
  for (const sourceKey of sourceKeys) {
    await syncItem(associators, sourceKey, payload[sourceKey]);
  }
  return "OK";
};
