import { syncItems } from "./syncItems";
import { searchIds } from "alldex-data/items";

function fromEntries(iterable) {
  return [...iterable].reduce(
    (obj, { 0: key, 1: val }) => Object.assign(obj, { [key]: val }),
    {}
  );
}

export const bulkAction = async payload => {
  const ids = await searchIds({ filter: payload.query });
  const syncPayload = fromEntries(ids.map(id => [`${id}`, payload.action]));
  console.log(syncPayload);
  return await syncItems(syncPayload);
};
